<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Quick Market - Berita</title>
    <link rel="icon" href="{{ asset('about/QM Logo.png') }}" type="image/png">
    <link rel="stylesheet" href="{{ asset('aroma/vendors/bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('aroma/vendors/fontawesome/css/all.min.css') }}">
	<link rel="stylesheet" href="{{ asset('aroma/vendors/themify-icons/themify-icons.css') }}">
	<link rel="stylesheet" href="{{ asset('aroma/vendors/linericon/style.css') }}">
    <link rel="stylesheet" href="{{ asset('aroma/vendors/owl-carousel/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('aroma/vendors/owl-carousel/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('aroma/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('aroma/css/scrollbar.css') }}">
</head>
<body>

    @include('user/header')


  <!-- ================ start banner area ================= -->	
  <section class="blog-banner-area" id="blog">
    <div class="container h-100">
      <div class="blog-banner">
        <div class="text-center">
          <h1>Berita</h1>
          <nav aria-label="breadcrumb" class="banner-breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="/user">Beranda</a></li>
              <li class="breadcrumb-item active" aria-current="page">Berita</li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </section>
  <!-- ================ end banner area ================= -->


  <!--================Blog Categorie Area =================-->
  <section class="blog_categorie_area">
    <div class="container">
      <div class="row">
        <div class="col-sm-6 col-lg-4 mb-4 mb-lg-0">
            <div class="categories_post">
                <img class="card-img rounded-0" src="{{ asset('aroma/img/blog/cat-post/cat-post-3.jpg') }}" alt="post" style="cursor: default">
                <div class="categories_details" style="cursor: default">
                    <div class="categories_text">
                        <a href="{{ url('/user/category/news/sosial') }}">
                            <h5>Sosial</h5>
                        </a>
                        <div class="border_line"></div>
                        <a href="{{ url('/user/category/news/sosial') }}">
                            <p style="color: white">Nikmati kehidupan sosial bersama</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-lg-4 mb-4 mb-lg-0">
          <div class="categories_post">
            <img class="card-img rounded-0" src="{{ asset('aroma/img/blog/cat-post/cat-post-2.jpg') }}" alt="post" style="cursor: default">
            <div class="categories_details" style="cursor: default">
              <div class="categories_text">
                <a href="{{ url('/user/category/news/politik') }}">
                    <h5>Politik</h5>
                </a>
                <div class="border_line"></div>
                <a href="{{ url('/user/category/news/politik') }}">
                    <p style="color: white">Jadilah bagian dari politik</p>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-lg-4 mb-4 mb-lg-0">
            <div class="categories_post">
                <img class="card-img rounded-0" src="{{ asset('aroma/img/blog/cat-post/cat-post-1.jpg') }}" alt="post" style="cursor: default">
                <div class="categories_details" style="cursor: default">
                    <div class="categories_text">
                        <a href="{{ url('/user/category/news/kuliner') }}">
                            <h5>Kuliner</h5>
                        </a>
                        <div class="border_line"></div>
                        <a href="{{ url('/user/category/news/kuliner') }}">
                            <p style="color: white">Yuk akhiri dengan makan bersama</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
  </section>
  <!--================Blog Categorie Area =================-->

  <!--================Blog Area =================-->
  <section class="blog_area">
      <div class="container">
          <div class="row">
              <div class="col-lg-8">
                  <div class="blog_left_sidebar">
                      @foreach ($news as $row)
                      <article class="row blog_item">
                          <div class="col-md-3">
                              <div class="blog_info text-right">
                                  <div class="post_tag">
                                      @foreach ($kategori as $item)
                                        @if ($item->nama_kategori == $row->kategori_berita)
                                            <a href="{{ url('/user/category/news', strtolower($item->nama_kategori)) }}" class="active">{{ $item->nama_kategori }}</a>
                                        @else
                                            <a href="{{ url('/user/category/news', strtolower($item->nama_kategori)) }}">{{ $item->nama_kategori }}</a>
                                        @endif
                                      @endforeach
                                  </div>
                                  <ul class="blog_meta list">
                                      <li><a href="{{ url('/user/news/'. $row->penulis) }}">{{ $row->penulis }}<i class="lnr lnr-user"></i></a></li>
                                      {{-- <li><a href="#" style="cursor: default">{{ ('d M, Y', strtotime($row->created_at)) }}<i class="lnr lnr-calendar-full"></i></a></li> --}}
                                      {{-- <li><a href="#" style="cursor: default">{{ \Carbon\Carbon::parse(strtotime($row->created_at))->translatedFormat('l F, Y') }}<i class="lnr lnr-calendar-full"></i></a></li> --}}
                                      <li><a href="#" style="cursor: default">{{ \Carbon\Carbon::parse(strtotime($row->created_at))->translatedFormat('d F, Y') }}<i class="lnr lnr-calendar-full"></i></a></li>
                                      {{-- <li><a href="#" style="cursor: default">{{ \Carbon\Carbon::parse(strtotime($row->created_at))->formatLocalized('%A F, Y') }}<i class="lnr lnr-calendar-full"></i></a></li> --}}
                                      {{-- <li><a href="#" style="cursor: default">{{ \Carbon\Carbon::parse(strtotime($row->created_at))->isoFormat('LLLL') }}<i class="lnr lnr-calendar-full"></i></a></li> --}}
                                      <li><a href="#" style="cursor: default">{{ $row->kunjungan }}x Dilihat<i class="lnr lnr-eye"></i></a></li>
                                      <li><a href="#" style="cursor: default">{{ \App\Http\Controllers\NewsController::totalKomentar($row->id) }} Komentar<i class="lnr lnr-bubble"></i></a></li>
                                      {{-- <input type="text" value="{{ $row->id }}" name="id" hidden> --}}
                                  </ul>
                              </div>
                          </div>
                          <div class="col-md-9">
                              <div class="blog_post">
                                  <img src="{{ asset('uploads'. $row->thumbnail_berita) }}" height="350px" class="card-img-top">
                                  <div class="blog_details">
                                      <a href="{{ url('user/news/' . $row->id . '/read') }}">
                                          <h2>{{ $row->judul_berita }}</h2>
                                      </a>
                                      <p>{{ $row->caption_berita }}</p>
                                      <a class="button button-blog" href="{{ url('user/news/' . $row->id . '/read') }}">Baca Selengkapnya</a>
                                  </div>
                              </div>
                          </div>
                      </article>
                      @endforeach
                      @if (Request::path() === 'user/news')
                      <nav class="blog-pagination justify-content-center d-flex">
                        {{ $news->links() }}
                      </nav>
                      @else
                          
                      @endif
                  </div>
              </div>
              <div class="col-lg-4">
                  <div class="blog_right_sidebar">
                      {{-- <aside class="single_sidebar_widget search_widget">
                          <div class="input-group">
                              <input type="text" class="form-control" placeholder="Search Posts">
                              <span class="input-group-btn">
                                  <button class="btn btn-default" type="button">
                                      <i class="lnr lnr-magnifier"></i>
                                  </button>
                              </span>
                          </div>
                          <!-- /input-group -->
                          <div class="br"></div>
                      </aside> --}}
                      <aside class="single_sidebar_widget author_widget">
                          <img class="author_img rounded-circle" src="{{ asset('about/Cimon_Author.jpg') }}" alt="" style="width: 49%; height: 49%;">
                          <h4>Cimon</h4>
                          <p>Senior Mouse Hunter</p>
                          <div class="social_icon">
                            <a href="https://www.facebook.com/azriel.maulana.9406">
                                  <i class="fab fa-facebook-f"></i>
                              </a>
                              {{-- <a href="#">
                                  <i class="fab fa-twitter"></i>
                              </a> --}}
                              <a href="https://github.com/Bancet6996">
                                  <i class="fab fa-github"></i>
                              </a>
                              {{-- <a href="#">
                                <i class="fab fa-behance"></i>
                              </a> --}}
                          </div>
                          <p>Weeeeoooonnnnggg..... Weeeeeeeewwwww.... Weeeeeonggggg....
                          </p>
                          <div class="br"></div>
                      </aside>
                      <aside class="single_sidebar_widget popular_post_widget">
                          <h3 class="widget_title">Postingan Terpopuler</h3>
                          @foreach ($ob_visit->take(5) as $item)
                            <div class="media post_item">
                                <img src="{{ asset('uploads'. $item->thumbnail_berita) }}" alt="post" width="100px" height="60px">
                                <div class="media-body">
                                    <a href="{{ url('user/news/' . $item->id . '/read') }}">
                                        <h3>{{ $item->judul_berita }}</h3>
                                    </a>
                                    <p>{{ Carbon\Carbon::parse($item->created_at)->diffForHumans() }}</p>
                                </div>
                            </div>
                          @endforeach
                          <div class="br"></div>
                      </aside>
                      {{-- <aside class="single_sidebar_widget ads_widget">
                          <a href="#">
                              <img class="img-fluid" src="{{ asset('aroma/img/blog/add.jpg') }}" alt="">
                          </a>
                          <div class="br"></div>
                      </aside> --}}
                      <aside class="single_sidebar_widget post_category_widget">
                          <h4 class="widget_title">Kategori Berita</h4>
                          <ul class="list cat-list">
                              @foreach ($kategori as $item)
                              <li>
                                <a href="{{ url('/user/category/news', strtolower($item->nama_kategori)) }}" class="d-flex justify-content-between">
                                    <p>{{ $item->nama_kategori }}</p>
                                    <p>{{ \App\Http\Controllers\NewsController::totalKategori($item->nama_kategori) }}</p>
                                </a>
                              </li>
                              @endforeach
                          </ul>
                          <div class="br"></div>
                      </aside>
                      <aside class="single-sidebar-widget newsletter_widget">
                          <h4 class="widget_title">Newsletter</h4>
                          <p>Here, I focus on a range of items and features that we use in life without giving them a second thought.</p>
                          <div class="form-group d-flex flex-row">
                              <div class="input-group">
                                  <div class="input-group-prepend">
                                      <div class="input-group-text">
                                          <i class="fa fa-envelope" aria-hidden="true"></i>
                                      </div>
                                  </div>
                                  <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="Enter email address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter email'">
                              </div>
                              <a href="#" class="bbtns">Subcribe</a>
                          </div>
                          <p class="text-bottom">You can unsubscribe at any time</p>
                          <div class="br"></div>
                      </aside>
                      <aside class="single-sidebar-widget tag_cloud_widget">
                          <h4 class="widget_title">Kategori</h4>
                          <ul class="list">
                              @foreach ($kategori as $item)
                                <li><a href="{{ url('/user/category/news', strtolower($item->nama_kategori)) }}">{{ $item->nama_kategori }}</a></li>
                              @endforeach
                          </ul>
                      </aside>
                  </div>
              </div>
          </div>
      </div>
  </section>
  <!--================Blog Area =================-->
  <br>
  <!--================Instagram Area =================-->
  <section class="instagram_area">
    <div class="container box_1620">
      <div class="insta_btn">
        <a class="btn theme_btn" href="https://www.instagram.com/quick77market/">Ikuti kami di Instagram</a>
      </div>
      <div class="instagram_image row m0">
        <a href="#" style="cursor: default"><img src="{{ asset('aroma/img/instagram/ins-1.jpg') }}" alt=""></a>
        <a href="#" ><img src="{{ asset('aroma/img/instagram/ins-2.jpg') }}" alt=""></a>
        <a href="#" ><img src="{{ asset('aroma/img/instagram/ins-3.jpg') }}" alt=""></a>
        <a href="#" ><img src="{{ asset('aroma/img/instagram/ins-4.jpg') }}" alt=""></a>
        <a href="#" ><img src="{{ asset('aroma/img/instagram/ins-5.jpg') }}" alt=""></a>
        <a href="#" ><img src="{{ asset('aroma/img/instagram/ins-6.jpg') }}" alt=""></a>
      </div>
    </div>
  </section>
  <!--================End Instagram Area =================-->
  
  @include('user/footer')

  <script src="{{ asset('aroma/vendors/jquery/jquery-3.2.1.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/bootstrap/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/skrollr.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/owl-carousel/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/nice-select/jquery.nice-select.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/jquery.ajaxchimp.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/mail-script.js') }}"></script>
  <script src="{{ asset('aroma/js/main.js') }}"></script>
</body>
</html>