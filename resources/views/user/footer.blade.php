<!--================ Start footer Area  =================-->	
<footer class="footer">
		<div class="footer-area">
			<div class="container">
				<div class="row section_gap">
					<div class="col-lg-3 col-md-6 col-sm-6">
						<div class="single-footer-widget tp_widgets">
							<h4 class="footer_title large_title">Misi Kami</h4>
							<p>
								So seed seed green that winged cattle in. Gathering thing made fly you're no 
								divided deep moved us lan Gathering thing us land years living.
							</p>
						</div>
					</div>
					<div class="offset-lg-1 col-lg-2 col-md-6 col-sm-6">
						<div class="single-footer-widget tp_widgets">
							<h4 class="footer_title">Tautan Cepat</h4>
							<ul class="list">
								<li><a href="/user">Beranda</a></li>
								<li><a href="/user/news">Berita</a></li>
								<li><a href="/user/contact">Kontak</a></li>
							</ul>
						</div>
					</div>
					<div class="col-lg-2 col-md-6 col-sm-6">
						<div class="single-footer-widget instafeed">
							<h4 class="footer_title">Galeri</h4>
							<ul class="list instafeed d-flex flex-wrap">
								<li><img src="{{ asset('aroma/img/gallery/r1.jpg') }}" alt=""></li>
								<li><img src="{{ asset('aroma/img/gallery/r2.jpg') }}" alt=""></li>
								<li><img src="{{ asset('aroma/img/gallery/r3.jpg') }}" alt=""></li>
								<li><img src="{{ asset('aroma/img/gallery/r5.jpg') }}" alt=""></li>
								<li><img src="{{ asset('aroma/img/gallery/r7.jpg') }}" alt=""></li>
								<li><img src="{{ asset('aroma/img/gallery/r8.jpg') }}" alt=""></li>
							</ul>
						</div>
					</div>
					<div class="offset-lg-1 col-lg-3 col-md-6 col-sm-6">
						<div class="single-footer-widget tp_widgets">
							<h4 class="footer_title">Tentangku</h4>
							<div class="ml-40">
								<p class="sm-head">
									<span class="fa fa-user"></span>
									Nama
								</p>
								<p>Azriel Maulana</p>
	
								<p class="sm-head">
									<span class="fa fa-phone"></span>
									Nomor Telepon
								</p>
								<p>
									+62 88 222 656 489
								</p>
	
								<p class="sm-head">
									<span class="fa fa-envelope"></span>
									Email
								</p>
								<p>
									bancet6996@gmail.com
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="footer-bottom">
			<div class="container">
				<div class="row d-flex">
					<p class="col-lg-12 footer-text text-center">
						<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> <a href="/">Quick Market</a> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
				</div>
			</div>
		</div>
	</footer>
	<!--================ End footer Area  =================-->