<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Quick Market - Detail Berita</title>
	<link rel="icon" href="{{ asset('about/QM Logo.png') }}" type="image/png">
	<link rel="stylesheet" href="{{ asset('aroma/vendors/bootstrap/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('aroma/vendors/fontawesome/css/all.min.css') }}">
	<link rel="stylesheet" href="{{ asset('aroma/vendors/themify-icons/themify-icons.css') }}">
	<link rel="stylesheet" href="{{ asset('aroma/vendors/linericon/style.css') }}">
	<link rel="stylesheet" href="{{ asset('aroma/vendors/owl-carousel/owl.theme.default.min.css') }}">
	<link rel="stylesheet" href="{{ asset('aroma/vendors/owl-carousel/owl.carousel.min.css') }}">
	<link rel="stylesheet" href="{{ asset('aroma/css/style.css') }}">
  	<link rel="stylesheet" href="{{ asset('aroma/css/scrollbar.css') }}">
</head>
<body>

	@include('user/header')

	<!-- ================ start banner area ================= -->	
	<section class="blog-banner-area" id="blog">
	    <div class="container h-100">
			<div class="blog-banner">
				<div class="text-center">
					<h1>Detail Berita</h1>
					<nav aria-label="breadcrumb" class="banner-breadcrumb">
                        <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/user">Beranda</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Berita</li>
                        </ol>
                    </nav>
				</div>
			</div>
        </div>
	</section>
	<!-- ================ end banner area ================= -->



  <!--================Blog Area =================-->
	<section class="blog_area single-post-area py-80px section-margin--small">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 posts-list">
					<div class="single-post row">
						<div class="col-lg-12">
							<div class="feature-img"><img class="img-fluid" src="{{ asset('uploads'. $news->thumbnail_berita) }}" alt=""></div>
						</div>
						<div class="col-lg-3  col-md-3">
							<div class="blog_info text-right">
								<div class="post_tag">
									@foreach ($kategori as $item)
                                        @if ($item->nama_kategori == $news->kategori_berita)
                                            <a href="{{ url('/user/category/news', $item->nama_kategori) }}" class="active">{{ old('kategori_berita', @$news->kategori_berita) }}</a>
                                        @else
                                            <a href="{{ url('/user/category/news', $item->nama_kategori) }}">{{ $item->nama_kategori }}</a>
                                        @endif
                                      @endforeach
								</div>
								<ul class="blog_meta list">
									<li><a href="{{ url('/user/news/'. @$news->penulis) }}">{{ old('penulis', @$news->penulis) }}<i class="lnr lnr-user"></i></a></li>
									<li><a href="#">{{ old('created_at', \Carbon\Carbon::parse(strtotime(@$news->created_at))->translatedFormat('d F, Y')) }}<i class="lnr lnr-calendar-full"></i></a></li>
									<li><a href="#">{{ old('kunjungan', @$news->kunjungan) }}x Dilihat<i class="lnr lnr-eye"></i></a></li>
									<li><a href="#">{{ $total_komentar }} Komentar<i class="lnr lnr-bubble"></i></a></li>
								</ul>
							</div>
						</div>
						<div class="col-lg-9 col-md-9 blog_details">
							<h2>{{ old('judul_berita', @$news->judul_berita) }}</h2>
							<p class="excert">{!! old('deskripsi_berita', @$news->deskripsi_berita) !!}</p>
						</div>
					</div>
				@include('user/comment')
				</div>
					<div class="col-lg-4">
						<div class="blog_right_sidebar">
							{{-- <aside class="single_sidebar_widget search_widget">
								<div class="input-group">
									<input type="text" class="form-control" placeholder="Search Posts">
									<span class="input-group-btn">
										<button class="btn btn-default" type="button">
											<i class="lnr lnr-magnifier"></i>
										</button>
									</span>
								</div>
							<!-- /input-group -->
							<div class="br"></div>
							</aside> --}}
							<aside class="single_sidebar_widget author_widget">
								<img class="author_img rounded-circle" src="{{ asset('about/Cimon_Author.jpg') }}" alt="" style="width: 49%; height: 49%;">
								<h4>Cimon</h4>
								<p>Senior Mouse Hunter</p>
								<div class="social_icon">
									<a href="https://www.facebook.com/azriel.maulana.9406">
										<i class="fab fa-facebook-f"></i>
									</a>
									{{-- <a href="#">
										<i class="fab fa-twitter"></i>
									</a> --}}
									<a href="https://github.com/Bancet6996">
										<i class="fab fa-github"></i>
									</a>
									{{-- <a href="#">
									  <i class="fab fa-behance"></i>
									</a> --}}
								</div>
								<p>Weeeeoooonnnnggg..... Weeeeeeeewwwww.... Weeeeeonggggg....
								</p>
								<div class="br"></div>
							</aside>
							<aside class="single_sidebar_widget popular_post_widget">
								<h3 class="widget_title">Postingan Terpopuler</h3>
									@foreach ($ob_visit->take(5) as $item)
									<div class="media post_item">
										<img src="{{ asset('uploads'. $item->thumbnail_berita) }}" alt="post" width="100px" height="60px">
										<div class="media-body">
											<a href="{{ url('user/news/' . $item->id . '/read') }}">
												<h3>{{ $item->judul_berita }}</h3>
											</a>
											<p>{{ Carbon\Carbon::parse($item->created_at)->diffForHumans() }}</p>
										</div>
									</div>
									@endforeach
								<div class="br"></div>
							</aside>
							{{-- <aside class="single_sidebar_widget ads_widget">
								<a href="#"><img class="img-fluid" src="{{ asset('aroma/img/blog/add.jpg') }}" alt=""></a>
								<div class="br"></div>
							</aside> --}}
							<aside class="single_sidebar_widget post_category_widget">
								<h4 class="widget_title">Kategori Berita</h4>
								<ul class="list cat-list">
									@foreach ($kategori as $item)
									<li>
										<a href="{{ url('/user/category/news', $item->nama_kategori) }}" class="d-flex justify-content-between">
											<p>{{ $item->nama_kategori }}</p>
											<p>{{ \App\Http\Controllers\NewsController::totalKategori($item->nama_kategori) }}</p>
										</a>
									</li>
									@endforeach
								</ul>
								<div class="br"></div>
							</aside>
							<aside class="single-sidebar-widget newsletter_widget">
								<h4 class="widget_title">Newsletter</h4>
								<p>Here, I focus on a range of items and features that we use in life without giving them a second thought.</p>
								<div class="form-group d-flex flex-row">
									<div class="input-group">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="fa fa-envelope" aria-hidden="true"></i></div>
										</div>
										<input type="text" class="form-control" id="inlineFormInputGroup" placeholder="Enter email address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter email'">
									</div>
									<a href="#" class="bbtns">Subcribe</a>
								</div>
								<p class="text-bottom">You can unsubscribe at any time</p>
								<div class="br"></div>
							</aside>
							<aside class="single-sidebar-widget tag_cloud_widget">
								<h4 class="widget_title">Kategori</h4>
								<ul class="list">
									@foreach ($kategori as $item)
										<li><a href="{{ url('/user/category/news', $item->nama_kategori) }}">{{ $item->nama_kategori }}</a></li>
									@endforeach
								</ul>
							</aside>
						</div>
					</div>
				</div>
			</div>
	</section>
	<!--================Blog Area =================-->
  
  <!--================Instagram Area =================-->
  <section class="instagram_area">
    <div class="container box_1620">
      <div class="insta_btn">
        <a class="btn theme_btn" href="#">Ikuti kami di Instagram</a>
      </div>
      <div class="instagram_image row m0">
        <a href="#"><img src="{{ asset('aroma/img/instagram/ins-1.jpg') }}" alt=""></a>
        <a href="#"><img src="{{ asset('aroma/img/instagram/ins-2.jpg') }}" alt=""></a>
        <a href="#"><img src="{{ asset('aroma/img/instagram/ins-3.jpg') }}" alt=""></a>
        <a href="#"><img src="{{ asset('aroma/img/instagram/ins-4.jpg') }}" alt=""></a>
        <a href="#"><img src="{{ asset('aroma/img/instagram/ins-5.jpg') }}" alt=""></a>
        <a href="#"><img src="{{ asset('aroma/img/instagram/ins-6.jpg') }}" alt=""></a>
      </div>
    </div>
  </section>
  <!--================End Instagram Area =================-->
  
    @include('user/footer')

    <script src="{{ asset('aroma/vendors/jquery/jquery-3.2.1.min.js') }}"></script>
	<script src="{{ asset('aroma/vendors/bootstrap/bootstrap.bundle.min.js') }}"></script>
	<script src="{{ asset('aroma/vendors/skrollr.min.js') }}"></script>
	<script src="{{ asset('aroma/vendors/owl-carousel/owl.carousel.min.js') }}"></script>
	<script src="{{ asset('aroma/vendors/nice-select/jquery.nice-select.min.js') }}"></script>
	<script src="{{ asset('aroma/vendors/jquery.ajaxchimp.min.js') }}"></script>
	<script src="{{ asset('aroma/vendors/mail-script.js') }}"></script>
	<script src="{{ asset('aroma/js/main.js') }}"></script>
	<script src="{{ asset('adminlte/dist/js/autosize.js') }}"></script>
	<script>
		autosize(document.querySelectorAll('textarea'));
	</script>
</body>
</html>