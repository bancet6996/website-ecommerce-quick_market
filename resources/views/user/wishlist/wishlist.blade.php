<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Quick Market - Daftar Harapan</title>
  <link rel="icon" href="{{ asset('about/QM Logo.png') }}" type="image/png">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/bootstrap/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/fontawesome/css/all.min.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/themify-icons/themify-icons.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/linericon/style.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/owl-carousel/owl.theme.default.min.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/owl-carousel/owl.carousel.min.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/nice-select/nice-select.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/nouislider/nouislider.min.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/css/style.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/css/scrollbar.css') }}">
</head>
<body>
  
    @include('user/header')

	<!-- ================ start banner area ================= -->	
	<section class="blog-banner-area" id="category">
		<div class="container h-100">
			<div class="blog-banner">
				<div class="text-center">
				    <h1>Daftar Harapan</h1>
				    <nav aria-label="breadcrumb" class="banner-breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="/user">Beranda</a></li>
              <li class="breadcrumb-item active" aria-current="page">Harapan</li>
            </ol>
          </nav>
				</div>
			</div>
    </div>
	</section>
	<!-- ================ end banner area ================= -->
  
  

  <!--================Cart Area =================-->
  @if(session()->has('message'))
    <div class="alert alert-info alert-dismissible fade show" role="alert">
      {{ session()->get('message') }}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  @endif
  <section class="cart_area">
      <div class="container">
          <div class="cart_inner">
              <div class="table-responsive">
                @if (Auth::check())
                @foreach ($wishlist as $nyeh)
                {{-- <form action="{{ url('/user/product/checkout/'. $nyeh->id) }}" method="POST"> --}}
                @endforeach
                    {{-- @csrf
                    @method('PATCH') --}}
                  <table class="table ">
                      <thead>
                          <tr>
                              <th scope="col">Produk</th>
                              <th scope="col">Harga</th>
                              <th scope="col"></th>
                              <th scope="col">Aksi</th>
                          </tr>
                      </thead>
                        <tbody>
                            @if (Auth::check())
                            @foreach ($wishlist as $row)
                            <tr>
                                <td>
                                    <div class="media">
                                        <div class="d-flex">
                                            <img src="{{ asset('uploads'. $row->gambar_produk) }}" class="img-fluid" style="min-height: 70px; min-width: 70px; max-height: 140px; max-width: 140px;">
                                        </div>
                                        <div class="media-body">
                                            <p>{{ $row->nama_produk }}</p>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <h5>Rp.{{ $row->harga_produk }}</h5>
                                </td>
                                <td>

                                </td>
                                {{-- <td>
                                    <h5 id="total">Rp.{{ $row->harga_produk }}</h5>
                                    <p id="total" value="29.99">29.99</p>
                                </td>  --}}
                                <td>
                                    <form action="{{ url('user/product/'.$row->id.'/wish/remove') }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger btn-block" type="submit" style="border-radius: 50px">Hapus</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                            @else
                                
                            @endif
                            {{-- <tr class="bottom_button">
                                <td>
                                    <a class="button" href="#">Update Cart</a>
                                </td>
                                <td>

                                </td>
                                <td>

                                </td>
                                <td>
                                    <div class="cupon_text d-flex align-items-center">
                                        <input type="text" placeholder="Coupon Code">
                                        <a class="primary-btn" href="#">Apply</a>
                                        <a class="button" href="#">Have a Coupon?</a>
                                    </div>
                                </td>
                            </tr> --}}
                            {{-- <tr>
                                <td>

                                </td>
                                <td>

                                </td>
                                <td>
                                    <h5>Subtotal</h5>
                                </td>
                                <td>
                                    <h5>$2160.00</h5>
                                </td>
                            </tr>
                            <tr class="shipping_area">
                                <td class="d-none d-md-block">

                                </td>
                                <td>

                                </td>
                                <td>
                                    <h5>Shipping</h5>
                                </td>
                                <td>
                                    <div class="shipping_box">
                                        <ul class="list">
                                            <li><a href="#">Flat Rate: $5.00</a></li>
                                            <li><a href="#">Free Shipping</a></li>
                                            <li><a href="#">Flat Rate: $10.00</a></li>
                                            <li class="active"><a href="#">Local Delivery: $2.00</a></li>
                                        </ul>
                                        <h6>Calculate Shipping <i class="fa fa-caret-down" aria-hidden="true"></i></h6>
                                        <select class="shipping_select">
                                            <option value="1">Bangladesh</option>
                                            <option value="2">India</option>
                                            <option value="4">Pakistan</option>
                                        </select>
                                        <select class="shipping_select">
                                            <option value="1">Select a State</option>
                                            <option value="2">Select a State</option>
                                            <option value="4">Select a State</option>
                                        </select>
                                        <input type="text" placeholder="Postcode/Zipcode">
                                        <a class="gray_btn" href="#">Update Details</a>
                                    </div>
                                </td>
                            </tr> --}}
                            <tr class="out_button_area">
                                <td class="d-none-l">

                                </td>
                                <td class="">

                                </td>
                                <td>

                                </td>
                                <td>
                                    <div class="checkout_btn_inner d-flex align-items-center">
                                        <a class="gray_btn" href="category">Lanjut berbelanja</a>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                  </table>
                {{-- </form> --}}
                @else
                    
                @endif
              </div>
          </div>
      </div>
  </section>
  <!--================End Cart Area =================-->



  @include('user/footer')



  <script src="{{ asset('aroma/vendors/jquery/jquery-3.2.1.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/bootstrap/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/skrollr.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/owl-carousel/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/nice-select/jquery.nice-select.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/jquery.ajaxchimp.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/mail-script.js') }}"></script>
  <script src="{{ asset('aroma/js/main.js') }}"></script>
  <script>
    $(document).ready(function() {
        var price = parseFloat($("#total").text());        
        function updatePrice() {
            var sum, num = parseInt($("#quantity").val(),10);
            console.log(num);
            if(num) {
                sum = num * price;
                $("#total").text(sum.toFixed(2));
            }
        }
        $(document).on("change, mouseup, keyup", "#quantity", updatePrice);
    });
  </script>
</body>
</html>