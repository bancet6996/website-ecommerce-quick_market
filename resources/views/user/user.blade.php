@extends('layouts.app')

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Quick Market - Beranda</title>
  {{-- <style>
    .owl-next, .owl-prev{
      visibility: hidden;
    }
  </style> --}}
  <link rel="icon" href="{{ asset('about/QM Logo.png') }}" type="image/png">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/bootstrap/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/fontawesome/css/all.min.css') }}">
	<link rel="stylesheet" href="{{ asset('aroma/vendors/themify-icons/themify-icons.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/nice-select/nice-select.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/owl-carousel/owl.theme.default.min.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/owl-carousel/owl.carousel.min.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/css/style.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/css/scrollbar.css') }}">
</head>
<body class="scrollbar scrollbar-black bordered-black square">
    <div class="force-overflow"></div>
  
    @include('user/header')

    <main class="site-main">

    @if(session()->has('message'))
    <div class="alert alert-info alert-dismissible fade show" role="alert">
      {{ session()->get('message') }}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    @endif
    
    @include('user/banner')

    @include('user/carousel')

    @include('user/trending')

    @include('user/offer')

    @include('user/selling')

    @include('user/section')  

    @include('user/subscribe')    

  </main>

  @include('user/footer')

  <script src="{{ asset('aroma/vendors/jquery/jquery-3.2.1.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/bootstrap/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/skrollr.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/owl-carousel/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/jquery-mousewheel-master/jquery.mousewheel.js') }}"></script>
  <script src="{{ asset('aroma/vendors/nice-select/jquery.nice-select.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/jquery.ajaxchimp.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/mail-script.js') }}"></script>
  <script src="{{ asset('aroma/js/main.js') }}"></script>
  <script>
    // var owl = $('.owl-carousel');
    // owl.owlCarousel({
    // loop:true,
    // nav:true,
    // margin:10,
    // autoplay:true,
    // autoplayTimeout:2100,
    // autoplayHoverPause:true,
    // responsive:{
    //     0:{
    //       items:1
    //     },
    //     600:{
    //       items:2
    //     },            
    //     960:{
    //       items:3
    //     },
    //     1200:{
    //       items:4
    //     }
    //   }
    // });
    // owl.on('mousewheel', '.owl-stage', function (e) {
    //   if (e.deltaY>0) {
    //     owl.trigger('next.owl');
    //   } else {
    //     owl.trigger('prev.owl');
    //   }
    //   e.preventDefault();
    // });
  </script>
</body>
</html>