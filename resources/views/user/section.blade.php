<!-- ================ Blog section start ================= -->
<style>
  #horizontal{
    display: inline-block;
  }
  #padding{
    padding: 14px;
  }
</style>  
<section class="blog">
  <div class="container">
    <div class="section-intro pb-60px">
      <p>Jenuh belanja ? Yuk baca berita</p>
      <h2>Berita <span class="section-intro__style">Terbaru</span></h2>
    </div>
    @foreach ($list_berita->take(3) as $row)
      <div id="horizontal" class="card border-white" style="width: 21rem;">
        <div class="card card-blog">
          <div class="card-blog__img">
              <img src="{{ asset('uploads'. $row->thumbnail_berita) }}" height="210px" class="card-img-top">
          </div>
          <div id="padding" class="card-body">
            <ul class="card-blog__info">
              <li>Oleh {{ $row->penulis }}</li>
              <li><i class="ti-comments-smiley"></i>{{ \App\Http\Controllers\NewsController::totalKomentar($row->id) }} Komentar</li>
            </ul>
            <h4 class="card-blog__title"><a href="{{ url('user/news/' . $row->id . '/read') }}">{{ $row->judul_berita }}</a></h4>
            <p>{{ $row->caption_berita }}</p>
            <a class="card-blog__link" href="{{ url('user/news/' . $row->id . '/read') }}">Baca Selengkapnya<i class="ti-arrow-right"></i></a>
          </div>
        </div>
      </div>
    @endforeach
  </div>
</section>
<!-- ================ Blog section end ================= -->