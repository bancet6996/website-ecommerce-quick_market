<!-- ================ trending product section start ================= -->  
<section class="section-margin calc-60px">
  <div class="container">
    <div class="section-intro pb-60px">
      <p>Produk yang populer</p>
      <h2>Produk <span class="section-intro__style">Trending</span></h2>
    </div>
    <div class="row">
      @foreach ($trending->take(4) as $row)
      @if (Auth::check())
        <div class="col-md-6 col-lg-4 col-xl-3 row-list"> 
          <div class="card text-center card-product">
            <div class="card-product__img">
              <img class="img-fluid" src="{{ asset('uploads'. $row->gambar_produk) }}" alt="" style="width: 263px; height:280px">
              <ul class="card-product__imgOverlay">
                <li><button style="cursor: default"><a href="{{ url('user/product/' .$row->id. '/info') }}"><i class="ti-search"></i></a></button></li>
                <li>
                  @if (\App\Http\Controllers\CartController::cartExist($row->id))
                    <form action="{{ url('user/product/'.$row->id.'/remove')}}" method="POST">
                      @method('DELETE')
                      @csrf
                      <button style="cursor: default" type="submit" @if($row->pemilik === auth()->user()->name) hidden @endif><i style="cursor: pointer" class="ti-close"></i></button>
                    </form>
                  @else
                    <form action="{{ url('user/product/'.$row->id.'/add') }}" method="POST">
                      @csrf
                      <button style="cursor: default" type="submit" @if($row->pemilik === auth()->user()->name) hidden @endif><i style="cursor: pointer" class="ti-shopping-cart"></i></button>
                    </form>
                  @endif
                </li>
                <li>
                  @if (\App\Http\Controllers\WishlistController::wishExist($row->id))
                  <form action="{{ url('user/product/'.$row->id.'/wish/remove') }}" method="POST">
                    @method('DELETE')
                    @csrf
                    <button style="cursor: default" type="submit" @if($row->pemilik === auth()->user()->name) hidden @endif><i style="cursor: pointer" class="ti-heart-broken"></i></button>
                  </form>
                  @else
                  <form action="{{ url('user/product/'.$row->id.'/wish') }}" method="POST">
                    @csrf
                    <button style="cursor: default" type="submit" @if($row->pemilik === auth()->user()->name) hidden @endif><i style="cursor: pointer" class="ti-heart"></i></button>
                  </form>  
                  @endif
                </li>
              </ul>
            </div>
            <div class="card-body">
              <p style="font-size: 8.4px">{{ $row->pemilik }}</p>
              <p>{{ $row->kategori_produk }}</p>
              <h4 class="card-product__title"><a href="{{ url('user/product/' .$row->id. '/info') }}">{{ $row->nama_produk }}</a></h4>
              <p class="card-product__price">Rp.{{ $row->harga_produk }}</p>
            </div>
          </div>
        </div>
      @else
      <div class="col-md-6 col-lg-4 col-xl-3 row-list"> 
          <div class="card text-center card-product">
            <div class="card-product__img">
              <img class="img-fluid" src="{{ asset('uploads'. $row->gambar_produk) }}" alt="" style="width: 263px; height:280px">
              <ul class="card-product__imgOverlay">
                <li><button style="cursor: default"><a href="{{ url('user/product/' .$row->id. '/info') }}"><i class="ti-search"></i></a></button></li>
                <li><button style="cursor: default" type="submit"><a href="{{ url('user/product/'.$row->id. '/add/session') }}"><i style="cursor: pointer" class="ti-shopping-cart"></i></a></button></li>
                <li>
                  @if (\App\Http\Controllers\WishlistController::wishExist($row->id))
                  <form action="{{ url('user/product/'.$row->id.'/wish/remove') }}" method="POST">
                    @method('DELETE')
                    @csrf
                    <button style="cursor: default" type="submit"><i style="cursor: pointer" class="ti-heart-broken"></i></button>
                  </form>
                  @else
                  <form action="{{ url('user/product/'.$row->id.'/wish') }}" method="POST">
                    @csrf
                    <button style="cursor: default" type="submit"><i style="cursor: pointer" class="ti-heart"></i></button>
                  </form>  
                  @endif
                </li>
              </ul>
            </div>
            <div class="card-body">
              <p style="font-size: 8.4px">{{ $row->pemilik }}</p>
              <p>{{ $row->kategori_produk }}</p>
              <h4 class="card-product__title"><a href="{{ url('user/product/' .$row->id. '/info') }}">{{ $row->nama_produk }}</a></h4>
              <p class="card-product__price">Rp.{{ $row->harga_produk }}</p>
            </div>
          </div>
        </div>
      @endif
      @endforeach
    </div>
  </div>
</section>
<!-- ================ trending product section end ================= -->  