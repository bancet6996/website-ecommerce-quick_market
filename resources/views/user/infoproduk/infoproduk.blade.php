<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Quick Market - Detail Produk</title>
	<style>
		.rating {
			direction: rtl;
			unicode-bidi: bidi-override;
			color: #ddd; /* Personal choice */
		}
		.rating input {
			display: none;
		}
		.rating label:hover,
		.rating label:hover ~ label,
		.rating input:checked + label,
		.rating input:checked + label ~ label {
			color: #fbd600; /* Personal color choice. Lifted from Bootstrap 4 */
		}
		.njay{
			color: #fbd600; /* Personal color choice. Lifted from Bootstrap 4 */
		}
	</style>
	<link rel="icon" href="{{ asset('about/QM Logo.png') }}" type="image/png">
    <link rel="stylesheet" href="{{ asset('aroma/vendors/bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('aroma/vendors/fontawesome/css/all.min.css') }}">
	<link rel="stylesheet" href="{{ asset('aroma/vendors/themify-icons/themify-icons.css') }}">
	<link rel="stylesheet" href="{{ asset('aroma/vendors/linericon/style.css') }}">
    <link rel="stylesheet" href="{{ asset('aroma/vendors/nice-select/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('aroma/vendors/owl-carousel/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('aroma/vendors/owl-carousel/owl.carousel.min.css') }}">
	<link rel="stylesheet" href="{{ asset('aroma/vendors/summernote/summernote-bs4.css') }}">
	<link rel="stylesheet" href="{{ asset('aroma/css/style.css') }}">
  	<link rel="stylesheet" href="{{ asset('aroma/css/scrollbar.css') }}">
</head>
<body>

	@include('user/header')
	
	<!-- ================ start banner area ================= -->	
	<section class="blog-banner-area" id="blog">
		<div class="container h-100">
			<div class="blog-banner">
				<div class="text-center">
					<h1>Detail Produk</h1>
					<nav aria-label="breadcrumb" class="banner-breadcrumb">
                        <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/user">Beranda</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Detail Produk</li>
                        </ol>
                    </nav>
				</div>
			</div>
        </div>
	</section>
	<!-- ================ end banner area ================= -->


  <!--================Single Product Area =================-->
	<div class="product_image_area">
		<div class="container">
			@if(session()->has('message'))
			<div class="alert alert-info alert-dismissible fade show" role="alert">
			  {{ session()->get('message') }}
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			  </button>
			</div>
			@endif
			@if ($errors->any())
				<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
				</div>
			@endif
			<div class="row s_product_inner">
				<div class="col-lg-6">
					<div class="owl-carousel owl-theme s_Product_carousel">
						<div class="single-prd-item">
                            <img src="{{ asset('uploads'. $produk->gambar_produk) }}" class="img-fluid">
						</div>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1">
					<div class="s_product_text">
						<h3>{{ $produk->nama_produk }}</h3>
						<h2>Rp.{{ $produk->harga_produk }}</h2>
						<ul class="list">
							<li><a class="active" href="#"><span>Kategori</span> : {{ $produk->kategori_produk }}</a></li>
							<li><a href="#"><span>Stok</span> : {{ $produk->stok_produk }} Stok</a></li>
						</ul>
						<p>{{ $produk->deskripsi_produk }}</p>
						<br>
						@if (Auth::check())
							@if ($produk->stok_produk > 0)
							<form action="{{ url('user/product/'.$produk->id.'/add') }}" method="POST" @if($produk->pemilik === auth()->user()->name) hidden @endif>
								@csrf
								<div class="product_count">
									<label for="qty">Quantity:</label>
									<input min="0" type="text" name="qty" id="sst" size="2" maxlength="12" value="1" title="Quantity:" class="input-text qty">
								</div>
								<br>
								<button id="product_count_button" onclick="var result = document.getElementById('sst'); var sst = result.value; if( !isNaN( sst ) &amp;&amp; sst > 0 ) result.value--;return false;" class="reduced items-count" type="button"><i class="ti-angle-left"></i></button>
								<button id="product_count_button" onclick="var result = document.getElementById('sst'); var sst = result.value; if( !isNaN( sst )) result.value++;return false;" class="increase items-count" type="button"><i class="ti-angle-right"></i></button>
								<br><br>
								<button type="submit" class="button btn-primary">Tambahkan ke keranjang</button>
							</form>
							@else
								
							@endif
						@else
							@if ($produk->stok_produk > 0)
								<a href="{{ url('user/product/'.$produk->id. '/add/session') }}" class="button btn-primary">Tambahkan ke keranjang</a>
							@else
								
							@endif
						@endif
						<div class="card_area d-flex align-items-center">
							<form action="{{ url('user/product/'.$produk->id.'/wish') }}" method="post">
								@csrf
								<button type="submit" class="icon_btn" style="border-style: none" @if(Auth::check() && $produk->pemilik === auth()->user()->name) hidden @endif><i class="lnr lnr lnr-heart"></i></button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--================End Single Product Area =================-->

	<!--================Product Description Area =================-->
	<section class="product_description_area">
		<div class="container">
			<ul class="nav nav-tabs" id="myTab" role="tablist">
				<li class="nav-item">
					<a class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Deskripsi</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile"
					 aria-selected="false">Spesifikasi</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact"
					 aria-selected="false">Komentar</a>
				</li>
				<li class="nav-item">
					<a class="nav-link active" id="review-tab" data-toggle="tab" href="#review" role="tab" aria-controls="review"
					 aria-selected="false">Ulasan</a>
				</li>
			</ul>
			<div class="tab-content" id="myTabContent">
				@if ($produk->deskripsi_lengkap_produk === null)
				<div class="tab-pane fade" id="home" role="tabpanel" aria-labelledby="home-tab">
					<p>{{ $produk->deskripsi_produk }}</p>
				</div>
				@else
				<div class="tab-pane fade" id="home" role="tabpanel" aria-labelledby="home-tab">
					<p>{{ $produk->deskripsi_lengkap_produk }}</p>
				</div>
				@endif
				<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
					<div class="table-responsive">
						<table class="table">
							<tbody>
								<tr>
									<td>
										<h5>Lebar</h5>
									</td>
									<td>
										@if ($produk->lebar_produk === null)
											<h5>Tidak didefinisikan</h5>
										@else
											<h5>{{ $produk->lebar_produk }}</h5>
										@endif
									</td>
								</tr>
								<tr>
									<td>
										<h5>Tinggi</h5>
									</td>
									<td>
										@if ($produk->tinggi_produk === null)
											<h5>Tidak didefinisikan</h5>
										@else
											<h5>{{ $produk->tinggi_produk }}</h5>
										@endif
									</td>
								</tr>
								<tr>
									<td>
										<h5>Ketebalan</h5>
									</td>
									<td>
										@if ($produk->ketebalan_produk === null)
											<h5>Tidak didefinisikan</h5>
										@else
											<h5>{{ $produk->ketebalan_produk }}</h5>
										@endif
									</td>
								</tr>
								<tr>
									<td>
										<h5>Berat</h5>
									</td>
									<td>
										@if ($produk->berat_produk === null)
											<h5>Tidak didefinisikan</h5>
										@else
											<h5>{{ $produk->berat_produk }} gram</h5>
										@endif
									</td>
								</tr>
								{{-- <tr>
									<td>
										<h5>Quality checking</h5>
									</td>
									<td>
										<h5>yes</h5>
									</td>
								</tr>
								<tr>
									<td>
										<h5>Freshness Duration</h5>
									</td>
									<td>
										<h5>03days</h5>
									</td>
								</tr>
								<tr>
									<td>
										<h5>When packeting</h5>
									</td>
									<td>
										<h5>Without touch of hand</h5>
									</td>
								</tr>
								<tr>
									<td>
										<h5>Each Box contains</h5>
									</td>
									<td>
										<h5>60pcs</h5>
									</td>
								</tr> --}}
							</tbody>
						</table>
					</div>
				</div>
				<div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
					<div class="row">
						<div class="col-lg-6">
							<div class="comment_list">
								@foreach ($komentar as $nyeh)
								<div class="review_item card border-light">
									<div class="media">
										<div class="d-flex">
											@if ( $nyeh->provider != 'google')
												<img src="{{ asset('uploads'.$nyeh->image) }}" style="width: 70px; height: 70px; border-radius: 50%;" @if($nyeh->image === null) hidden @endif>
											@else
												<img src="{{ $nyeh->image }}" style="width: 70px; height: 70px; border-radius: 50%;" @if($nyeh->image === null) hidden @endif>
											@endif
										</div>
										<div class="media-body">
											<h4>{{ $nyeh->name }}</h4>
											{{-- <h5>12th Feb, 2018 at 05:56 pm</h5> --}}
											<h5>{{ Carbon\Carbon::parse($nyeh->created_at)->diffForHumans() }}</h5>
										</div>
										@if (Auth::check())
											@if ($nyeh->name === auth()->user()->name)
												<form action="{{ url('/user/product/comment/'. $nyeh->id .'/delete') }}" method="post">
													@method('PATCH')
													@csrf
													<button type="submit" class="btn btn-link" style="text-decoration: none; font-size: 14px">Hapus</button>
												</form>
											@else
												
											@endif
										@else	

										@endif
									</div>
									<p>{!! $nyeh->comment !!}</p>
								</div>
								@endforeach
							</div>
						</div>
						<div class="col-lg-6">
							<div class="review_box">
								<h4>Kirim komentar</h4>
								<form class="row contact_form" action="{{ url('/user/product/'. $produk->id .'/info/comment') }}" method="POST" id="contactForm" novalidate="novalidate">
									@csrf
									@if (Auth::check())
									<div class="col-md-12">
										<div class="form-group">
											<textarea class="form-control" name="komentar" id="message" rows="7" cols="28" placeholder="Komentar"></textarea>
										</div>
									</div>
									<div class="col-md-12 text-right">
										<button type="submit" value="submit" class="btn primary-btn">Kirim</button>
									</div>
									@else
										<h5>Ingin berkomentar ? Cus <a href="/login">Login</a></h5>
									@endif									
								</form>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane fade show active" id="review" role="tabpanel" aria-labelledby="review-tab">
					<div class="row">
						@if(Auth::check())
						<div @if( \App\Http\Controllers\PesananController::rating(auth()->user()->name, $produk->id)) class="col-lg-6" @else class="col-lg-12" @endif>
							<div class="row total_rate">
								<div class="col-6">
									<div class="box_total">
										<h5>Rata-rata</h5>
										@if ($avg_rating === null)
											<h4>0</h4>											
										@else
											<h4>{{ number_format($avg_rating,1) }}</h4>											
										@endif
										<h6>({{ $total_rating }} Ulasan)</h6>
									</div>
								</div>
								<div class="col-6">
									<div class="rating_list">
										<h3>Berdasarkan {{ $total_rating }} Ulasan</h3>
										<table style="width: 100%">
											<tr>
												<td>1</td>
												<td><i class="fa fa-star njay"></i></td>
												<td>{{ $total_rating_1 }}</td>
											</tr>
											<tr>
												<td>2</td>
												<td><i class="fa fa-star njay"></i><i class="fa fa-star njay"></i></td>
												<td>{{ $total_rating_2 }}</td>
											</tr>
											<tr>
												<td>3</td>
												<td><i class="fa fa-star njay"></i><i class="fa fa-star njay"></i><i class="fa fa-star njay"></i></td>
												<td>{{ $total_rating_3 }}</td>
											</tr>
											<tr>
												<td>4</td>
												<td><i class="fa fa-star njay"></i><i class="fa fa-star njay"></i><i class="fa fa-star njay"></i><i class="fa fa-star njay"></i></td>
												<td>{{ $total_rating_4 }}</td>
											</tr>
											<tr>
												<td>5</td>
												<td><i class="fa fa-star njay"></i><i class="fa fa-star njay"></i><i class="fa fa-star njay"></i><i class="fa fa-star njay"></i><i class="fa fa-star njay"></i></td>
												<td>{{ $total_rating_5 }}</td>
											</tr>
										</table>
										{{-- <ul class="list">
											<li><a href="#">1 <i class="fa fa-star njay"></i> {{ $total_rating_1 }}</a></li>
											<li><a href="#">2 <i class="fa fa-star njay"></i><i class="fa fa-star njay"></i> {{ $total_rating_2 }}</a></li>
											<li><a href="#">3 <i class="fa fa-star njay"></i><i class="fa fa-star njay"></i><i class="fa fa-star njay"></i> {{ $total_rating_3 }}</a></li>
											<li><a href="#">4 <i class="fa fa-star njay"></i><i class="fa fa-star njay"></i><i class="fa fa-star njay"></i><i class="fa fa-star njay"></i> {{ $total_rating_4 }}</a></li>
											<li><a href="#">5 <i class="fa fa-star njay"></i><i class="fa fa-star njay"></i><i class="fa fa-star njay"></i><i class="fa fa-star njay"></i><i class="fa fa-star njay"></i> {{ $total_rating_5 }}</a></li>
										</ul> --}}
									</div>
								</div>
							</div>
							<div class="review_list">
								@foreach ($rating as $item)
								<div class="review_item">
									<div class="media">
										<div class="d-flex">
											@if ($item->provider != 'google')
												<img src="{{ asset('uploads'. $item->image) }}" alt="" style="width: 70px; height: 70px; border-radius: 50%;" @if($item->image === null) hidden @endif>												
											@else
												<img src="{{ $nyeh->image }}" style="width: 70px; height: 70px; border-radius: 50%;" @if($nyeh->image === null) hidden @endif>
											@endif
										</div>
										<div class="media-body">
											<h4>{{ $item->name }}</h4>
											@for ($i = 0; $i < $item->rating; $i++)
												<i class="fa fa-star aria-hidden="true"></i>
											@endfor
											{{-- <i class="fa fa-star njay"></i> --}}
										</div>
									</div>
									<p>{!! $item->comment !!}</p>
								</div>
								@endforeach
							</div>
						</div>
						@else
						<div class="col-lg-12">
							<div class="row total_rate">
								<div class="col-6">
									<div class="box_total">
										<h5>Rata-rata</h5>
										@if ($avg_rating === null)
											<h4>0</h4>											
										@else
											<h4>{{ number_format($avg_rating,1) }}</h4>											
										@endif
										<h6>({{ $total_rating }} Ulasan)</h6>
									</div>
								</div>
								<div class="col-6">
									<div class="rating_list">
										<h3>Berdasarkan {{ $total_rating }} Ulasan</h3>
										<table style="width: 100%">
											<tr>
												<td>1</td>
												<td><i class="fa fa-star njay"></i></td>
												<td>{{ $total_rating_1 }}</td>
											</tr>
											<tr>
												<td>2</td>
												<td><i class="fa fa-star njay"></i><i class="fa fa-star njay"></i></td>
												<td>{{ $total_rating_2 }}</td>
											</tr>
											<tr>
												<td>3</td>
												<td><i class="fa fa-star njay"></i><i class="fa fa-star njay"></i><i class="fa fa-star njay"></i></td>
												<td>{{ $total_rating_3 }}</td>
											</tr>
											<tr>
												<td>4</td>
												<td><i class="fa fa-star njay"></i><i class="fa fa-star njay"></i><i class="fa fa-star njay"></i><i class="fa fa-star njay"></i></td>
												<td>{{ $total_rating_4 }}</td>
											</tr>
											<tr>
												<td>5</td>
												<td><i class="fa fa-star njay"></i><i class="fa fa-star njay"></i><i class="fa fa-star njay"></i><i class="fa fa-star njay"></i><i class="fa fa-star njay"></i></td>
												<td>{{ $total_rating_5 }}</td>
											</tr>
										</table>
										{{-- <ul class="list">
											<li><a href="#">1 <i class="fa fa-star njay"></i> {{ $total_rating_1 }}</a></li>
											<li><a href="#">2 <i class="fa fa-star njay"></i><i class="fa fa-star njay"></i> {{ $total_rating_2 }}</a></li>
											<li><a href="#">3 <i class="fa fa-star njay"></i><i class="fa fa-star njay"></i><i class="fa fa-star njay"></i> {{ $total_rating_3 }}</a></li>
											<li><a href="#">4 <i class="fa fa-star njay"></i><i class="fa fa-star njay"></i><i class="fa fa-star njay"></i><i class="fa fa-star njay"></i> {{ $total_rating_4 }}</a></li>
											<li><a href="#">5 <i class="fa fa-star njay"></i><i class="fa fa-star njay"></i><i class="fa fa-star njay"></i><i class="fa fa-star njay"></i><i class="fa fa-star njay"></i> {{ $total_rating_5 }}</a></li>
										</ul> --}}
									</div>
								</div>
							</div>
							<div class="review_list">
								@foreach ($rating as $item)
								<div class="review_item">
									<div class="media">
										<div class="d-flex">
											@if ($item->provider != 'google')
												<img src="{{ asset('uploads'. $item->image) }}" alt="" style="width: 70px; height: 70px; border-radius: 50%;" @if($item->image === null) hidden @endif>												
											@else
												<img src="{{ $nyeh->image }}" style="width: 70px; height: 70px; border-radius: 50%;" @if($nyeh->image === null) hidden @endif>
											@endif
										</div>
										<div class="media-body">
											<h4>{{ $item->name }}</h4>
											@for ($i = 0; $i < $item->rating; $i++)
												<i class="fa fa-star aria-hidden="true"></i>
											@endfor
											{{-- <i class="fa fa-star njay"></i> --}}
										</div>
									</div>
									<p>{!! $item->comment !!}</p>
								</div>
								@endforeach
							</div>
						</div>
						@endif
						@if(Auth::check())
							@if( \App\Http\Controllers\PesananController::rating(auth()->user()->name, $produk->id))
							<form action="{{ url('/user/product/'. $produk->id .'/info/rating') }}" method="post" class="form-contact form-review mt-3">
								@csrf
								<div class="col-lg-6">
									<div class="review_box">
										<h4>Buat Ulasan</h4>
										<p>Penilaian :</p>
										<br>
										<ul class="list">
											<div class="rating rating2">
												<div class="rating" style="width: 20rem">
													<input id="rating-5" type="radio" name="rating" value="5"/><label for="rating-5"><i class="fa fa-star"></i></label>
													<input id="rating-4" type="radio" name="rating" value="4"/><label for="rating-4"><i class="fa fa-star"></i></label>
													<input id="rating-3" type="radio" name="rating" value="3"/><label for="rating-3"><i class="fa fa-star"></i></label>
													<input id="rating-2" type="radio" name="rating" value="2"/><label for="rating-2"><i class="fa fa-star"></i></label>
													<input id="rating-1" type="radio" name="rating" value="1"/><label for="rating-1"><i class="fa fa-star"></i></label>
												</div>
											</div>
										</ul>
										<div class="form-group">
											<textarea class="summernote" name="ulasan" id="textarea" cols="30" rows="7" placeholder="Enter Message"></textarea>
										</div>
										<div class="form-group">
											<button type="submit" class="button button--active button-review" style="margin-left: 390px">Kirim</button>
										</div>
									</div>
								</div>
							</form>
							@endif
						@endif
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================End Product Description Area =================-->

	@include('user/topproduk')
	
	<br><br><br><br><br>

    @include('user/footer')

  <script src="{{ asset('aroma/vendors/jquery/jquery-3.2.1.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/bootstrap/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/skrollr.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/owl-carousel/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/nice-select/jquery.nice-select.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/jquery.ajaxchimp.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/mail-script.js') }}"></script>
  <script src="{{ asset('aroma/vendors/summernote/summernote-bs4.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/summernote/lang/summernote-id-ID.js') }}"></script>
  <script src="{{ asset('aroma/vendors/summernote/lang/summernote-id-ID.min.js') }}"></script>
  <script src="{{ asset('aroma/js/main.js') }}"></script>
  <script src="{{ asset('adminlte/dist/js/autosize.js') }}"></script>
  <script>
	autosize(document.querySelectorAll('textarea'));
	$(function () {
    	// Summernote
		$('.summernote').summernote({
		height: 210,
		width: 504,
		lang: "id-ID",
		toolbar: [
			// [groupName, [list of button]]
			['style', ['bold', 'italic', 'underline', 'clear']],
			['insert', ['link', 'picture', 'video',]],
			['view', ['undo', 'redo', 'help']],
			['mybutton', ['hello']],
		],
		buttons: {
			hello: HelloButton,
		}
		});
	  });
	  
	  var HelloButton = function (context) {
    var ui = $.summernote.ui;

    // create button
    var button = ui.button({
      contents: '<i class="fa fa-level-down-alt"/> Masalah "Enter Key"',
      tooltip: 'Tekan "SHIFT + Enter" untuk menambahkan baris baru',
      click: function () {
        // invoke insertText method with 'hello' on editor module.
        context.invoke('editor.insertText', 'hello');
      }
    });

    return button.render();   // return button as jquery object
  }
  </script>
</body>
</html>