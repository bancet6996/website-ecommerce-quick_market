<!-- ================ top product area start ================= -->	
<section class="related-product-area">
	<div class="container">
		<div class="section-intro pb-60px">
            <p>Produk yang populer</p>
            <h2>Top <span class="section-intro__style">Produk</span></h2>
        </div>
		<div class="row mt-30">
            @foreach ($top->take(12) as $item)
                <div class="col-sm-6 col-xl-3 mb-4 mb-xl-0">
                    <br>
                    <div class="single-search-product-wrapper">
                        <div class="single-search-product d-flex">
                            <a href="{{ url('user/product/' .$item->id. '/info') }}"><img src="{{ asset('uploads'. $item->gambar_produk) }}" class="img-fluid"></a>
                            <div class="desc">
                                <a href="{{ url('user/product/' .$item->id. '/info') }}" class="title">{{ $item->nama_produk }}</a>
                                <div class="price">Rp.{{ $item->harga_produk }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
	</div>
</section>
<!-- ================ top product area end ================= -->	