<!--================ Hero banner start =================-->
{{-- <style>
  .carousel-item{
    transition: transform 20s ease, opacity .5s ease-out; 
  }
</style> --}}
<section class="hero-banner">
  <div class="container">
    <div class="row no-gutters pt-60px">
      <div class="col-12 d-none d-block">
        <div class="hero-banner__img" style="width: 100%; height: 700px">
          <div id="carousel" class="carousel slide" data-ride="carousel" data-interval="3500">
            <ol class="carousel-indicators">
              @foreach ($banner as $row)
              <li data-target="#carousel" data-slide-to="{{ isset($i) ? ++ $i : $i = 0 }}" class="@if($loop->first) active @endif"></li>
              @endforeach
            </ol>
            <div class="carousel-inner">
              @foreach ($banner as $row)
              <div class="carousel-item @if($loop->first) active @endif">
                <a href="{{ $row->tautan }}"><img height="630px" class="d-block w-100" src="{{ asset('uploads'. $row->gambar_banner) }}"></a>
                <div class="carousel-caption d-none d-md-block">
                  <h5 style="color: white">{{ $row->judul }}</h5>
                  <p style="color: white">{{ $row->keterangan }}</p>
                </div>
              </div>
              @endforeach
            </div>
            <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--================ Hero banner start =================-->