<!--================ Hero Carousel start =================-->
<section class="section-margin mt-0">
  <div class="owl-carousel owl-theme hero-carousel">
    @foreach ($list_produk->take(7) as $row)
    <div class="hero-carousel__slide">
      <img src="{{ asset('uploads'. $row->gambar_produk) }}" alt="" class="img-fluid">
      <a href="{{ url('user/product/' .$row->id. '/info') }}" class="hero-carousel__slideOverlay">
        <p style="font-size: 9.8px">{{ $row->pemilik }}</p>
        <h3>{{ $row->nama_produk }}</h3>
        <p>{{ $row->kategori_produk }}</p>
      </a>
    </div>
    @endforeach
  </div>
</section>
<!--================ Hero Carousel end =================-->