<!--================ Start Header Menu Area =================-->
<header class="header_area">
  <div class="main_menu">
    <nav class="navbar navbar-expand-lg navbar-light">
      <div class="container">
        <a class="navbar-brand logo_h" href="/"><img src="{{ asset('about/QM Logo with text.png') }}" alt="" style="height: 49px"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
          <ul class="nav navbar-nav menu_nav ml-auto mr-auto">
            <li class="nav-item @if(Request::path() === '/' || Request::path() === 'user') active @endif"><a class="nav-link" href="/user">Beranda</a></li>
            <li class="nav-item submenu dropdown @if(Request::path() === 'user/category' || Request::path() === 'user/cart' || Request::path() === 'user/wishlist' || Request::path() === 'user/product/'. @$produk->id .'/info') active @endif">
              <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                aria-expanded="false">Belanja</a>
              <ul class="dropdown-menu">
                <li class="nav-item"><a class="nav-link" href="/user/category">Kategori</a></li>
                <li class="nav-item"><a class="nav-link" href="/user/cart">Keranjang</a></li>
                @if (Auth::check())
                <li class="nav-item"><a class="nav-link" href="/user/wishlist">Daftar Harapan</a></li>
                @endif
              </ul>
						</li>
          <li class="nav-item @if(Request::path() === 'user/news' || Request::path() === 'user/news/'. @$news->id .'/read') active @endif"><a class="nav-link" href="/user/news">Berita</a></li>
					</li>
          <li class="nav-item @if(Request::path() === 'user/contact') active @endif"><a class="nav-link" href="/user/contact">Kontak</a></li>
        </ul>

        @if (Auth::check())
          @if(session('cart'))
          <ul class="nav-shop">
            <li class="nav-item"><button><a href="/user/cart"><i class="ti-shopping-cart"></i><span class="nav-shop__circle">{{ $count_produk  + count(session('cart')) }}</span></a></button> </li>
          </ul>
          @else
          <ul class="nav-shop">
            <li class="nav-item"><button><a href="/user/cart"><i class="ti-shopping-cart"></i><span class="nav-shop__circle">{{ $count_produk }}</span></a></button> </li>
          </ul>
          @endif
        @else
          @if(session('cart'))
          <ul class="nav-shop">
            <li class="nav-item"><button><a href="/user/cart"><i class="ti-shopping-cart"></i><span class="nav-shop__circle">{{ count(session('cart')) }}</span></a></button> </li>
          </ul>
          @else
          <ul class="nav-shop">
            <li class="nav-item"><button><a href="/user/cart"><i class="ti-shopping-cart"></i><span class="nav-shop__circle">0</span></a></button> </li>
          </ul>
          @endif
        @endif

        <!-- Right Side Of Navbar -->
        <ul class="nav navbar-nav menu_nav ml-auto mr-auto">
        <!-- Authentication Links -->
        @guest
          <li class="nav-item @if(Request::path() === 'user/login') active @endif">
              <a class="nav-link" href="/user/login">Masuk</a>
          </li>
          @if (Route::has('register'))
            <li class="nav-item @if(Request::path() === 'user/register') active @endif">
                <a class="nav-link" href="/user/register">Daftar</a>
            </li>
          @endif
          @else
          <li class="nav-item submenu dropdown">
            @if (auth()->user()->provider != 'google')
              <img src="{{ asset('uploads'.auth()->user()->image) }}" style="width: 63px; height: 63px; border-radius: 50%;" @if(auth()->user()->image === null) hidden @endif>
            @else
              <img alt="{{Auth::user()->name}}" src="{{Auth::user()->image}}" style="width: 63px; height: 63px; border-radius: 50%;"/>
            @endif
            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
              aria-expanded="false">
              {{ Auth::user()->name }}
              <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
              <li class="nav-item" @if(auth()->user()->provider != 'admin' && auth()->user()->provider != 'superadmin') hidden @endif><a class="nav-link" href="/admin">Admin</a></li>
              <li class="nav-item"><a class="nav-link" href="{{ url('user/voucher') }}">Dompet</a></li>
              <li class="nav-item"><a class="nav-link" href="{{ url('user/setting/'.auth()->user()->name) }}">Pengaturan Profile</a></li>
              <li class="nav-item"><a class="nav-link" href="{{ url('user/address/'.auth()->user()->name) }}">Pengaturan Alamat</a></li>
              <li class="nav-item"><a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">{{ __('Keluar') }}</a></li>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
                </form>
            </ul>
          </li>
          @endguest
          </ul>
        </div>
      </div>
    </nav>
  </div>
</header>
<!--================ End Header Menu Area =================-->