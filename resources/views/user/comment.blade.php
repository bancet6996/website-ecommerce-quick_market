<div class="comments-area">
    <h4>{{ $total_komentar }} Komentar</h4>
    @foreach ($komentar as $row)
    <div class="comment-list">
        <div class="single-comment justify-content-between d-flex">
            <div class="user justify-content-between d-flex">
                <div class="thumb">
                    <img src="{{ asset('uploads'.$row->image) }}" style="width: 70px; height: 70px;" @if($row->image === null) hidden @endif>
                </div>
                <div class="desc">
                    <h5>{{ $row->name }}</h5>
                    <p class="date">{{ Carbon\Carbon::parse($row->created_at)->diffForHumans() }}</p>
                    <p class="comment">{{ $row->comment }}</p>
                </div>
            </div>
            @if (Auth::check() && $row->name === auth()->user()->name)
            <form action="{{ url('/user/news/comment/'. $row->id .'/delete') }}" method="post">
                @method('PATCH')
                @csrf
                <button type="submit" class="btn btn-link" style="text-decoration: none;">Hapus</button>
            </form>
            @else

            @endif
        </div>
    </div>
    @endforeach
</div>
<div class="comment-form">
    <h4>Kirim Komentar</h4>
    @if(session()->has('message'))
	<div class="alert alert-info alert-dismissible fade show" role="alert">
		{{ session()->get('message') }}
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
	@endif
	@if ($errors->any())
		<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>
		</div>
	@endif
    <form action="{{ url('/user/news/'. $news->id .'/read/comment') }}" method="POST">
        @csrf
        <div class="form-group">
            @if (Auth::check())                
                <textarea class="form-control mb-10" rows="5" name="komentar" placeholder="Komentar" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Messege'" required=""></textarea>
                <button type="submit" class="button button-postComment button--active">Kirim</button>
                @else
                <h6>Ingin berkomentar ? Cus <a href="/login">Login</a></h6>
            @endif
        </div>
    </form>
</div>