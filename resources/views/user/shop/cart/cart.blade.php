<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Quick Market - Keranjang</title>
  <style>
      tr, th, td{
        text-align: center;
      }
      .kuantitas{
        width: 77px;
        padding: 6px 12px 6px 10px;
        display: block;
        margin: auto;
      }
  </style>
    <link rel="icon" href="{{ asset('about/QM Logo.png') }}" type="image/png">
    <link rel="stylesheet" href="{{ asset('aroma/vendors/bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('aroma/vendors/fontawesome/css/all.min.css') }}">
	<link rel="stylesheet" href="{{ asset('aroma/vendors/themify-icons/themify-icons.css') }}">
	<link rel="stylesheet" href="{{ asset('aroma/vendors/linericon/style.css') }}">
    <link rel="stylesheet" href="{{ asset('aroma/vendors/owl-carousel/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('aroma/vendors/owl-carousel/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('aroma/vendors/nice-select/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('aroma/vendors/nouislider/nouislider.min.css') }}">
    <link rel="stylesheet" href="{{ asset('aroma/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('aroma/css/scrollbar.css') }}">
</head>
<body>
  
    @include('user/header')

	<!-- ================ start banner area ================= -->	
	<section class="blog-banner-area" id="category">
		<div class="container h-100">
			<div class="blog-banner">
				<div class="text-center">
				    <h1>Keranjang Produk</h1>
				    <nav aria-label="breadcrumb" class="banner-breadcrumb">
                        <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/user">Beranda</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Keranjang</li>
                        </ol>
                    </nav>
				</div>
			</div>
        </div>
	</section>
	<!-- ================ end banner area ================= -->
  
  

  <!--================Cart Area =================-->
  @if(session()->has('message'))
    <div class="alert alert-info alert-dismissible fade show" role="alert">
      {{ session()->get('message') }}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  @endif
  <section class="cart_area">
      <div class="container">
          <div class="cart_inner">
              <div class="table-responsive">
                @if (Auth::check())
                {{-- @foreach ($keranjang as $nyeh)
                <form action="{{ url('user/product/checkout/'. $nyeh->id) }}" method="POST">
                @endforeach
                @csrf
                @method('PATCH') --}}
                  <table class="table">
                      @if($cek_keranjang != 0 || session('cart') != 0)
                      <thead>
                          <tr>
                              @if(session('cart'))
                              <th scope="col">Tipe</th>
                              @endif
                              <th scope="col">Produk</th>
                              <th scope="col">Harga</th>
                              <th scope="col">Kuantitas</th>
                              <th scope="col">Subtotal</th>
                              <th scope="col">Aksi</th>
                          </tr>
                      </thead>
                      @else
                      <thead>
                          <tr>
                              <th scope="col" colspan="6" style="text-align: center">Ups ! Keranjang masih kosong, pilih-pilih dulu lha baru balik lagi ke sini</th>
                          </tr>
                      </thead>
                      @endif
                    <tbody>
                        @if(session('cart'))
                        @foreach(session('cart') as $id => $details)
                        {{-- Session --}}
                        <tr>
                            @if(session('cart'))
                            <td title="Item ini akan bertahan selama 2 jam setelah keluar dari situs ini">Session</td>
                            @endif
                            <td data-th="Product">
                                <div class="media">
                                    <div class="d-flex"><img src="{{ asset('uploads'.$details['gambar_produk']) }}" style="height: 140px; width: 140px;" class="img-responsive"/></div>
                                    <div class="media-body">{{ $details['nama_produk'] }}</div>
                                </div>
                            </td>
                            <td data-th="Price">Rp.{{ $details['harga_produk'] }}</td>
                            <td data-th="Quantity">
                                <input type="number" style="width: 77px; margin: auto;" min="1" max="{{ $details['stok_produk'] }}" value="{{ $details['kuantitas'] }}" class="form-control quantity"/>
                            </td>
                            <td data-th="Subtotal" class="text-center">Rp.{{ $details['harga_produk'] * $details['kuantitas'] }}</td>
                            <td class="actions" data-th="">
                                <button class="btn btn-info btn-sm update-cart" data-id="{{ $id }}"><i class="fa fa-sync"></i></button>
                                <button class="btn btn-danger btn-sm remove-from-cart" data-id="{{ $id }}"><i class="fa fa-trash-alt"></i></button>
                            </td>
                        </tr>
                        @endforeach
                        @endif
                        {{-- End Session --}}
                        @foreach ($keranjang as $row)
                        {{-- Database --}}
                        <tr>
                            @if(session('cart'))
                            <td title="Item ini akan bertahan selamanya selama item ini tidak dihapus">Database</td>
                            @endif
                            <td>
                                <div class="media">
                                    <div class="d-flex">
                                        <img src="{{ asset('uploads'. $row->gambar_produk) }}" class="img-fluid" style="height: 140px; width: 140px;">
                                    </div>
                                    <div class="media-body">
                                        <p>{{ $row->nama_produk }}</p>
                                    </div>
                                </div>
                            </td>
                            <td>Rp.{{ $row->harga_produk }}</td>
                            <td>
                                <div>
                                    <form action="{{ url('user/product/'.$row->id.'/update') }}" method="post">
                                        @csrf
                                        @method('PATCH')
                                            <input class="kuantitas" type="number" value="{{ $row->kuantitas }}" name="quantity" min="1" max="{{ $row->stok_produk }}">
                                            {{-- <input id="quantity" size="30" type="number" value="1"/> --}}
                                            {{-- <input type="text" name="qty" id="sst" maxlength="12" value="1" title="Quantity:"
                                                class="input-text qty"> --}}
                                            {{-- <button onclick="var result = document.getElementById('sst'); var sst = result.value; if( !isNaN( sst )) result.value++;return false;"
                                                class="increase items-count" type="button"><i class="lnr lnr-chevron-up"></i></button>
                                            <button onclick="var result = document.getElementById('sst'); var sst = result.value; if( !isNaN( sst ) &amp;&amp; sst > 0 ) result.value--;return false;"
                                                class="reduced items-count" type="button"><i class="lnr lnr-chevron-down"></i></button> --}}
                                </div>
                                </td>
                                <td>Rp.{{ $row->total_harga }}</td>
                            <td>
                                <div class="btn-group" role="group" aria-label="">                                    
                                    <button class="btn btn-info btn-sm mr-1 rounded" type="submit" data-toggle="tooltip" data-placement="right" title="Perbarui"><i class="fa fa-sync"></i></button>
                                </form>
                                {{-- <br> --}}
                                <form action="{{ url('user/product/'.$row->id.'/remove') }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger btn-sm" type="submit" data-toggle="tooltip" data-placement="right" title="Hapus"><i class="far fa-trash-alt"></i></button>
                                </form>
                            </div>                                
                            </td>                            
                        </tr>                
                        @endforeach
                        {{-- <tr class="bottom_button">
                            <td>
                                <a class="button" href="#">Update Cart</a>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                <div class="cupon_text d-flex align-items-center">
                                    <input type="text" placeholder="Coupon Code">
                                    <a class="primary-btn" href="#">Apply</a>
                                    <a class="button" href="#">Have a Coupon?</a>
                                </div>
                            </td>
                        </tr> --}}
                        {{-- <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                <h5>Subtotal</h5>
                            </td>
                            <td>
                                <h5>$2160.00</h5>
                            </td>
                        </tr>
                        <tr class="shipping_area">
                            <td class="d-none d-md-block">
                            </td>
                            <td>
                            </td>
                            <td>
                                <h5>Shipping</h5>
                            </td>
                            <td>
                                <div class="shipping_box">
                                    <ul class="list">
                                        <li><a href="#">Flat Rate: $5.00</a></li>
                                        <li><a href="#">Free Shipping</a></li>
                                        <li><a href="#">Flat Rate: $10.00</a></li>
                                        <li class="active"><a href="#">Local Delivery: $2.00</a></li>
                                    </ul>
                                    <h6>Calculate Shipping <i class="fa fa-caret-down" aria-hidden="true"></i></h6>
                                    <select class="shipping_select">
                                        <option value="1">Bangladesh</option>
                                        <option value="2">India</option>
                                        <option value="4">Pakistan</option>
                                    </select>
                                    <select class="shipping_select">
                                        <option value="1">Select a State</option>
                                        <option value="2">Select a State</option>
                                        <option value="4">Select a State</option>
                                    </select>
                                    <input type="text" placeholder="Postcode/Zipcode">
                                    <a class="gray_btn" href="#">Update Details</a>
                                </div>
                            </td>
                        </tr> --}}
                        <tr class="out_button_area">
                            @if(session('cart'))
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></t\d>
                            <td></td>
                            @else
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            @endif
                            <td>
                                <div class="checkout_btn_inner d-flex align-items-center">
                                    <a class="gray_btn" href="category">Lanjut berbelanja</a>
                                    @if($cek_keranjang != 0 || session('cart') != 0)
                                    <a class="primary-btn ml-2" href="/user/checkout">Lanjutkan ke <i>Checkout</i></a>
                                    @endif
                                    {{-- <button class="primary-btn ml-2 border-0" type="submit">Lanjutkan ke <i>Checkout</i></button> --}}
                                </div>
                            </td>
                        </tr>
                    </tbody>
                  </table>
                {{-- </form> --}}
                @else
                <table id="cart" class="table table-hover table-condensed">
                    @if(session('cart'))
                    <thead>
                        <tr>
                            <th>Produk</th>
                            <th>Harga</th>
                            <th>Kuantitas</th>
                            <th>Subtotal</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    @else
                    <thead>
                        <tr>
                            <th colspan="5" style="text-align: center">Ups ! Keranjang masih kosong, pilih-pilih dulu lha baru balik lagi ke sini</th>
                        </tr>
                    </thead>
                    @endif
                    <tbody>
            
                    <?php $total = 0 ?>
            
                    @if(session('cart'))
                        @foreach(session('cart') as $id => $details)
            
                            <?php $total += $details['harga_produk'] * $details['kuantitas'] ?>
            
                            {{-- <tr>
                                <td data-th="Product">
                                    <div class="media">
                                        <div class="d-flex">
                                            <img src="{{ asset('uploads'.$details['gambar_produk']) }}" class="img-fluid" style="height: 140px; width: 140px;">
                                        </div>
                                        <div class="media-body">
                                            <p>{{ $details['nama_produk'] }}</p>
                                        </div>
                                    </div>
                                </td>
                                <td data-th="Price">Rp.{{ $details['harga_produk'] }}</td>
                                <td data-th="Quantity">
                                    <input class="kuantitas" type="number" value="{{ $details['kuantitas'] }}" class="form-control quantity" />
                                </td>
                                <td data-th="Subtotal" class="text-center">Rp.{{ $details['harga_produk'] * $details['kuantitas'] }}</td>
                                <td class="actions" data-th="">
                                    <button class="btn btn-info btn-sm update-cart" data-id="{{ $id }}"><i class="fa fa-sync"></i></button>
                                    <button class="btn btn-danger btn-sm remove-from-cart" data-id="{{ $id }}"><i class="fa fa-trash-alt"></i></button>
                                </td>
                            </tr> --}}

                            <tr>
                                <td data-th="Product">
                                    <div class="media">
                                        <div class="d-flex"><img src="{{ asset('uploads'.$details['gambar_produk']) }}" style="height: 140px; width: 140px;" class="img-responsive"/></div>
                                        <div class="media-body">{{ $details['nama_produk'] }}</div>
                                    </div>
                                </td>
                                <td data-th="Price">Rp.{{ $details['harga_produk'] }}</td>
                                <td data-th="Quantity">
                                    <input type="number" style="width: 77px; margin: auto;" min="1" max="{{ $details['stok_produk'] }}" value="{{ $details['kuantitas'] }}" class="form-control quantity" />
                                </td>
                                <td data-th="Subtotal" class="text-center">Rp.{{ $details['harga_produk'] * $details['kuantitas'] }}</td>
                                <td class="actions" data-th="">
                                    <button class="btn btn-info btn-sm update-cart" data-id="{{ $id }}"><i class="fa fa-sync"></i></button>
                                    <button class="btn btn-danger btn-sm remove-from-cart" data-id="{{ $id }}"><i class="fa fa-trash-alt"></i></button>
                                </td>
                            </tr>

                        @endforeach
                    @endif

                    </tbody>
                    <tfoot>
                    {{-- <tr class="visible-xs">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="text-center"><strong>Total {{ $total }}</strong></td>
                    </tr> --}}
                    <tr>
                        <td>
                            <div class="checkout_btn_inner d-flex align-items-center">
                                <a class="gray_btn" href="category">Lanjut berbelanja</a>
                            </div>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="hidden-xs text-center"><strong>Total Rp.{{ $total }}</strong></td>
                    </tr>
                    </tfoot>
                </table>
                <div>Ingin melanjutkan ke <i>Checkout</i> ? Cus lah <a href="/login">login</a> dulu</div>
                @endif
              </div>
          </div>
      </div>
  </section>
  <!--================End Cart Area =================-->

  @include('user/footer')

  <script src="{{ asset('aroma/vendors/jquery/jquery-3.2.1.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/bootstrap/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/skrollr.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/owl-carousel/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/nice-select/jquery.nice-select.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/jquery.ajaxchimp.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/mail-script.js') }}"></script>
  <script src="{{ asset('aroma/js/main.js') }}"></script>
  <script>
      $(document).ready(function() {
            var price = parseFloat($("#total").text());        
            function updatePrice() {
                var sum, num = parseInt($("#quantity").val(),10);
                console.log(num);
                if(num) {
                    sum = num * price;
                    $("#total").text(sum.toFixed(2));
                }
            }
            $(document).on("change, mouseup, keyup", "#quantity", updatePrice);
        });

        $(".update-cart").click(function (e) {
            e.preventDefault();
            var ele = $(this);
                $.ajax({
                url: '{{ url('user/product/update/session') }}',
                method: "patch",
                data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id"), quantity: ele.parents("tr").find(".quantity").val()},
                success: function (response) {
                    window.location.reload();
                }
            });
        });

        $(".remove-from-cart").click(function (e) {
            e.preventDefault();
            var ele = $(this);
            if(confirm("Yakin untuk menghapus ?")) {
                $.ajax({
                    url: '{{ url('user/product/remove/session') }}',
                    method: "DELETE",
                    data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id")},
                    success: function (response) {
                        window.location.reload();
                    }
                });
            }
        });
  </script>
</body>
</html>