<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Quick Market - Kategori</title>
	<link rel="icon" href="{{ asset('about/QM Logo.png') }}" type="image/png">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/bootstrap/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/fontawesome/css/all.min.css') }}">
	<link rel="stylesheet" href="{{ asset('aroma/vendors/themify-icons/themify-icons.css') }}">
	<link rel="stylesheet" href="{{ asset('aroma/vendors/linericon/style.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/owl-carousel/owl.theme.default.min.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/owl-carousel/owl.carousel.min.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/nice-select/nice-select.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/nouislider/nouislider.min.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/css/style.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/css/scrollbar.css') }}">
</head>
<body>

  @include('user/header')
{{-- 
  <nav class="breadcrumb">
    <a class="breadcrumb-item" href="#">asx</a>
    <a class="breadcrumb-item" href="#">sa</a>
    <span class="breadcrumb-item active">asd</span>
  </nav> --}}

	<!-- ================ start banner area ================= -->	
	<section class="blog-banner-area" id="category">
		<div class="container h-100">
			<div class="blog-banner">
				<div class="text-center">
					<h1>Kategori Produk</h1>
					<nav aria-label="breadcrumb" class="banner-breadcrumb">
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/user">Beranda</a></li>
            <li class="breadcrumb-item active" aria-current="page">Kategori</li>
            </ol>
          </nav>
			</div>
		</div>
    </div>
	</section>
	<!-- ================ end banner area ================= -->

  <!-- ================ category section start ================= -->		  
  <section class="section-margin--small mb-5">
    <div class="container">
      <div class="row">
        <div class="col-xl-3 col-lg-4 col-md-5">
          <div class="sidebar-categories">
            <div class="head">Daftar Kategori</div>
            <ul class="main-categories">
              <li class="common-filter">
                <ul>
                  <li class="filter-list">
                    <a class="text-secondary" href="/user/category">
                      <label>Tampilkan Semua</label>
                    </a>
                  </li>
                  @foreach ($kategori as $item)
                    <li class="filter-list">
                      <a href="{{ url('/user/category/product', strtolower($item->nama_kategori)) }}" class="text-secondary">
                        <label>{{$item->nama_kategori}}</label>
                      </a>
                    </li>
                  @endforeach
                </ul>
                <br>
              </li>
            </ul>
          </div>
        </div>
        <div class="col-xl-9 col-lg-8 col-md-7">
          <!-- Start Filter Bar -->
          <div class="filter-bar d-flex flex-wrap align-items-center">
            {{-- <div class="sorting">
              <select>
                @foreach ($kategori as $item)
                  <option value="{{$item->nama_kategori}}">{{$item->nama_kategori}}</option>
                @endforeach
              </select>
            </div> --}}
            <div>
              <form action="{{ url('/user/category/search') }}" method="GET">
                <div class="input-group filter-bar-search">
                  <input type="search" placeholder="Cari" id="myInput" name="cari">
                  <div class="input-group-append">
                    <button type="submit"><i class="ti-search"></i></button>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <!-- End Filter Bar -->
          <!-- Start Best Seller -->
          <section class="lattest-product-area pb-40 category-list">
            @if(session()->has('message'))
            <div class="alert alert-info alert-dismissible fade show" role="alert">
              {{ session()->get('message') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @endif
            @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
            @endif
            @if (Request::path() === 'user/category')
              @if (Auth::check())
              <div class="row">
                @foreach ($produk_kategori as $row)                  
                  <div class="col-md-6 col-lg-4" id="myCard">
                    <div class="card text-center card-product">
                      <div class="card-product__img">
                        <img src="{{ asset('uploads'. $row->gambar_produk) }}" class="img-fluid" style="width: 263px; height:280px">
                        <ul class="card-product__imgOverlay">
                          <li><button style="cursor: default"><a href="{{ url('user/product/' .$row->id. '/info') }}"><i class="ti-search"></i></a></button></li>
                          <li>
                            @if (\App\Http\Controllers\CartController::cartExist($row->id))
                              <form action="{{ url('user/product/'.$row->id.'/remove')}}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button style="cursor: default" type="submit" @if($row->pemilik === auth()->user()->name) hidden @endif><i style="cursor: pointer" class="ti-close"></i></button>
                              </form>
                            @else
                              <form action="{{ url('user/product/'.$row->id.'/add') }}" method="POST">
                                @csrf
                                <button style="cursor: default" type="submit" @if($row->pemilik === auth()->user()->name) hidden @endif><i style="cursor: pointer" class="ti-shopping-cart"></i></button>
                              </form>
                            @endif
                          </li>
                          <li>
                            @if (\App\Http\Controllers\WishlistController::wishExist($row->id))
                            <form action="{{ url('user/product/'.$row->id.'/wish/remove') }}" method="POST">
                              @method('DELETE')
                              @csrf
                              <button style="cursor: default" type="submit" @if($row->pemilik === auth()->user()->name) hidden @endif><i style="cursor: pointer" class="ti-heart-broken"></i></button>
                            </form>
                            @else
                            <form action="{{ url('user/product/'.$row->id.'/wish') }}" method="POST">
                              @csrf
                              <button style="cursor: default" type="submit" @if($row->pemilik === auth()->user()->name) hidden @endif><i style="cursor: pointer" class="ti-heart"></i></button>
                            </form>  
                            @endif
                          </li>
                        </ul>
                      </div>
                      <div class="card-body">
                        <p style="font-size: 8.4px">{{ $row->pemilik }}</p>
                        <p>{{ $row->kategori_produk }}</p>
                        <h4 class="card-product__title"><a href="{{ url('user/product/' .$row->id. '/info') }}">{{ $row->nama_produk }}</a></h4>
                        <p class="card-product__price">Rp.{{ $row->harga_produk }}</p>
                      </div>
                    </div>
                  </div>
                @endforeach
              </div>
              @else
              <div class="row">
                @foreach ($produk_kategori as $row)                  
                  <div class="col-md-6 col-lg-4" id="myCard">
                    <div class="card text-center card-product">
                      <div class="card-product__img">
                        <img src="{{ asset('uploads'. $row->gambar_produk) }}" class="img-fluid" style="width: 263px; height:280px">
                        <ul class="card-product__imgOverlay">
                          <li><button style="cursor: default"><a href="{{ url('user/product/' .$row->id. '/info') }}"><i class="ti-search"></i></a></button></li>
                          <li><button style="cursor: default" type="submit"><a href="{{ url('user/product/'.$row->id. '/add/session') }}"><i style="cursor: pointer" class="ti-shopping-cart"></i></a></button></li>
                          <li><button style="cursor: default" type="submit"><i style="cursor: pointer" class="ti-heart"></i></button></li>
                        </ul>
                      </div>
                      <div class="card-body">
                        <p>{{ $row->kategori_produk }}</p>
                        <h4 class="card-product__title"><a href="{{ url('user/product/' .$row->id. '/info') }}">{{ $row->nama_produk }}</a></h4>
                        <p class="card-product__price">Rp.{{ $row->harga_produk }}</p>
                      </div>
                    </div>
                  </div>
                @endforeach
              </div>
              @endif
            </section>
            <nav class="blog-pagination justify-content-center d-flex">
            {{ $produk_kategori->links() }}
            </nav>
          @else
            @if (Auth::check())
            <div class="row">
              @foreach ($produk_kategori as $row)                  
                <div class="col-md-6 col-lg-4" id="myCard">
                  <div class="card text-center card-product">
                    <div class="card-product__img">
                      <img src="{{ asset('uploads'. $row->gambar_produk) }}" class="img-fluid" style="width: 263px; height:280px">
                      <ul class="card-product__imgOverlay">
                        <li><button style="cursor: default"><a href="{{ url('user/product/' .$row->id. '/info') }}"><i class="ti-search"></i></a></button></li>
                        <li>
                          @if (\App\Http\Controllers\CartController::cartExist($row->id))
                            <form action="{{ url('user/product/'.$row->id.'/remove')}}" method="POST">
                              @method('DELETE')
                              @csrf
                              <button style="cursor: default" type="submit"><i style="cursor: pointer" class="ti-close"></i></button>
                            </form>
                          @else
                            <form action="{{ url('user/product/'.$row->id.'/add') }}" method="POST">
                              @csrf
                              <button style="cursor: default" type="submit"><i style="cursor: pointer" class="ti-shopping-cart"></i></button>
                            </form>
                          @endif
                        </li>
                        <li>
                          @if (\App\Http\Controllers\WishlistController::wishExist($row->id))
                          <form action="{{ url('user/product/'.$row->id.'/wish/remove') }}" method="POST">
                            @method('DELETE')
                            @csrf
                            <button style="cursor: default" type="submit"><i style="cursor: pointer" class="ti-heart-broken"></i></button>
                          </form>
                          @else
                          <form action="{{ url('user/product/'.$row->id.'/wish') }}" method="POST">
                            @csrf
                            <button style="cursor: default" type="submit"><i style="cursor: pointer" class="ti-heart"></i></button>
                          </form>  
                          @endif
                        </li>
                      </ul>
                    </div>
                    <div class="card-body">
                      <p style="font-size: 8.4px">{{ $row->pemilik }}</p>
                      <p>{{ $row->kategori_produk }}</p>
                      <h4 class="card-product__title"><a href="{{ url('user/product/' .$row->id. '/info') }}">{{ $row->nama_produk }}</a></h4>
                      <p class="card-product__price">Rp.{{ $row->harga_produk }}</p>
                    </div>
                  </div>
                </div>
              @endforeach
            </div>
            @else
            <div class="row">
              @foreach ($produk_kategori as $row)                  
                <div class="col-md-6 col-lg-4" id="myCard">
                  <div class="card text-center card-product">
                    <div class="card-product__img">
                      <img src="{{ asset('uploads'. $row->gambar_produk) }}" class="img-fluid" style="width: 263px; height:280px">
                      <ul class="card-product__imgOverlay">
                        <li><button style="cursor: default"><a href="{{ url('user/product/' .$row->id. '/info') }}"><i class="ti-search"></i></a></button></li>
                        <li><button style="cursor: default" type="submit"><a href="{{ url('user/product/'.$row->id. '/add/session') }}"><i style="cursor: pointer" class="ti-shopping-cart"></i></a></button></li>
                        <li><button style="cursor: default" type="submit"><i style="cursor: pointer" class="ti-heart"></i></button></li>
                      </ul>
                    </div>
                    <div class="card-body">
                      <p>{{ $row->kategori_produk }}</p>
                      <h4 class="card-product__title"><a href="{{ url('user/product/' .$row->id. '/info') }}">{{ $row->nama_produk }}</a></h4>
                      <p class="card-product__price">Rp.{{ $row->harga_produk }}</p>
                    </div>
                  </div>
                </div>
              @endforeach
            </div>
            @endif
          </section>
          {{-- <nav class="blog-pagination justify-content-center d-flex">
          {{ $produk->links() }}
          </nav> --}}
          @endif
          <!-- End Best Seller -->
        </div>
      </div>
    </div>
  </section>
	<!-- ================ category section end ================= -->		  

	@include('user/topproduk')	

	@include('user/subscribe')

  @include('user/footer')

  <script src="{{ asset('aroma/vendors/jquery/jquery-3.2.1.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/bootstrap/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/skrollr.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/owl-carousel/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/nice-select/jquery.nice-select.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/nouislider/nouislider.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/jquery.ajaxchimp.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/mail-script.js') }}"></script>
  <script src="{{ asset('aroma/js/main.js') }}"></script>
  {{-- <script>
    $(document).ready(function(){
      $("#myInput").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#myCard div.card").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
      });
    });
  </script> --}}
</body>
</html>