<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Quick Market - Checkout</title>
  <style>
      .ongkir{
        border-width: 1px;
        border-bottom-style: solid;
      }
      th, td{
        font-family: inherit;
        color: black;
        font-weight: normal;
        font-size: 14px;
      }
      .total{
          background: none;
          border-style: none;
          outline: none;
          font-family: inherit;
          font-weight: 500;
      }
      .huruf{
        font-family: inherit;
        font-weight: 500;
      }
      .produk{
          color: #777;
      }
      .produk:hover{
        color: #777;
      }
      .head{
          font-weight: 500;
          font-size: 15px;
      }
      input[type=number]::-webkit-inner-spin-button, 
      input[type=number]::-webkit-outer-spin-button { 
        -webkit-appearance: none; 
        margin: 0; 
      }
      input[type=number] {
        -moz-appearance: textfield;
      }
  </style>
  <link rel="icon" href="{{ asset('about/QM Logo.png') }}" type="image/png">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/bootstrap/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/fontawesome/css/all.min.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/themify-icons/themify-icons.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/linericon/style.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/owl-carousel/owl.theme.default.min.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/owl-carousel/owl.carousel.min.css') }}">
  {{-- <link rel="stylesheet" href="{{ asset('aroma/vendors/nice-select/nice-select.css') }}"> --}}
  <link rel="stylesheet" href="{{ asset('aroma/vendors/nouislider/nouislider.min.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/css/style.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/css/scrollbar.css') }}">
</head>
<body>
    @include('user/header')

	<!-- ================ start banner area ================= -->	
	<section class="blog-banner-area" id="category">
		<div class="container h-100">
			<div class="blog-banner">
				<div class="text-center">
					<h1>Checkout Produk</h1>
					<nav aria-label="breadcrumb" class="banner-breadcrumb">
                        <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Beranda</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Checkout</li>
                        </ol>
                    </nav>
				</div>
			</div>
        </div>
	</section>
	<!-- ================ end banner area ================= -->
  
  
  <!--================Checkout Area =================-->
  <section class="checkout_area section-margin--small">
    <div class="container">
        {{-- <div class="returning_customer">
            <div class="check_title">
                <h2>Returning Customer? <a href="#">Click here to login</a></h2>
            </div>
            <p>If you have shopped with us before, please enter your details in the boxes below. If you are a new
                customer, please proceed to the Billing & Shipping section.</p>
            <form class="row contact_form" action="#" method="post" novalidate="novalidate">
                <div class="col-md-6 form-group p_star">
                    <input type="text" class="form-control" placeholder="Username or Email*" onfocus="this.placeholder=''" onblur="this.placeholder = 'Username or Email*'" id="name" name="name">
                    <!-- <span class="placeholder" data-placeholder="Username or Email"></span> -->
                </div>
                <div class="col-md-6 form-group p_star">
                    <input type="password" class="form-control" placeholder="Password*" onfocus="this.placeholder=''" onblur="this.placeholder = 'Password*'" id="password" name="password">
                    <!-- <span class="placeholder" data-placeholder="Password"></span> -->
                </div>
                <div class="col-md-12 form-group">
                    <button type="submit" value="submit" class="button button-login">login</button>
                    <div class="creat_account">
                        <input type="checkbox" id="f-option" name="selector">
                        <label for="f-option">Remember me</label>
                    </div>
                    <a class="lost_pass" href="#">Lost your password?</a>
                </div>
            </form>
        </div>
        <div class="cupon_area">
            <div class="check_title">
                <h2>Have a coupon? <a href="#">Click here to enter your code</a></h2>
            </div>
            <input type="text" placeholder="Enter coupon code">
            <a class="button button-coupon" href="#">Apply Coupon</a>
        </div> --}}
        <div class="billing_details">
            <div class="row">
                <div class="col-lg-8">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                      <ul>
                        @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                        @endforeach
                      </ul>
                    </div>
                    @endif
                    @if(session()->has('message'))
                        <div class="alert alert-info alert-dismissible fade show" role="alert">
                        {{ session()->get('message') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        </div>
                    @endif
                    <h3>Detail Tagihan</h3>
                    <form class="row contact_form" action="{{ url('/user/checkout') }}" method="post" novalidate="novalidate">
                        @csrf
                        {{-- <div class="col-md-6 form-group p_star">
                            <select name="provinsi_asal" class="form-control" id="">
                                <option value="">Provinsi Asal</option>
                                @foreach ($provinces as $province => $value)
                                <option value="{{ $province }}">{{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6 form-group p_star">
                            <select name="provinsi_tujuan" class="form-control" id="">
                                <option value="">Provinsi Tujuan</option>
                                @foreach ($provinces as $province => $value)
                                <option value="{{ $province }}">{{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6 form-group p_star">
                            <select name="kota_asal" class="form-control" id="">
                                <option value="">Kota Asal</option>
                            </select>
                        </div>
                        <div class="col-md-6 form-group p_star">
                            <select name="kota_tujuan" class="form-control" id="">
                                <option value="">Kota Tujuan</option>
                            </select>
                        </div> --}}
                        <div class="col-md-12 form-group p_star">
                            <select name="kurir" class="form-control" id="">
                                <option value="">Kurir</option>
                                @foreach ($couriers as $courier => $value)
                                <option value="{{ $courier }}">{{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-12 form-group p_star">
                            <button type="submit" class="button button-coupon btn btn-block">Cek Ongkir</button>
                        </div>
                        <div class="col-md-12">
                            <div class="sidebar-categories">
                              <div class="head"></div>
                              <div class="table-responsive">
                                    <table class="table table-borderless">
                                        {{-- <thead>
                                            <tr>
                                                <th style="text-align: center">Jenis Layanan</th>
                                                <th>Harga</th>
                                                <th>Estimasi</th>
                                            </tr>
                                        </thead> --}}
                                        <tbody>
                                            @foreach ($kurir as $kr)
                                            <tr class="ongkir">
                                                <td scope="row"><a href="{{ url('/user/shipping/'. $kr->id .'/choose') }}" class="pixel-radio"></a><label for="{{ $kr->id }}">{{ $kr->skt_jenis_layanan }} ({{ $kr->pjg_jenis_layanan }})</label></td>
                                                <td scope="row"><label for="{{ $kr->id }}">Rp.{{ $kr->harga }}</label></td>
                                                @if ($kr->skt_kurir === 'pos')
                                                    <td scope="row"><label for="{{ $kr->id }}">{{ $kr->estimasi }}</label></td>
                                                @else
                                                    <td scope="row"><label for="{{ $kr->id }}">{{ $kr->estimasi }} Hari</label></td>
                                                @endif
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                              </div>
                            </div>
                        </div>
                   {{-- <div class="col-md-12 form-group mb-0">
                        <div class="creat_account">
                            <h3>Detail Pengiriman</h3>
                        </div>
                        </div>
                        <div class="col-md-12 form-group">
                            <input type="text" class="form-control" id="zip" name="zip" placeholder="Kode Pos">
                        </div>
                        <div class="col-md-12 form-group">
                            <textarea class="form-control" name="" rows="7" placeholder="Alamat Lengkap"></textarea>
                        </div>
                        <div class="col-md-12 form-group">
                            <textarea class="form-control" name="message" id="message" rows="7" placeholder="Catatan untuk Penjual"></textarea>
                        </div> --}}
                    </form>
                </div>
                <div class="col-lg-4">
                    <div class="order_box">
                        <form action="{{ url('/user/transaction') }}" method="post">
                            @csrf
                            <h2>Pesanan</h2>
                            <?php $total = 0 ?>
                            <table class="table table-borderless">
                                <tr>
                                    <th class="head" colspan="2">Produk</th>
                                    <th class="head" style="text-align: right">Total</th>
                                </tr>
                                @if(session('cart'))
                                @foreach(session('cart') as $id => $details)
                                <tr>
                                    <?php $total += $details['harga_produk'] * $details['kuantitas'] ?>
                                    <td><a class="produk" href="{{ url('user/product/' .$details['id_produk']. '/info') }}">{{ $details['nama_produk'] }}</a></td>
                                    <td class="produk">x {{ $details['kuantitas'] }}</td>
                                    <td class="produk" align="right">Rp.{{ $details['harga_produk'] * $details['kuantitas'] }}</td>
                                </tr>
                                @endforeach
                                @foreach ($keranjang as $row)
                                <tr>
                                    <td><a class="produk" href="{{ url('user/product/' .$row->id. '/info') }}">{{ $row->nama_produk }}</a></td>
                                    <td class="produk">x {{ $row->kuantitas }}</td>
                                    <td class="produk" align="right">@currency($row->total_harga)</td>
                                </tr>
                                @endforeach
                                @else
                                @foreach ($keranjang as $row)
                                <tr>
                                    <td><a class="produk" href="{{ url('user/product/' .$row->id. '/info') }}">{{ $row->nama_produk }}</a></td>
                                    <td class="produk">x {{ $row->kuantitas }}</td>
                                    <td class="produk" align="right">@currency($row->total_harga)</td>
                                </tr>
                                @endforeach
                                @endif
                            </table>
                            <table class="table">
                                <tr>
                                    <td class="huruf" colspan="2">Subtotal @currency($subtotal + $total)</td>
                                </tr>
                                @if (session('ongkir'))
                                <tr>
                                    <td class="huruf" colspan="2">Ongkir Rp.<input type="number" value="{{ session()->get('ongkir')[2] }}" name="ongkir" class="total" readonly></td>
                                </tr>
                                @endif
                                @if (session('kupon'))
                                <tr>
                                    <td class="huruf">{{ session()->get('kupon') }}</td>
                                </tr>
                                @endif
                                <tr>
                                    <td class="huruf" colspan="2">Total Rp.<input type="text" value="{{ $subtotal + $total + session()->get('ongkir')[2] }}" class="total" name="total" readonly></td>
                                    {{-- <td align="right">Rp.{{ $subtotal + $total + session()->get('ongkir') }}</td> --}}
                                    {{-- <td style="text-align: right">Rp.<input type="text" value="{{ $subtotal + $total + session()->get('ongkir') }}" size="14" class="total" name="total" readonly></td> --}}
                                </tr>
                            </table>
                            {{-- <ul class="list list_2">
                                <li><a href="#" style="cursor: default">Subtotal <span>Rp.{{ $subtotal + $total }}</span></a></li>
                                @if (session('ongkir'))
                                <li><a href="#" style="cursor: default">Ongkir <span>Rp.{{ session()->get('ongkir') }}</span></a></li>
                                @endif
                                <li><a href="#" style="cursor: default">Total <span>Rp.<input type="text" value="{{ $subtotal + $total + session()->get('ongkir') }}" class="total" name="total" readonly></span></a></li>
                            </ul> --}}
                            {{-- <div class="payment_item">
                                <div class="radion_btn">
                                    <input type="radio" id="f-option5" name="selector">
                                    <label for="f-option5">Cek Ongkir</label>
                                    <div class="check"></div>
                                </div>
                                <p>Mohon untuk mengirimkan Nama Toko, Alamat, Kota, Negara, dan Kode Pos</p>
                            </div>
                            <div class="payment_item active">
                                <div class="radion_btn">
                                    <input type="radio" id="f-option6" name="selector">
                                    <label for="f-option6">Paypal </label>
                                    <img src="img/product/card.jpg" alt="">
                                    <div class="check"></div>
                                </div>
                                <p>Bayar via PayPal, kamu dapat membayar dengan kartu kreditmu jika kamu tidak mempunyai akun PayPal</p>
                            </div>
                            <div class="creat_account">
                                <input type="checkbox" id="f-option4" name="selector">
                                <label for="f-option4">Saya telah membaca dan menyetujui </label>
                                <a href="#">Syarat & Ketentuan*</a>
                            </div> --}}
                            <table class="table table-borderless table-responsive">
                                <tr style="border-bottom-style: none">
                                    <td>
                                        <div class="radion_btn">
                                            <input type="radio" id="f-option5" name="via" value="COD">
                                            <label for="f-option5">COD</label>
                                            <div class="check"></div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="radion_btn">
                                            <input type="radio" id="f-option6" name="via" value="QM Wallet">
                                            <label for="f-option6" title="Saldo anda Rp.{{ auth()->user()->qm_wallet }}">QM Wallet</label>
                                            <div class="check"></div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            @if(session('ongkir'))
                            <input type="hidden" value="{{ strtoupper(session('ongkir')[0]) }}" name="kurir">
                            <input type="hidden" value="{{ session('ongkir')[1] }}" name="jenis_layanan">
                            <div class="text-center">
                            <button type="submit" class="button button-paypal">Pesan Sekarang</button>
                            </div>
                            @endif
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
  <!--================End Checkout Area =================-->



  @include('user/footer')



  <script src="{{ asset('aroma/vendors/jquery/jquery-3.2.1.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/bootstrap/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/skrollr.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/owl-carousel/owl.carousel.min.js') }}"></script>
  {{-- <script src="{{ asset('aroma/vendors/nice-select/jquery.nice-select.min.js') }}"></script> --}}
  <script src="{{ asset('aroma/vendors/jquery.ajaxchimp.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/mail-script.js') }}"></script>
  <script src="{{ asset('aroma/js/main.js') }}"></script>
  <script src="{{ asset('adminlte/dist/js/autosize.js') }}"></script>
  <script>
    autosize(document.querySelectorAll('textarea'));

    // $(document).ready(function () {
    //     $('select[name="provinsi_asal"]').on('change', function(){
    //         let provinceId = $(this).val();
    //         if (provinceId) {
    //             jQuery.ajax({
    //                 url : '/user/checkout/'+provinceId+'/cities',
    //                 type : "GET",
    //                 dataType : "json",
    //                 success:function(data){
    //                     $('select[name="kota_asal"]').empty();
    //                     $.each(data, function(key, value){
    //                     console.log(value);
    //                         $('select[name="kota_asal"]').append('<option value="'+ key +'">'+ value +'</option>');
    //                     });
    //                 },
    //             });
    //         }
    //         else{
    //             $('select[name="kota_asal"]').empty();
    //         }
    //     });

    //     $('select[name="provinsi_tujuan"]').on('change', function(){
    //         let provinceId = $(this).val();
    //         if (provinceId) {
    //             jQuery.ajax({
    //                 url : '/user/checkout/'+provinceId+'/cities',
    //                 type : "GET",
    //                 dataType : "json",
    //                 success:function(data){
    //                     $('select[name="kota_tujuan"]').empty();
    //                     $.each(data, function(key, value){
    //                         $('select[name="kota_tujuan"]').append('<option value="'+ key +'">'+ value +'</option>');
    //                     });
    //                 },
    //             });
    //         }
    //         else{
    //             $('select[name="kota_tujuan"]').empty();
    //         }
    //     });
    // });

  </script>
</body>
</html>