<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Quick Market</title>
  <link rel="icon" href="{{ asset('about/QM Logo.png') }}" type="image/png">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/bootstrap/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/fontawesome/css/all.min.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/themify-icons/themify-icons.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/linericon/style.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/owl-carousel/owl.theme.default.min.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/owl-carousel/owl.carousel.min.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/nice-select/nice-select.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/nouislider/nouislider.min.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/css/style.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/css/scrollbar.css') }}">
</head>
<body>
    @include('user/header')
  
	<!-- ================ start banner area ================= -->	
	<section class="blog-banner-area" id="category">
		<div class="container h-100">
			<div class="blog-banner">
				<div class="text-center">
					<h1>Konfirmasi Pembayaran</h1>
					<nav aria-label="breadcrumb" class="banner-breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="/">Beranda</a></li>
              <li class="breadcrumb-item active" aria-current="page">Konfirmasi</li>
            </ol>
          </nav>
				</div>
			</div>
        </div>
	</section>
	<!-- ================ end banner area ================= -->
  
  <!--================Order Details Area =================-->
  <section class="order_details section-margin--small">
    <div class="container">
      <p class="text-center billing-alert">Terima kasih. Pesananmu telah diterima.</p>
      <div class="row">
        <div class="col">
          <div class="confirmation-card">
            <h3 class="billing-title">Info Pesanan</h3>
            <table class="order-rable">
              <tr>
                <td>Nomor Resi</td>
                <td>: {{ $transaksi->no_resi }}</td>
              </tr>
              <tr>
                <td>Tanggal</td>
                <td>: {{ date('d-m-y', strtotime($transaksi->created_at)) }}</td>
              </tr>
              <tr>
                <td>Total</td>
                <td>: @currency($transaksi->total_harga)</td>
              </tr>
              <tr>
                <td>Metode Pembayaran</td>
                <td>: {{ $transaksi->metode_pembayaran }}</td>
              </tr>
            </table>
          </div>
        </div>
        {{-- <div class="col-md-6 col-xl-4 mb-4 mb-xl-0">
            <div class="confirmation-card">
              <h3 class="billing-title">Info Pesanan</h3>
              <table class="order-rable">
                <tr>
                  <td>Nomor Pesanan</td>
                  <td>: 60235</td>
                </tr>
                <tr>
                  <td>Tanggal</td>
                  <td>: Oct 03, 2017</td>
                </tr>
                <tr>
                  <td>Total</td>
                  <td>: USD 2210</td>
                </tr>
                <tr>
                  <td>Metode Pembayaran</td>
                  <td>: Check payments</td>
                </tr>
              </table>
            </div>
          </div> --}}
        {{-- <div class="col-md-6 col-xl-4 mb-4 mb-xl-0">
          <div class="confirmation-card">
            <h3 class="billing-title">Alamat Pengirim</h3>
            <table class="order-rable">
              <tr>
                <td>Jalan</td>
                <td>: 56/8 panthapath</td>
              </tr>
              <tr>
                <td>Kota</td>
                <td>: Dhaka</td>
              </tr>
              <tr>
                <td>Negara</td>
                <td>: Indonesia</td>
              </tr>
              <tr>
                <td>Kode Pos</td>
                <td>: 1205</td>
              </tr>
            </table>
          </div>
        </div>
        <div class="col-md-6 col-xl-4 mb-4 mb-xl-0">
          <div class="confirmation-card">
            <h3 class="billing-title">Alamat Penerima</h3>
            <table class="order-rable">
              <tr>
                <td>Jalan</td>
                <td>: 56/8 panthapath</td>
              </tr>
              <tr>
                <td>Kota</td>
                <td>: Dhaka</td>
              </tr>
              <tr>
                <td>Negara</td>
                <td>: Indonesia</td>
              </tr>
              <tr>
                <td>Kode Pos</td>
                <td>: 1205</td>
              </tr>
            </table>
          </div>
        </div> --}}
      </div>
      <div class="row">
        <div class="col">
          <div class="order_details_table">
            <h2>Detail Pesanan</h2>
            <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th scope="col">Produk</th>
                    <th scope="col">Kuantitas</th>
                    <th scope="col">Total</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($pesanan as $row)
                  <tr>
                    <td>
                      <p>{{ $row->nama_produk }}</p>
                    </td>
                    <td>
                      <h5>x {{ $row->kuantitas }}</h5>
                    </td>
                    <td>
                      <p>@currency($row->total_harga)</p>
                    </td>
                  </tr>
                  @endforeach
                  <tr>
                    <td>
                      <h4>Subtotal</h4>
                    </td>
                    <td>
                      <h5></h5>
                    </td>
                    <td>
                      <p>@currency($subtotal)</p>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <h4>Ongkir</h4>
                    </td>
                    <td>
                      <h5></h5>
                    </td>
                    <td>
                      <p>@currency($transaksi->ongkir)</p>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <h4>Total</h4>
                    </td>
                    <td>
                      <h5></h5>
                    </td>
                    <td>
                      <h4>@currency($subtotal + $transaksi->ongkir)</h4>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--================End Order Details Area =================-->



  @include('user/footer')



  <script src="{{ asset('aroma/vendors/jquery/jquery-3.2.1.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/bootstrap/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/skrollr.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/owl-carousel/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/nice-select/jquery.nice-select.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/jquery.ajaxchimp.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/mail-script.js') }}"></script>
  <script src="{{ asset('aroma/js/main.js') }}"></script>
</body>
</html>