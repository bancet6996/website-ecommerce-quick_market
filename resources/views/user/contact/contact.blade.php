<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Quick Market - Kontak</title>
  <link rel="icon" href="{{ asset('about/QM Logo.png') }}" type="image/png">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/bootstrap/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/fontawesome/css/all.min.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/themify-icons/themify-icons.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/linericon/style.css') }}">
  {{-- <link rel="stylesheet" href="{{ asset('aroma/vendors/nice-select/nice-select.css') }}"> --}}
  <link rel="stylesheet" href="{{ asset('aroma/vendors/owl-carousel/owl.theme.default.min.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/owl-carousel/owl.carousel.min.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/css/style.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/css/scrollbar.css') }}">
</head>
<body>
  
  @include('user/header')

	<!-- ================ start banner area ================= -->
	<section class="blog-banner-area" id="contact">
		<div class="container h-100">
			<div class="blog-banner">
				<div class="text-center">
					<h1>Kontak Kami</h1>
					<nav aria-label="breadcrumb" class="banner-breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="/user">Beranda</a></li>
              <li class="breadcrumb-item active" aria-current="page">Kontak</li>
            </ol>
          </nav>
				</div>
			</div>
    </div>
	</section>
	<!-- ================ end banner area ================= -->

	<!-- ================ contact section start ================= -->
  <section class="section-margin--small">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-lg-3 mb-4 mb-md-0">
          <div class="media contact-info">
            <span class="contact-info__icon"><i class="ti-home"></i></span>
            <div class="media-body">
              <h3>Bandung, Jawa Barat</h3>
              <p>Jl. Batu Nunggal Abadi I No 4</p>
            </div>
          </div>
          <div class="media contact-info">
            <span class="contact-info__icon"><i class="ti-mobile"></i></span>
            @if (Auth::check())
              <div class="media-body">
                <a href="https://wa.me/6288222656489?text=Assalamualaikum...%0AHalo%2C%20saya%20{{Auth::user()->name}}%0ASaya%20ingin%20.....">
                  <h3>+62 88 222 656 489</h3>
                  <p>Kontak WhatsApp</p>
                </a>
              </div>
            @else
              <div class="media-body">
                <a href="https://wa.me/6288222656489?text=Assalamualaikum...%0AHalo%2C%20saya%20ingin%20.....">
                  <h3>+62 88 222 656 489</h3>
                  <p>Kontak WhatsApp</p>
                </a>
              </div>
            @endif
          </div>
          <div class="media contact-info">
            <span class="contact-info__icon"><i class="ti-email"></i></span>
            <div class="media-body">
              <h3>bancet6996@gmail.com</h3>
              <p>Kontak Email</p>
            </div>
          </div>
        </div>
        <div class="col-md-8 col-lg-9">
          @if(session()->has('message'))
          <div class="alert alert-info alert-dismissible fade show" role="alert">
            {{ session()->get('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          @endif
          @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif
          @if (Auth::check())
          <form action="{{ url('/user/contact/post') }}" class="form-contact contact_form" action="contact_process.php" method="post" id="contactForm" novalidate="novalidate">
            @csrf
            <div class="row">
              <div class="col-lg-5">
                <div class="form-group">
                  <input class="form-control" name="nama" id="name" type="text" placeholder="Masukkan nama" value="{{ Auth::user()->name }}">
                </div>
                <div hidden class="form-group">
                  <input class="form-control" name="email" id="email" type="email" placeholder="Enter email address" value="{{ Auth::user()->email }}" readonly>
                </div>
                <div class="form-group">
                  <input class="form-control" name="subjek" id="subject" type="text" value="Pesan dari {{ Auth::user()->name}}" placeholder="Masukkan subjek">
                </div>
                <select name="kepada" class="form-control">
                  <option class="form-control" value="">Kepada</option>
                  <option class="form-control" value="Quick Market">Quick Market (qm@qm.id)</option>
                  @foreach ($admin as $item)
                    <option class="form-control" value="{{ $item->name }}">{{ $item->name }} ({{ $item->email }})</option>
                  @endforeach
                </select>
              </div>
              <div class="col-lg-7">
                <div class="form-group">
                    <textarea class="form-control different-control w-100" name="pesan" id="message" cols="30" rows="5" placeholder="Masukkan pesan" style="resize: none"></textarea>
                </div>
              </div>
            </div>
            <div class="form-group text-center text-md-right mt-3">
              <button type="submit" class="button button--active button-contactForm">Kirim Pesan</button>
            </div>
          </form>
          @else
          <form action="{{ url('/user/contact/post') }}" class="form-contact contact_form" action="contact_process.php" method="post" id="contactForm" novalidate="novalidate">
            @csrf
            <div class="row">
              <div class="col-lg-5">
                <div class="form-group">
                  <input class="form-control" name="nama" id="name" type="text" placeholder="Masukkan nama">
                </div>
                <div class="form-group">
                  <input class="form-control" name="email" id="email" type="email" placeholder="Masukkan email">
                </div>
                <div class="form-group">
                  <input class="form-control" name="subjek" id="subject" type="text" placeholder="Masukkan subjek">
                </div>
              </div>
              <div class="col-lg-7">
                <div class="form-group">
                    <textarea class="form-control different-control w-100" name="pesan" id="message" cols="30" rows="5" placeholder="Masukkan pesan" style="resize: none"></textarea>
                </div>
              </div>
            </div>
            <div class="form-group text-center text-md-right mt-3">
              <button type="submit" class="button button--active button-contactForm">Kirim Pesan</button>
            </div>
          </form>
          @endif
        </div>
      </div>
    </div>
  </section>
	<!-- ================ contact section end ================= -->
  
  @include('user/footer')

  <script src="{{ asset('aroma/vendors/jquery/jquery-3.2.1.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/bootstrap/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/skrollr.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/owl-carousel/owl.carousel.min.js') }}"></script>
  {{-- <script src="{{ asset('aroma/vendors/nice-select/jquery.nice-select.min.js') }}"></script> --}}
  {{-- <script src="{{ asset('aroma/vendors/jquery.form.js') }}"></script>
  <script src="{{ asset('aroma/vendors/jquery.validate.min.js') }}"></script> --}}
  <script src="{{ asset('aroma/vendors/contact.js') }}"></script>
  <script src="{{ asset('aroma/vendors/jquery.ajaxchimp.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/mail-script.js') }}"></script>
  <script src="{{ asset('aroma/js/main.js') }}"></script>
</body>
</html>