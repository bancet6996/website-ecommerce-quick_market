@extends('layouts.app')
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="google-signin-client_id" content="457203821272-nu2gbtnvhck52adg5u7rg2f0128k3c9l.apps.googleusercontent.com">
  <title>Quick Market - Daftar</title>
  	<link rel="icon" href="{{ asset('about/QM Logo.png') }}" type="image/png">
    <link rel="stylesheet" href="{{ asset('aroma/vendors/bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('aroma/vendors/fontawesome/css/all.min.css') }}">
	<link rel="stylesheet" href="{{ asset('aroma/vendors/themify-icons/themify-icons.css') }}">
	<link rel="stylesheet" href="{{ asset('aroma/vendors/linericon/style.css') }}">
    <link rel="stylesheet" href="{{ asset('aroma/vendors/owl-carousel/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('aroma/vendors/owl-carousel/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('aroma/vendors/nice-select/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('aroma/vendors/nouislider/nouislider.min.css') }}">

  	<link rel="stylesheet" href="{{ asset('aroma/css/style.css') }}">
  	<link rel="stylesheet" href="{{ asset('aroma/css/scrollbar.css') }}">
</head>
<body>
    
	@include('user/header')
  
  <!-- ================ start banner area ================= -->	
	<section class="blog-banner-area" id="category">
		<div class="container h-100">
			<div class="blog-banner">
				<div class="text-center">
					<h1>Daftar</h1>
					<nav aria-label="breadcrumb" class="banner-breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="/user">Beranda</a></li>
              <li class="breadcrumb-item active" aria-current="page">Daftar</li>
            </ol>
          </nav>
				</div>
			</div>
    	</div>
	</section>
	<!-- ================ end banner area ================= -->
  
  <!--================Register Box Area =================-->
  <form method="POST" action="{{ route('register') }}">
    @csrf
  		<section class="login_box_area section-margin">
			<div class="container">
				<div class="row">
					<div class="col-lg-6">
						<div class="login_box_img">
							<div class="hover">
								<h4>Sudah punya akun ?</h4>
								<p>Yuk tinggal masuk aja</p>
								<a class="button button-account" href="login">Masuk Sekarang</a>
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="login_form_inner register_form_inner">
							<h3>Buat Akun</h3>
							<form class="row login_form" action="#/" id="register_form" >

								<div class="col-md-12 form-group">
									<input placeholder="Nama" id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
									@error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                               		 @enderror
								</div>

								<div class="col-md-12 form-group">
									<input placeholder="Alamat Email" id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
									@error('email')
										<span class="invalid-feedback" role="alert">
											<strong>{{ $message }}</strong>
										</span>
									@enderror
								</div>

								<div class="col-md-12 form-group">
									<input placeholder="Kata Sandi" id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
									@error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
								</div>

								<div class="col-md-12 form-group">
									<input placeholder="Konfirmasi Kata Sandi" id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
								</div>

								<div class="col-md-12 form-group">
									<input type="checkbox" onclick="myFunction()">
									<label for="show">Tampilkan Kata Sandi</label> <br><br>
									<div class="creat_account">
										<input type="checkbox" id="f-option2" name="selector">
										<label for="f-option2">Tetapkan saya masuk</label>
									</div>
								</div>

								<div class="col-md-12 form-group">
									<button type="submit" value="submit" class="button button-register w-100">Daftar</button>
								</div>

								<div class="btn white darken-4 col s10 m4">
									<a href="{{ url('/user/login/google') }}" style="text-transform:none">
										<div class="left">
											<img width="28px" alt="Google &quot;G&quot; Logo" src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/512px-Google_%22G%22_Logo.svg.png"/> Masuk dengan Google
										</div>
									</a>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>
  </form>
  <!--================End Login Box Area =================-->



  @include('user/footer')

  <script src="{{ asset('aroma/vendors/jquery/jquery-3.2.1.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/bootstrap/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/skrollr.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/owl-carousel/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/nice-select/jquery.nice-select.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/jquery.ajaxchimp.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/mail-script.js') }}"></script>
  <script src="{{ asset('aroma/js/main.js') }}"></script>
  <script src="https://apis.google.com/js/platform.js" async defer></script>
  <script>
	function myFunction() {
		  var x = document.getElementById("password");
		  if (x.type === "password") {
			  x.type = "text";
		  } else {
			  x.type = "password";
		  }
	  }
  </script>
</body>
</html>