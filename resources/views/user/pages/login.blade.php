@extends('layouts.app')
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Quick Market - Masuk</title>
	<link rel="icon" href="{{ asset('about/QM Logo.png') }}" type="image/png">
    <link rel="stylesheet" href="{{ asset('aroma/vendors/bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('aroma/vendors/fontawesome/css/all.min.css') }}">
	<link rel="stylesheet" href="{{ asset('aroma/vendors/themify-icons/themify-icons.css') }}">
	<link rel="stylesheet" href="{{ asset('aroma/vendors/linericon/style.css') }}">
    <link rel="stylesheet" href="{{ asset('aroma/vendors/owl-carousel/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('aroma/vendors/owl-carousel/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('aroma/vendors/nice-select/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('aroma/vendors/nouislider/nouislider.min.css') }}">
  	<link rel="stylesheet" href="{{ asset('aroma/css/style.css') }}">
  	<link rel="stylesheet" href="{{ asset('aroma/css/scrollbar.css') }}">
</head>
<body>
    
    @include('user/header')
  
  <!-- ================ start banner area ================= -->	
	<section class="blog-banner-area" id="category">
		<div class="container h-100">
			<div class="blog-banner">
				<div class="text-center">
					<h1>Masuk</h1>
					<nav aria-label="breadcrumb" class="banner-breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="/user">Beranda</a></li>
              <li class="breadcrumb-item active" aria-current="page">Masuk</li>
            </ol>
          </nav>
				</div>
			</div>
    </div>
	</section>
	<!-- ================ end banner area ================= -->
  
  <!--================Login Box Area =================-->
  <form action="{{ route('login') }}" method="POST">
  		@csrf
		<section class="login_box_area section-margin">
			<div class="container">
				<div class="row">
					<div class="col-lg-6">
						<div class="login_box_img">
							<div class="hover">
								<h4>Baru mengunjungi website kami ?</h4>
								<p>Yuk daftar lalu dapatkan promo menarik dari website kami dan jadilah bagian dari website kami</p>
								<a class="button button-account" href="register">Buat Akun</a>
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="login_form_inner">
							<h3>Masuk</h3>
							<form class="row login_form" action="	#/" id="contactForm" >
								<div class="col-md-12 form-group">
									<input placeholder="Email" id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
									@error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                	@enderror
								</div>
								<div class="col-md-12 form-group">
									<input placeholder="Kata Sandi" id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" autofocus>
									@error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                	@enderror
								</div>

								<input type="checkbox" onclick="myFunction()" id="show">
								<label for="show">Tampilkan Kata Sandi</label> <br><br>

								<div class="col-md-12 form-group">
									<div class="creat_account">
										<div class="form-check">
                                    		<input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                    		<label class="form-check-label" for="remember">
                                        	{{ __('Ingat Saya') }}
                                    		</label>
                                		</div>
									</div>
								</div>
								<div class="col-md-12 form-group">
									<button type="submit" value="submit" class="button button-login w-100">Masuk</button>
									{{-- <br><br>
									@if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password ?') }}
                                    </a>
                                	@endif --}}
								</div>
								  
								<div class="btn white darken-4 col s10 m4">
									<a href="{{ url('/user/login/google') }}" style="text-transform:none">
										<div class="left">
											<img width="28px" alt="Google &quot;G&quot; Logo" src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/512px-Google_%22G%22_Logo.svg.png"/> Masuk dengan Google
										</div>
									</a>
							   </div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>
  </form>
	<!--================End Login Box Area =================-->

  @include('user/footer')

  <script src="{{ asset('aroma/vendors/jquery/jquery-3.2.1.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/bootstrap/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/skrollr.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/owl-carousel/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/nice-select/jquery.nice-select.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/jquery.ajaxchimp.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/mail-script.js') }}"></script>
  <script src="{{ asset('aroma/js/main.js') }}"></script>
  <script>
  function myFunction() {
		var x = document.getElementById("password");
		if (x.type === "password") {
			x.type = "text";
		} else {
			x.type = "password";
		}
	}
  </script>
</body>
</html>