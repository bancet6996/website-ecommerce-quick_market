@extends('layouts.app')
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Quick Market - Dompet</title>
  	<link rel="icon" href="{{ asset('about/QM Logo.png') }}" type="image/png">
    <link rel="stylesheet" href="{{ asset('aroma/vendors/bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('aroma/vendors/fontawesome/css/all.min.css') }}">
	<link rel="stylesheet" href="{{ asset('aroma/vendors/themify-icons/themify-icons.css') }}">
	<link rel="stylesheet" href="{{ asset('aroma/vendors/linericon/style.css') }}">
    <link rel="stylesheet" href="{{ asset('aroma/vendors/owl-carousel/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('aroma/vendors/owl-carousel/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('aroma/vendors/nice-select/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('aroma/vendors/nouislider/nouislider.min.css') }}">

  	<link rel="stylesheet" href="{{ asset('aroma/css/style.css') }}">
  	<link rel="stylesheet" href="{{ asset('aroma/css/scrollbar.css') }}">
</head>
<body>
    
	@include('user/header')
  
  <!-- ================ start banner area ================= -->	
	<section class="blog-banner-area" id="category">
		<div class="container h-100">
			<div class="blog-banner">
				<div class="text-center">
					<h1>Dompet</h1>
					<nav aria-label="breadcrumb" class="banner-breadcrumb">
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="/user">Beranda</a></li>
							<li class="breadcrumb-item active" aria-current="page">Pengaturan</li>
						</ol>
					</nav>
				</div>
			</div>
    </div>
	</section>
	<!-- ================ end banner area ================= -->
  
  <!--================Login Box Area =================-->
  {{-- <form method="POST" action="{{ route('register') }}">
	@csrf --}}
	@if(session()->has('message'))
		<div class="alert alert-info alert-dismissible fade show" role="alert">
		{{ session()->get('message') }}
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		</div>
	@endif
  		<section class="login_box_area section-margin">
			<div class="container">
				<div class="row">
					<div class="col-lg-6">
						<div class="login_box_img">
							<div class="hover">
								<h4>Saldo : Rp.{{ auth()->user()->qm_wallet }}</h4>
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="login_form_inner register_form_inner">
							<h3>Isi Saldo</h3>
							<form enctype="multipart/form-data" action="{{ url('user/voucher/fill/'. auth()->user()->id) }}" method="post">
								@csrf
								@method('PATCH')

								<div class="col-md-12 form-group">
									<input placeholder="Kode" type="text" class="form-control" name="kode" required autofocus>
								</div>

								<input type="hidden" name="_method" value="PUT">
    							<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

								<div class="col-md-12 form-group">
									<button type="submit" value="submit" class="btn btn-success btn-block" style="text-transform: uppercase">Isi</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>
  {{-- </form> --}}
	<!--================End Login Box Area =================-->



  @include('user/footer')


  <script src="{{ asset('aroma/vendors/jquery/jquery-3.2.1.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/bootstrap/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/skrollr.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/owl-carousel/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/nice-select/jquery.nice-select.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/jquery.ajaxchimp.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/mail-script.js') }}"></script>
  <script src="{{ asset('aroma/js/main.js') }}"></script>
</body>
</html>