@extends('layouts.app')
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Quick Market - Pengaturan</title>
  	<link rel="icon" href="{{ asset('about/QM Logo.png') }}" type="image/png">
    <link rel="stylesheet" href="{{ asset('aroma/vendors/bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('aroma/vendors/fontawesome/css/all.min.css') }}">
	<link rel="stylesheet" href="{{ asset('aroma/vendors/themify-icons/themify-icons.css') }}">
	<link rel="stylesheet" href="{{ asset('aroma/vendors/linericon/style.css') }}">
    <link rel="stylesheet" href="{{ asset('aroma/vendors/owl-carousel/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('aroma/vendors/owl-carousel/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('aroma/vendors/nice-select/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('aroma/vendors/nouislider/nouislider.min.css') }}">

  	<link rel="stylesheet" href="{{ asset('aroma/css/style.css') }}">
  	<link rel="stylesheet" href="{{ asset('aroma/css/scrollbar.css') }}">
</head>
<body>
    
	@include('user/header')
  
  <!-- ================ start banner area ================= -->	
	<section class="blog-banner-area" id="category">
		<div class="container h-100">
			<div class="blog-banner">
				<div class="text-center">
					<h1>Pengaturan Akun</h1>
					<nav aria-label="breadcrumb" class="banner-breadcrumb">
						<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/user">Beranda</a></li>
						<li class="breadcrumb-item active" aria-current="page">Pengaturan</li>
						</ol>
					</nav>
				</div>
			</div>
    </div>
	</section>
	<!-- ================ end banner area ================= -->
  
  <!--================Login Box Area =================-->
  {{-- <form method="POST" action="{{ route('register') }}">
    @csrf --}}
  		<section class="login_box_area section-margin">
			<div class="container">
				<div class="row">
					<div class="col-lg-6">
						<div class="login_box_img">
							<div class="hover">
								@if (auth()->user()->provider != 'google')
									<img class="img-fluid" src="{{ asset('uploads'.auth()->user()->image) }}" style="max-height: 610px" @if(auth()->user()->image === null) hidden @endif>
								@else
									<img class="img-fluid" alt="{{Auth::user()->name}}" src="{{Auth::user()->image}}" style="max-height: 610px"/>
								@endif
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="login_form_inner register_form_inner">
							<h3>Pengaturan Profile</h3>
							<form enctype="multipart/form-data" action="{{ url('user/setting/'.auth()->user()->name.'/update') }}" method="post">
								@csrf
								@method('PATCH')

								<div class="col-md-12 form-group">
									<input placeholder="Nama" id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ Auth::user()->name }}" required autocomplete="name" autofocus>
									@error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                               		 @enderror
								</div>

								<div class="col-md-12 form-group">
									<input readonly placeholder="Alamat Email" id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ Auth::user()->email }}" required autocomplete="email">
									@error('email')
										<span class="invalid-feedback" role="alert">
											<strong>{{ $message }}</strong>
										</span>
									@enderror
								</div>

								<div class="col-md-12 form-group">
									<label for="image">Pilih Foto Profil</label>
									<input @if(auth()->user()->image != null) title="Kosongkan jika tidak ingin mengganti foto profil" @endif type="file" class="form-control-file @error('image') is-invalid @enderror" name="image" id="image">
									@error('image')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                	@enderror
								</div>

								<div class="col-md-12 form-group">
									<input data-toggle="password" placeholder="Kata Sandi Baru" id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="new-password">
									@error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                	@enderror
								</div>

								<div class="col-md-12 form-group">
									<input placeholder="Konfirmasi Kata Sandi" id="password-confirm" type="password" class="form-control" name="password_confirmation" autocomplete="new-password">
								</div>

								<div @if(Auth::user()->provider != 'default') hidden @endif>
									<input type="checkbox" value="admin" name="provider"> Izinkan saya untuk dapat menjual produk
								</div>
								<br>

								<input type="checkbox" onclick="myFunction()" id="show">
								<label for="show">Tampilkan Kata Sandi</label> <br><br>

								<input type="hidden" name="_method" value="PUT">
    							<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

								<div class="col-md-12 form-group">
									<button type="submit" value="submit" class="btn btn-success btn-block" style="text-transform: uppercase">Simpan</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>
  {{-- </form> --}}
	<!--================End Login Box Area =================-->



  @include('user/footer')


  <script src="{{ asset('aroma/vendors/jquery/jquery-3.2.1.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/bootstrap/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/skrollr.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/owl-carousel/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/nice-select/jquery.nice-select.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/jquery.ajaxchimp.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/mail-script.js') }}"></script>
  <script src="{{ asset('aroma/js/main.js') }}"></script>
  <script>
	  $('#password').tooltip({'trigger':'focus', 'title': 'Kosongkan saja jika tidak ingin mengganti kata sandi'});
	  function myFunction() {
		var x = document.getElementById("password");
		if (x.type === "password") {
			x.type = "text";
		} else {
			x.type = "password";
		}
	}
  </script>
</body>
</html>