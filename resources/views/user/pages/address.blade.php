@extends('layouts.app')
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Quick Market - Pengaturan</title>
  <style>
      input[type=number]::-webkit-inner-spin-button, 
      input[type=number]::-webkit-outer-spin-button { 
        -webkit-appearance: none; 
        margin: 0; 
      }
      input[type=number] {
        -moz-appearance: textfield;
      }
  </style>
  <link rel="icon" href="{{ asset('about/QM Logo.png') }}" type="image/png">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/bootstrap/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/fontawesome/css/all.min.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/themify-icons/themify-icons.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/linericon/style.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/owl-carousel/owl.theme.default.min.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/vendors/owl-carousel/owl.carousel.min.css') }}">
  {{-- <link rel="stylesheet" href="{{ asset('aroma/vendors/nice-select/nice-select.css') }}"> --}}
  <link rel="stylesheet" href="{{ asset('aroma/vendors/nouislider/nouislider.min.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/css/style.css') }}">
  <link rel="stylesheet" href="{{ asset('aroma/css/scrollbar.css') }}">
</head>
<body>
    
	@include('user/header')
  
  <!-- ================ start banner area ================= -->	
	<section class="blog-banner-area" id="category">
		<div class="container h-100">
			<div class="blog-banner">
				<div class="text-center">
					<h1>Pengaturan Akun</h1>
					<nav aria-label="breadcrumb" class="banner-breadcrumb">
						<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/user">Beranda</a></li>
						<li class="breadcrumb-item active" aria-current="page">Pengaturan</li>
						</ol>
					</nav>
				</div>
			</div>
    </div>
	</section>
	<!-- ================ end banner area ================= -->
  
  <!--================Login Box Area =================-->
  {{-- <form method="POST" action="{{ route('register') }}">
    @csrf --}}
      @if(session()->has('message'))
      <div class="alert alert-secondary alert-dismissible fade show" role="alert">
          {{ session()->get('message') }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
      </div>
      @endif
  		<section class="login_box_area section-margin">
			<div class="container">
				<div class="row">
					<div class="col-lg-6">
						<div class="login_box_img">
							<div class="hover">
								@if (auth()->user()->provider != 'google')
									<img class="img-fluid" src="{{ asset('uploads'.auth()->user()->image) }}" style="max-height: 610px" @if(auth()->user()->image === null) hidden @endif>
								@else
									<img class="img-fluid" alt="{{Auth::user()->name}}" src="{{Auth::user()->image}}" style="max-height: 610px"/>
								@endif
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="login_form_inner register_form_inner">
							<h3>Pengaturan Alamat</h3>
							<form enctype="multipart/form-data" action="{{ url('user/address/'.auth()->user()->name.'/update') }}" method="post">
								@csrf
								@method('PATCH')

								<div class="col-md-12 form-group">
									<select name="provinsi" class="form-control" id="">
                    <option value="">Provinsi Asal</option>
                    @foreach ($provinces as $province => $value)
                    <option value="{{ $province }}">{{ $value }}</option>
                    @endforeach
                  </select>
								</div>

								<div class="col-md-12 form-group">
									<select name="kota" class="form-control" id="">
                    <option value="">Kota Asal</option>
                  </select>
								</div>

								<div class="col-md-12 form-group">
									<input type="text" class="form-control" placeholder="Kode Pos" name="kode_pos" value="{{ auth()->user()->kode_pos }}">
                </div>
                    
                <div class="col-md-12 form-group">
									<textarea name="alamat_lengkap" id="" cols="30" rows="5" class="form-control" placeholder="Alamat Lengkap" style="resize: none">{{ auth()->user()->alamat_lengkap }}</textarea>
								</div>

								<div class="col-md-12 form-group">
                  <input type="tel" class="form-control" placeholder="Nomor Telepon" name="telepon" value="{{ auth()->user()->telepon }}">
								</div>
								<br>

								<input type="hidden" name="_method" value="PUT">
    							<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

								<div class="col-md-12 form-group">
									<button type="submit" value="submit" class="btn btn-success btn-block" style="text-transform: uppercase">Simpan</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>
  {{-- </form> --}}
	<!--================End Login Box Area =================-->



  @include('user/footer')


  <script src="{{ asset('aroma/vendors/jquery/jquery-3.2.1.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/bootstrap/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/skrollr.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/owl-carousel/owl.carousel.min.js') }}"></script>
  {{-- <script src="{{ asset('aroma/vendors/nice-select/jquery.nice-select.min.js') }}"></script> --}}
  <script src="{{ asset('aroma/vendors/jquery.ajaxchimp.min.js') }}"></script>
  <script src="{{ asset('aroma/vendors/mail-script.js') }}"></script>
  <script src="{{ asset('aroma/js/main.js') }}"></script>
  <script>
	  $('#password').tooltip({'trigger':'focus', 'title': 'Kosongkan saja jika tidak ingin mengganti kata sandi'});
	  function myFunction() {
            var x = document.getElementById("password");
            if (x.type === "password") {
                x.type = "text";
            } else {
                x.type = "password";
            }
	    }

        $(document).ready(function () {
            $('select[name="provinsi"]').on('change', function(){
                let provinceId = $(this).val();
                if (provinceId) {
                    jQuery.ajax({
                        url : '/user/checkout/'+provinceId+'/cities',
                        type : "GET",
                        dataType : "json",
                        success:function(data){
                            $('select[name="kota"]').empty();
                            $.each(data, function(key, value){
                            console.log(value);
                                $('select[name="kota"]').append('<option value="'+ key +'">'+ value +'</option>');
                            });
                        },
                    });
                }
                else{
                    $('select[name="kota"]').empty();
                }
            });
        });
  </script>
</body>
</html>