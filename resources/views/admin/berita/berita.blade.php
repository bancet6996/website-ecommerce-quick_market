<!DOCTYPE html>
<html>
<head>
  <style>
      textarea {
        padding: 10px;
        vertical-align: top;
        width: 200px;
      }
      textarea:focus {
        outline-style: solid;
        outline-width: 2px;
      }
      #horizontal{
        display: inline-block;
      }
      #btn50{
        width: 146px;
      }
  </style>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Quick Market | Berita</title>
  <link rel="icon" href="{{ asset('about/QM Logo.png') }}" type="image/png">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
  <!-- iCheck -->
  <link rel="stylesheet" hrefBanner="{{ asset('adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- JQVMap -->
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/jqvmap/jqvmap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('adminlte/dist/css/adminlte.min.css') }}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/daterangepicker/daterangepicker.css') }}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/summernote/summernote-bs4.css') }}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <script>
    
  </script>
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

    @include('admin/header')

    @include('admin/sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Berita</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->

      <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
                <a href="{{ url('admin/berita/create') }}">Buat Berita Baru</a>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
        @if(session()->has('message'))
        <div class="alert alert-secondary alert-dismissible fade show" role="alert">
            {{ session()->get('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif
        @foreach ($berita as $row)
        @if (Auth::user()->name === $row->penulis)
        <div id="horizontal" class="card" style="width: 21rem;">
          <img src="{{ asset('uploads'. $row->thumbnail_berita) }}" height="210px" class="card-img-top">
          <div class="card-body" id="#horizontal">
            <h3 class="card-title"><b>{{ $row->judul_berita }}</b></h3>
            <br>
            <a id="btn50" class="btn btn-info" href="{{ url('admin/berita/' . $row->id . '/baca') }}">Baca</a>
            <a id="btn50" class="btn btn-primary" href="{{ url('admin/berita/' . $row->id . '/edit') }}">Ubah</a>
            <hr>
            <form action="{{ url('admin/berita', $row->id) }}" method="POST">
              @method('PATCH')
              @csrf
              <button class="btn btn-danger btn-block" type="submit">Hapus</button>
            </form>
          </div>
        </div>
        @else
        <div hidden id="horizontal" class="card" style="width: 21rem;">
          <img src="{{ asset('uploads'. $row->thumbnail_berita) }}" height="210px" class="card-img-top">
          <div class="card-body" id="#horizontal">
            <h3 class="card-title"><b>{{ $row->judul_berita }}</b></h3>
            <br>
            <a id="btn50" class="btn btn-info" href="{{ url('admin/berita/' . $row->id . '/baca') }}">Baca</a>
            <a id="btn50" class="btn btn-primary" href="{{ url('admin/berita/' . $row->id . '/edit') }}">Ubah</a>
            <hr>
            <form action="{{ url('admin/berita', $row->id) }}" method="POST">
              @method('DELETE')
              @csrf
              <button class="btn btn-danger btn-block" type="submit">Hapus</button>
            </form>
          </div>
        </div>
        @endif
        @endforeach
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  {{-- <footer id="footer" class="main-footer">
      <!-- To the right -->
      <div class="float-right d-none d-sm-inline">
        Anything you want
      </div>
      <!-- Default to the left -->
      <strong>Copyright &copy; 2014-2019 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
  </footer> --}}
  @include('admin/footer')

  <!-- Control Sidebar -->
  <aside clasbanner.blade.phps="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{ asset('adminlte/plugins/jquery/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('adminlte/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{ asset('adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- ChartJS -->
<script src="{{ asset('adminlte/plugins/chart.js') }}/Chart.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('adminlte/plugins/sparklines/sparkline.js') }}"></script>
<!-- JQVMap -->
<script src="{{ asset('adminlte/plugins/jqvmap/jquery.vmap.min.js') }}"></script>
<script src="{{ asset('adminlte/plugins/jqvmap/maps/jquery.vmap.world.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('adminlte/plugins/jquery-knob/jquery.knob.min.js') }}"></script>
<!-- daterangepicker -->
<script src="{{ asset('adminlte/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('adminlte/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ asset('adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<!-- Summernote -->
<script src="{{ asset('adminlte/plugins/summernote/summernote-bs4.min.js') }}"></script>
<!-- overlayScrollbars -->
<script src="{{ asset('adminlte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('adminlte/dist/js/adminlte.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ asset('adminlte/dist/js/pages/dashboard.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('adminlte/dist/js/demo.js') }}"></script>
<!-- Autosize Textarea -->
<script src="{{ asset('adminlte/dist/js/autosize.js') }}"></script>
<script>
		autosize(document.querySelectorAll('textarea'));
</script>
</body>
</html>