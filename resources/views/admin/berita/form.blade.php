<!DOCTYPE html>
<html>
<head>
  <style>
    #save{
      position: -webkit-sticky;
      position: sticky;
      bottom: 0;
    }
      .container {
      padding: 50px 200px;
      }
      .box {
        position: relative;
        background: #ffffff;
        width: 100%;
      }
      .box-header {
        color: #444;
        display: block;
        padding: 10px;
        position: relative;
        border-bottom: 1px solid #f4f4f4;
        margin-bottom: 10px;
      }
      .box-tools {
        position: absolute;
        right: 10px;
        top: 5px;
      }
      .dropzone-wrapper {
        border: 2px dashed #91b0b3;
        color: #92b0b3;
        position: relative;
        height: 150px;
      }
      .dropzone-desc {
        position: absolute;
        margin: 0 auto;
        left: 0;
        right: 0;
        text-align: center;
        width: 40%;
        top: 50px;
        font-size: 16px;
      }
      .dropzone,
      .dropzone:focus {
        position: absolute;
        outline: none !important;
        width: 100%;
        height: 150px;
        cursor: pointer;
        opacity: 0;
      }
      .dropzone-wrapper:hover,
      .dropzone-wrapper.dragover {
        background: #ecf0f5;
      }
      .preview-zone {
        text-align: center;
      }
      .preview-zone .box {
        box-shadow: none;
        border-radius: 0;
        margin-bottom: 0;
      }
    }
  </style>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Quick Market | Berita</title>
  <link rel="icon" href="{{ asset('about/QM Logo.png') }}" type="image/png">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
  <!-- iCheck -->
  <link rel="stylesheet" hrefBanner="{{ asset('adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- JQVMap -->
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/jqvmap/jqvmap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('adminlte/dist/css/adminlte.min.css') }}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/daterangepicker/daterangepicker.css') }}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/summernote/summernote-bs4.css') }}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

    @include('admin/header')

    @include('admin/sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Berita</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->

      <form id="form" enctype="multipart/form-data" action="{{ url('/admin/berita', @$berita->id) }}" method="POST">
        @csrf

          @if(!@empty($berita))
              @method('PATCH')
          @endif

        <div class="col-md-14">
          <div class="card card-secondary">
            <div class="card-header">
              <h3 class="card-title" style="margin-top: 9px">Buat Berita Baru</h3>
              <div class="card-tools">
                <a href="/admin/berita" style="margin-right: 21px">
                  <button type="button" class="btn btn-secondary" data-toggle="tooltip" data-placement="left" title="Kembali">
                    <i class="fas fa-chevron-left"></i>
                  </button>
                </a>
              </div>
            </div>
            <div class="card-body">
                @if ($errors->any())
                  <div class="alert alert-danger">
                    <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div>
                @endif
              <div class="form-group">
                <label>Judul Berita</label>
                <input type="text" class="form-control" name="judul_berita" value="{{ old('judul_berita', @$berita->judul_berita) }}">
              </div>
              <div class="form-group">
                <label>Caption Berita</label>
                <input id="caption" type="text" class="form-control" name="caption_berita" value="{{ old('caption_berita', @$berita->caption_berita) }}">
              </div>
              <div class="form-group">
                <label for="">Kategori Berita</label>
                <select name="kategori_berita" id="" class="form-control">
                  <option value="">Kategori Berita</option>
                  @foreach ($kategori as $row)
                    <option value="{{ $row->nama_kategori }}">{{ $row->nama_kategori }}</option>                  
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                  <div class="container">
                      <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                        <div class="preview-zone hidden">
                          <div class="box box-solid">
                          <div class="box-header with-border">
                            <div><b>Thumbnail Berita</b></div>
                            <div class="box-tools pull-right">
                            <button type="button" class="btn btn-danger btn-xs remove-preview">
                              <i class="fa fa-times"></i> Reset Gambar
                            </button>
                            </div>
                          </div>
                          <div class="box-body"></div>
                          </div>
                        </div>
                        <div class="dropzone-wrapper">
                          <div class="dropzone-desc">
                            <i class="glyphicon glyphicon-download-alt"></i>
                            <p>Pilih gambar atau drag gambar kesini</p>
                          </div>
                          <input type="file" name="thumbnail_berita" class="dropzone">
                        </div>
                        </div>
                      </div>
                      </div>
                    </div>
                </div>
              <div class="form-group">
                <label>Deskripsi</label><br>
                <textarea name="deskripsi_berita" id="" cols="28" rows="14" class="summernote">{{ old('deskripsi_berita', @$berita->deskripsi_berita)}}</textarea>
                {{-- <div class="summernote"></div><br> --}}
                {{-- <button id="edit" class="btn btn-secondary btn-block" onclick="edit()" type="button">Ubah Deskripsi</button> --}}
                {{-- <button id="save" class="btn btn-success btn-block" onclick="save()" type="button">Simpan Deskripsi</button> --}}
              </div>
              <br>
              <input class="btn btn-success btn-block" type="submit" value="Kirim Berita">
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
    </form>
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  {{-- <footer id="footer" class="main-footer">
      <!-- To the right -->
      <div class="float-right d-none d-sm-inline">
        Anything you want
      </div>
      <!-- Default to the left -->
      <strong>Copyright &copy; 2014-2019 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
  </footer> --}}
  @include('admin/footer')

  <!-- Control Sidebar -->
  <aside clasbanner.blade.phps="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{ asset('adminlte/plugins/jquery/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('adminlte/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{ asset('adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- ChartJS -->
<script src="{{ asset('adminlte/plugins/chart.js') }}/Chart.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('adminlte/plugins/sparklines/sparkline.js') }}"></script>
<!-- JQVMap -->
<script src="{{ asset('adminlte/plugins/jqvmap/jquery.vmap.min.js') }}"></script>
<script src="{{ asset('adminlte/plugins/jqvmap/maps/jquery.vmap.world.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('adminlte/plugins/jquery-knob/jquery.knob.min.js') }}"></script>
<!-- daterangepicker -->
<script src="{{ asset('adminlte/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('adminlte/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ asset('adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<!-- Summernote -->
<script src="{{ asset('adminlte/plugins/summernote/summernote-bs4.min.js') }}"></script>
<script src="{{ asset('adminlte/plugins/summernote/lang/summernote-id-ID.js') }}"></script>
<script src="{{ asset('adminlte/plugins/summernote/lang/summernote-id-ID.min.js') }}"></script>
<!-- overlayScrollbars -->
<script src="{{ asset('adminlte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('adminlte/dist/js/adminlte.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ asset('adminlte/dist/js/pages/dashboard.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('adminlte/dist/js/demo.js') }}"></script>
<!-- Autosize Textarea -->
<script src="{{ asset('adminlte/dist/js/autosize.js') }}"></script>
<script>
		autosize(document.querySelectorAll('textarea'));
</script>
<script>
  $('#caption').tooltip({'trigger':'focus', 'title': 'Dapat dikosongkan'});
  $(function () {
    // Summernote
    $('.summernote').summernote({
      height: 777,
      placeholder: 'Tulis disini...',
      disableDragAndDrop: false,
      lang : "id-ID",
      spellCheck: true,
      toolbar: [
        // [groupName, [list of button]]
        ['style', ['style', 'bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough', 'superscript', 'subscript']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
        ['fontname', ['fontname']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['forecolor', ['forecolor']],
        ['backcolor', ['backcolor']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['insert', ['link', 'picture', 'video', 'table', 'hr']],
        ['view', ['fullscreen', 'codeview', 'undo', 'redo', 'help']],
      ],
    });
  })

  // var edit = function() {
  // $('.summernote').summernote({
  //   focus: true,
  //   height: 2100
  //   });
  // };

  // var save = function() {
  //   var markup = $('.summernote').summernote('code');
  //   $('.summernote').summernote('destroy');
  // };

  function readFile(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        
        reader.onload = function (e) {
        var htmlPreview = 
        '<img width="200" src="' + e.target.result + '" />'+
        '<p>' + input.files[0].name + '</p>';
        var wrapperZone = $(input).parent();
        var previewZone = $(input).parent().parent().find('.preview-zone');
        var boxZone = $(input).parent().parent().find('.preview-zone').find('.box').find('.box-body');
        
        wrapperZone.removeClass('dragover');
        previewZone.removeClass('hidden');
        boxZone.empty();
        boxZone.append(htmlPreview);
        };
        
        reader.readAsDataURL(input.files[0]);
      }
    }
    function reset(e) {
      e.wrap('<form>').closest('form').get(0).reset();
      e.unwrap();
    }
    $(".dropzone").change(function(){
      readFile(this);
    });
    $('.dropzone-wrapper').on('dragover', function(e) {
      e.preventDefault();
      e.stopPropagation();
      $(this).addClass('dragover');
    });
    $('.dropzone-wrapper').on('dragleave', function(e) {
      e.preventDefault();
      e.stopPropagation();
      $(this).removeClass('dragover');
    });
    $('.remove-preview').on('click', function() {
      var boxZone = $(this).parents('.preview-zone').find('.box-body');
      var previewZone = $(this).parents('.preview-zone');
      var dropzone = $(this).parents('.form-group').find('.dropzone');
      boxZone.empty();
      previewZone.addClass('hidden');
      reset(dropzone);
    });
</script>
</body>
</html>