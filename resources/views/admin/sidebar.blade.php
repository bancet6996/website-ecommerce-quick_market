<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="/" class="brand-link">
    <img src="{{ asset('about/QM Logo.png') }}" alt="Quick Market Logo" class="brand-image img-circle" style="opacity: .8">
    <span class="brand-text font-weight-light">Quick Market</span>
  </a>
  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        @if (auth()->user()->provider != 'google')
          <img src="{{ asset('uploads'.auth()->user()->image) }}" class="img-circle" alt="User Image" @if(auth()->user()->image === null) hidden @endif>
        @endif
      </div>
      <div class="info">
        <a href="{{ url('user/setting/'.auth()->user()->name) }}" class="d-block">{{ Auth::user()->name }}</a>
      </div>
    </div>
    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        @if (Auth::user()->provider === 'superadmin')
          <li class="nav-item has-treeview">
            <a href="/admin/notification" class="nav-link @if(Request::path() === 'admin/notification') active @endif">
              <i class="nav-icon fas fa-bell"></i>
              <p>
                Notifikasi <span class="badge badge-info right">{{ $notifikasi }}</span>
              </p>
            </a>
          </li>
          {{-- <li class="nav-item has-treeview">
            <a href="/admin/transaction" class="nav-link @if(Request::path() === 'admin/transaction') active @endif">
              <i class="nav-icon fas fa-money-check-alt"></i>
              <p>
                Transaksi
              </p>
            </a>
          </li> --}}
          <li class="nav-item has-treeview">
            <a href="/admin/order" class="nav-link @if(Request::path() === 'admin/order') active @endif">
              <i class="nav-icon fas fa-clipboard-list"></i>
              <p>
                Pesanan <span class="badge badge-info right">{{ $total_pesanan }}</span>
              </p>
            </a>
          </li>
          <li @if(Auth::user()->provider != 'superadmin') hidden @endif class="nav-item has-treeview">
            <a href="/admin/banner" class="nav-link @if(Request::path() === 'admin/banner' || Request::path() === 'admin/banner/create' || Request::path() === 'admin/banner/'. @$banner->id .'/edit') active @endif">
              <i class="nav-icon fas fa-digital-tachograph"></i>
              <p>
                Banner
              </p>
            </a>
          </li>
          <li @if(Auth::user()->provider != 'superadmin') hidden @endif class="nav-item has-treeview">
            <a href="/admin/adminuser" class="nav-link @if(Request::path() === 'admin/adminuser') active @endif">
              <i class="nav-icon fas fa-user-check"></i>
              <p>
                Safe List
              </p>
            </a>
          </li>
          <li @if(Auth::user()->provider != 'superadmin') hidden @endif class="nav-item has-treeview">
            <a href="/admin/banned-list" class="nav-link @if(Request::path() === 'admin/banned-list') active @endif">
              <i class="nav-icon fas fa-user-slash"></i>
              <p>
                Banned List
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="/admin/produk" class="nav-link @if(Request::path() === 'admin/produk' || Request::path() === 'admin/produk/create' || Request::path() === 'admin/produk/'. @$produk->id .'/edit') active @endif">
              <i class="nav-icon fas fa-shopping-bag"></i>
              <p>
                Produk
              </p>
            </a>
          </li>
          <li @if(Auth::user()->provider != 'superadmin') hidden @endif class="nav-item has-treeview">
              <a href="/admin/kategori-produk" class="nav-link @if(Request::path() === 'admin/kategori-produk' || Request::path() === 'admin/kategori-produk/create' || Request::path() === 'admin/kategori-produk/'. @$kategori_produk->id .'/edit') active @endif">
                <i class="nav-icon fas fa-boxes"></i>
                <p>
                  Kategori Produk
                </p>
              </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="/admin/berita" class="nav-link @if(Request::path() === 'admin/berita' || Request::path() === 'admin/berita/create' || Request::path() === 'admin/berita/'. @$berita->id .'/edit' || Request::path() === 'admin/berita/'. @$berita->id .'/baca') active @endif">
              <i class="nav-icon fas fa-newspaper"></i>
              <p>
                Berita
              </p>
            </a>
          </li>
          <li @if(Auth::user()->provider != 'superadmin') hidden @endif class="nav-item has-treeview">
            <a href="/admin/kategori-berita" class="nav-link @if(Request::path() === 'admin/kategori-berita' || Request::path() === 'admin/kategori-berita/create' || Request::path() === 'admin/kategori-berita/'. @$kategori_berita->id .'/edit') active @endif">
              <i class="nav-icon fas fa-tags"></i>
              <p>
                Kategori Berita
              </p>
            </a>
          </li>
          <li @if(Auth::user()->provider != 'superadmin') hidden @endif class="nav-item has-treeview">
            <a href="/admin/validasi-berita" class="nav-link @if(Request::path() === 'admin/validasi-berita') active @endif">
              <i class="nav-icon fas fa-check"></i>
              <p>
                Validasi Berita <span class="badge badge-info right">{{ $unvalidate }}</span>
              </p>
            </a>
          </li>
          <li @if(Auth::user()->provider != 'superadmin') hidden @endif class="nav-item has-treeview">
            <a href="/admin/unvalidasi-berita" class="nav-link @if(Request::path() === 'admin/unvalidasi-berita') active @endif">
              <i class="nav-icon fas fa-check-double"></i>
              <p>
                Unvalidasi Berita
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="/admin/inbox" class="nav-link @if(Request::path() === 'admin/inbox') active @endif">
              <i class="nav-icon fas fa-envelope"></i>
              <p>
                Kotak Masuk <span class="badge badge-info right">{{ $unread }}</span>
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="/admin/outbox" class="nav-link @if(Request::path() === 'admin/outbox') active @endif">
              <i class="nav-icon fas fa-envelope-open"></i>
              <p>
                Kotak Keluar
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
          <a href="/admin/voucher" class="nav-link @if(Request::path() === 'admin/voucher' || Request::path() === 'admin/voucher/create') active @endif">
              <i class="nav-icon fas fa-ticket-alt"></i>
              <p>
                Voucher
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="/admin/trash" class="nav-link @if(Request::path() === 'admin/trash') active @endif">
              <i class="nav-icon fas fa-trash-alt"></i>
              <p>
                Sampah
              </p>
            </a>
          </li>
          {{-- <!-- <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-money-check-alt"></i>
              <p>
                Transaksi
              </p>
            </a>
          </li>
        </ul> --> --}}
        <li class="nav-item has-treeview">
          <a href="/admin/about" class="nav-link @if(Request::path() === 'admin/about') active @endif">
            <i class="nav-icon fas fa-info-circle"></i>
            <p>
              Tentang
            </p>
          </a>
        </li>
        @else
          <li class="nav-item has-treeview">
            <a href="/admin/notification" class="nav-link @if(Request::path() === 'admin/notification') active @endif">
              <i class="nav-icon fas fa-bell"></i>
              <p>
                Notifikasi <span class="badge badge-info right">{{ $notifikasi  }}</span>
              </p>
            </a>
          </li>
          {{-- <li class="nav-item has-treeview">
            <a href="/admin/transaction" class="nav-link @if(Request::path() === 'admin/transaction') active @endif">
              <i class="nav-icon fas fa-money-check-alt"></i>
              <p>
                Transaksi
              </p>
            </a>
          </li> --}}
          <li class="nav-item has-treeview">
            <a href="/admin/order" class="nav-link @if(Request::path() === 'admin/order') active @endif">
              <i class="nav-icon fas fa-clipboard-list"></i>
              <p>
                Pesanan <span class="badge badge-info right">{{ $total_pesanan }}</span>
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="/admin/produk" class="nav-link @if(Request::path() === 'admin/produk' || Request::path() === 'admin/produk/create') active @endif">
              <i class="nav-icon fas fa-shopping-bag"></i>
              <p>
                Produk
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="/admin/berita" class="nav-link @if(Request::path() === 'admin/berita' || Request::path() === 'admin/berita/create') active @endif">
              <i class="nav-icon fas fa-newspaper"></i>
              <p>
                Berita
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="/admin/inbox" class="nav-link @if(Request::path() === 'admin/inbox') active @endif">
              <i class="nav-icon fas fa-envelope"></i>
              <p>
                Pesan Masuk <span class="badge badge-info right">{{ $unread }}</span>
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="/admin/outbox" class="nav-link @if(Request::path() === 'admin/outbox') active @endif">
              <i class="nav-icon fas fa-envelope-open"></i>
              <p>
                Kotak Keluar
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="/admin/trash" class="nav-link @if(Request::path() === 'admin/trash') active @endif">
              <i class="nav-icon fas fa-trash-alt"></i>
              <p>
                Sampah
              </p>
            </a>
          </li>
          {{-- <!-- <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-money-check-alt"></i>
              <p>
                Transaksi
              </p>
            </a>
          </li>
        </ul> --> --}}
        <li class="nav-item has-treeview">
          <a href="/admin/about" class="nav-link @if(Request::path() === 'admin/about') active @endif">
            <i class="nav-icon fas fa-info-circle"></i>
            <p>
              Tentang
            </p>
          </a>
        </li>
        @endif
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>