<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Quick Market | Inbox</title>
  <link rel="icon" href="{{ asset('about/QM Logo.png') }}" type="image/png">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
  <!-- iCheck -->
  <link rel="stylesheet" hrefBanner="{{ asset('adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- JQVMap -->
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/jqvmap/jqvmap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('adminlte/dist/css/adminlte.min.css') }}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/daterangepicker/daterangepicker.css') }}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/summernote/summernote-bs4.css') }}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

    @include('admin/header')

    @include('admin/sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div id="cardInbox" class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Inbox</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->

      <!-- Main content -->
      <div class="card bg-light mb-3">
        <div class="card-header">
          <h3 class="card-title">
            <a href="/user/contact">Kirim Pesan</a>
          </h3>
        </div>
        <div class="card-body">
          @if(session()->has('message'))
          <div class="alert alert-secondary alert-dismissible fade show" role="alert">
              {{ session()->get('message') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
          </div>
          @endif
          <table class="table table-hover table-striped" id="example2">
            <thead class="thead-dark" align="center">
              <tr>
                <th>Dari</th>
                <th>Email</th>
                <th>Subjek</th>
                <th>Dikirim Pada</th>
                <th>Baca</th>
                <th>Hapus</th>
              </tr>
            </thead>
            <tbody align="center">
            @foreach ($collection as $item)
            @if (Auth::user()->name != $item->kepada)
            <tr hidden>
              <td class="mailbox-name">{{ $item->name }}</td>
              <td class="mailbox-name">{{ $item->email }}</td>
              <td class="mailbox-subject">{{ $item->subject }}</td>
              {{-- <td class="mailbox-date">{{ date('H:i:s d-F-Y', strtotime($item->created_at)) }}</td> --}}
              {{-- <td class="mailbox-date">{{ date('l, d F Y, H:i', strtotime($item->created_at)) }}</td> --}}
              <td class="mailbox-date">{{ Carbon\Carbon::parse($item->created_at)->diffForHumans() }}</td>
              <td><a href="{{ url('/admin/inbox/'. $item->id .'/read') }}" class="btn btn-info btn-block"><i class="fa fa-eye"></i></a></td>
              <td>
                <form action="{{ url('admin/inbox', $item->id) }}" method="POST">
                  @method('PATCH')
                  @csrf
                  <button class="btn btn-danger btn-block" type="submit">Hapus</button>
                </form>
              </td>
            </tr>
            @else
            <tr>
              <td class="mailbox-name">{{ $item->name }}</td>
              <td class="mailbox-name">{{ $item->email }}</td>
              <td class="mailbox-subject">{{ $item->subject }}</td>
              <td class="mailbox-date">{{ Carbon\Carbon::parse($item->created_at)->diffForHumans() }}</td>
              <td><a href="{{ url('/admin/inbox/'. $item->id .'/read') }}" data-toggle="tooltip" data-placement="right" title="Baca" @if($item->read_inbox != 'readed') class="btn btn-primary" @else class="btn btn-success" @endif><i class="fa fa-eye"></i></a></td>
              <td>
                <form action="{{ url('admin/inbox', $item->id) }}" method="POST">
                  @method('PATCH')
                  @csrf
                  <button class="btn btn-danger" type="submit" data-toggle="tooltip" data-placement="right" title="Hapus"><i class="far fa-trash-alt"></i></button>
                </form>
              </td>
            </tr>
            @endif
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
      
      </div>
      <!-- /.row -->
    <!-- /.content -->
    </div>
    <!-- /.content-header -->
  </div>
  <!-- /.content-wrapper -->
  <!-- Main Footer -->
  {{-- <footer id="footer" class="main-footer">
      <!-- To the right -->
      <div class="float-right d-none d-sm-inline">
        Anything you want
      </div>
      <!-- Default to the left -->
      <strong>Copyright &copy; 2014-2019 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
  </footer> --}}
  @include('admin/footer')

  <!-- Control Sidebar -->
  <aside clasbanner.blade.phps="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{ asset('adminlte/plugins/jquery/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('adminlte/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{ asset('adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- ChartJS -->
<script src="{{ asset('adminlte/plugins/chart.js') }}/Chart.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('adminlte/plugins/sparklines/sparkline.js') }}"></script>
<!-- JQVMap -->
<script src="{{ asset('adminlte/plugins/jqvmap/jquery.vmap.min.js') }}"></script>
<script src="{{ asset('adminlte/plugins/jqvmap/maps/jquery.vmap.world.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('adminlte/plugins/jquery-knob/jquery.knob.min.js') }}"></script>
<!-- daterangepicker -->
<script src="{{ asset('adminlte/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('adminlte/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ asset('adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<!-- Summernote -->
<script src="{{ asset('adminlte/plugins/summernote/summernote-bs4.min.js') }}"></script>
<!-- overlayScrollbars -->
<script src="{{ asset('adminlte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('adminlte/dist/js/adminlte.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ asset('adminlte/dist/js/pages/dashboard.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('adminlte/dist/js/demo.js') }}"></script>
<!-- DataTables -->
<script src="{{ asset('adminlte/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script>
  $(function () {
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": false,
      "info": false,
      "autoWidth": true,
    });
  });
</script>
</body>
</html>