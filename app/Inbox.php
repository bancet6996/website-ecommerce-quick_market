<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inbox extends Model
{
    protected $table = 'inbox';
    protected $fillable = ['name', 'email', 'kepada', 'subject', 'message', 'read_inbox', 'delete_status_in', 'delete_status_out'];
    public $timestamps = true;
}
