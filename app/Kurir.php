<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kurir extends Model
{
    protected $table = 'kurir';
    protected $fillable = [ 'jalur', 'skt_kurir', 'pjg_kurir', 'skt_jenis_layanan', 'pjg_jenis_layanan', 'harga', 'total_berat', 'estimasi'];
}
