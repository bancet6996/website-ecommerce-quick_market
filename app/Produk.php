<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    protected $table = 'produk';
    protected $fillable = ['pemilik', 'nama_produk', 'kategori_produk', 'gambar_produk', 'deskripsi_produk', 'harga_produk', 'stok_produk', 'berat_produk', 'lebar_produk', 'tinggi_produk', 'ketebalan_produk', 'deskripsi_lengkap_produk', 'delete_status', 'tautan', 'favorit', 'rating', 'trending', 'kota',];
    public $timestamps = true;
}
