<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komentar extends Model
{
    protected $table = 'komentar';
    protected $fillable = ['category', 'to_post', 'provider', 'name', 'image', 'email', 'comment', 'date', 'delete_status',];
    public $timestamps = true;
}
