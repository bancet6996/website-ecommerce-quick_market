<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $table = 'rating';
    protected $fillable = ['to_post', 'provider', 'name', 'image', 'email', 'rating', 'comment', 'date', 'delete_status',];
    public $timestamps = true;
}
