<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $table = 'banner';
    protected $fillable = ['pemilik', 'gambar_banner', 'tautan', 'judul', 'deskripsi'];
    public $timestamps = true;
}
