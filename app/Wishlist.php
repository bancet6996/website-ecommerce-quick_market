<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{
    protected $table = 'wishlist';
    protected $fillable = ['pemilik', 'id_produk', 'nama_produk', 'gambar_produk', 'deskripsi_produk', 'harga_produk',];
}
