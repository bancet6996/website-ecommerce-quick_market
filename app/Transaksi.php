<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    protected $table = 'transaksi';
    protected $fillable = ['pemilik', 'kurir', 'jenis_layanan', 'metode_pembayaran', 'ongkir', 'total_harga', 'status_pembayaran', 'no_resi',];
    // protected $fillable = ['pemilik', 'alamat_pengirim', 'kota_pengirim', 'kode_pos_pengirim', 'alamat_penerima', 'kota_penerima', 'kode_pos_penerima', 'metode_pembayaran', 'total_harga'];
}
