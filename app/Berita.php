<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Berita extends Model
{
    //
    protected $table = 'berita';
    protected $fillable = ['penulis', 'judul_berita', 'caption_berita', 'kategori_berita', 'thumbnail_berita', 'deskripsi_berita', 'files', 'kunjungan', 'validasi', 'kode_berita', 'delete_status'];
    public $timestamps = true;
}
