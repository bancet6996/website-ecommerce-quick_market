<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notifikasi extends Model
{
    protected $table = 'notifikasi';
    protected $fillable = ['pemilik', 'to_post', 'subjek', 'category', 'status',];
    public $timestamps = true;
}
