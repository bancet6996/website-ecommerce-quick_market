<?php

namespace App\Http\Controllers;

use App\Berita;
use App\Inbox;
use App\Keranjang;
use App\Notifikasi;
use App\Pesanan;
use App\User;
use App\Voucher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VoucherController extends Controller
{
    public function index(){
        $data['count_produk'] = Keranjang::where('pemilik', auth()->user()->name)->count();
        return view('user/pages/voucher', $data);
    }

    public function isi(Request $request){
        $voucher = Voucher::where('kode', $request->kode)->first();
        if ($voucher) {
            $user = User::find(Auth::id());
            $user->qm_wallet = $user->qm_wallet + $voucher->nominal;
            $user->save();
            $voucher->delete();

            return redirect()->back()->with('message', 'Saldo berhasil diisi');
        } else {
            return redirect()->back()->with('message', 'Kode salah atau sudah digunakan');
        }

    }

    public function indexAdmin(){
        $data['voucher'] = Voucher::get();
        $data['unread'] = Inbox::where('read_inbox', 'unread')->where('kepada', '=', auth()->user()->name)->where('delete_status_in', 'no')->count();
        $data['unvalidate'] = Berita::where('validasi', 'belum')->count();
        $data['notifikasi'] = Notifikasi::where('pemilik', auth()->user()->name)->count();
        $data['total_pesanan'] = Pesanan::where('pemilik_produk', auth()->user()->name)->count();
        return view('admin/voucher/voucher', $data);
    }

    public function acakKode(){
        $data['unread'] = Inbox::where('read_inbox', 'unread')->where('kepada', '=', auth()->user()->name)->where('delete_status_in', 'no')->count();
        $data['unvalidate'] = Berita::where('validasi', 'belum')->count();
        $data['notifikasi'] = Notifikasi::where('pemilik', auth()->user()->name)->count();
        $data['total_pesanan'] = Pesanan::where('pemilik_produk', auth()->user()->name)->count();
        return view('admin/voucher/form', $data);
    }

    public function buatKode(Request $request){
        if ($request->kode === null) {
            Voucher::create([
                'kode' => mt_rand(),
                'nominal' => $request->nominal,
            ]);
            return redirect('/admin/voucher');
        } else {
            Voucher::create([
                'kode' => $request->kode,
                'nominal' => $request->nominal,
            ]);
            return redirect('/admin/voucher');
        }
    }
}
