<?php

namespace App\Http\Controllers;

use App\Berita;
use App\Inbox;
use App\Keranjang;
use App\Kurir;
use App\Notifikasi;
use App\Pesanan;
use App\Produk;
use App\Transaksi;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TransaksiController extends Controller
{
    public function index(){
        $max = Pesanan::where('pemilik', auth()->user()->name)->max('akumulasi_pembelian');
        if (Pesanan::where('pemilik', auth()->user()->name)->where('akumulasi_pembelian', $max)->first() === null) {
            return redirect()->back();
        } else {
            $data['count_produk'] = Keranjang::where('pemilik', auth()->user()->name)->count();
            $data['transaksi'] = Transaksi::where('pemilik', auth()->user()->name)->orderBy('created_at', 'desc')->first();
            $data['pesanan'] = Pesanan::where('pemilik', auth()->user()->name)->where('akumulasi_pembelian', $max)->get();
            $data['subtotal'] = Pesanan::where('pemilik', auth()->user()->name)->sum('total_harga');
            return view('user/shop/transaction/transaction', $data);
        }
    }

    public function pilihOngkir($id){
        $kurir = Kurir::find($id);
        $data = collect([$kurir->skt_kurir, $kurir->skt_jenis_layanan, $kurir->harga]);
        session()->put('ongkir', $data);
        return redirect()->back();
    }

    public function pesan(Request $request){
        $max_ps = Pesanan::max('no_resi');
        $increment = $max_ps + 1;
        if ($request->via === 'COD') {
            Transaksi::create([
                'pemilik' => auth()->user()->name,
                'kurir' => $request->kurir,
                'jenis_layanan' => $request->jenis_layanan,
                'metode_pembayaran' => $request->via,
                'ongkir' => $request->ongkir,
                'total_harga' => $request->total,
                'status_pembayaran' => 'belum',
                'no_resi' => str_pad($increment,7,0, STR_PAD_LEFT),
            ]);
            $keranjang = Keranjang::where('pemilik', auth()->user()->name)->get();
            $ed = Pesanan::where('pemilik', auth()->user()->name)->max('akumulasi_pembelian');

            if (session('cart')) {
                foreach (session('cart') as $id => $details) {
                    Pesanan::create([
                        'pemilik' => auth()->user()->name,
                        'id_produk' => $details['id_produk'],
                        'pemilik_produk' => $details['pemilik_produk'],
                        'nama_produk' => $details['nama_produk'],
                        'gambar_produk' => $details['gambar_produk'],
                        'deskripsi_produk' => $details['deskripsi_produk'],
                        'harga_produk' => $details['harga_produk'],
                        'berat_produk' => $details['berat_produk'],
                        'kuantitas' => $details['kuantitas'],
                        'total_harga' => $details['harga_produk'] * $details['kuantitas'],
                        'total_berat' => $details['berat_produk'] * $details['kuantitas'],
                        'akumulasi_pembelian' => $ed + 1,
                        'status_pengiriman' => 'belum',
                        'no_resi' => str_pad($increment,7,0, STR_PAD_LEFT),
                        'rating' => 'belum',
                        'kurir' => $request->kurir,
                    ]);
                    $pr = Produk::where('id', $details['id_produk'])->first();
                    $pr->stok_produk = $pr->stok_produk - $details['kuantitas'];
                    $pr->trending += 1;
                    $pr->save();
                }
                foreach ($keranjang as $kr) {
                    Pesanan::create([
                        'pemilik' => $kr->pemilik,
                        'id_produk' => $kr->id_produk,
                        'pemilik_produk' => $kr->pemilik_produk,
                        'nama_produk' => $kr->nama_produk,
                        'gambar_produk' => $kr->gambar_produk,
                        'deskripsi_produk' => $kr->deskripsi_produk,
                        'harga_produk' => $kr->harga_produk,
                        'berat_produk' => $kr->berat_produk,
                        'kuantitas' => $kr->kuantitas,
                        'total_harga' => $kr->total_harga,
                        'total_berat' => $kr->total_berat,
                        'akumulasi_pembelian' => $ed + 1,
                        'status_pengiriman' => 'belum',
                        'no_resi' => str_pad($increment,7,0, STR_PAD_LEFT),
                        'rating' => 'belum',
                        'kurir' => $request->kurir,
                    ]);
                    $pr = Produk::where('id', $kr->id_produk)->first();
                    $pr->stok_produk = $pr->stok_produk - $kr->kuantitas;
                    $pr->trending += 1;
                    $pr->save();
                }
                session()->forget(['cart', 'ongkir']);
            } else {
                foreach ($keranjang as $kr) {
                    Pesanan::create([
                        'pemilik' => $kr->pemilik,
                        'id_produk' => $kr->id_produk,
                        'pemilik_produk' => $kr->pemilik_produk,
                        'nama_produk' => $kr->nama_produk,
                        'gambar_produk' => $kr->gambar_produk,
                        'deskripsi_produk' => $kr->deskripsi_produk,
                        'harga_produk' => $kr->harga_produk,
                        'berat_produk' => $kr->berat_produk,
                        'kuantitas' => $kr->kuantitas,
                        'total_harga' => $kr->total_harga,
                        'total_berat' => $kr->total_berat,
                        'akumulasi_pembelian' => $ed + 1,
                        'status_pengiriman' => 'belum',
                        'no_resi' => str_pad($increment,7,0, STR_PAD_LEFT),
                        'rating' => 'belum',
                        'kurir' => $request->kurir,
                    ]);
                    $pr = Produk::where('id', $kr->id_produk)->first();
                    $pr->stok_produk = $pr->stok_produk - $kr->kuantitas;
                    $pr->trending += 1;
                    $pr->save();
                }
            }
            

            Keranjang::where('pemilik', auth()->user()->name)->delete();
    
            return redirect('/user/transaction');
        } else {
            if (auth()->user()->qm_wallet < $request->total) {
                return redirect()->back()->with('message', 'Saldo tidak cukup');
            } else {
                Transaksi::create([
                    'pemilik' => auth()->user()->name,
                    'kurir' => $request->kurir,
                    'jenis_layanan' => $request->jenis_layanan,
                    'metode_pembayaran' => $request->via,
                    'ongkir' => $request->ongkir,
                    'total_harga' => $request->total,
                    'status_pembayaran' => 'belum',
                    'no_resi' => str_pad($increment,7,0, STR_PAD_LEFT),
                ]);
                $keranjang = Keranjang::where('pemilik', auth()->user()->name)->get();
                $ed = Pesanan::where('pemilik', auth()->user()->name)->max('akumulasi_pembelian');
    
                if (session('cart')) {
                    foreach (session('cart') as $id => $details) {
                        Pesanan::create([
                            'pemilik' => auth()->user()->name,
                            'id_produk' => $details['id_produk'],
                            'pemilik_produk' => $details['pemilik_produk'],
                            'nama_produk' => $details['nama_produk'],
                            'gambar_produk' => $details['gambar_produk'],
                            'deskripsi_produk' => $details['deskripsi_produk'],
                            'harga_produk' => $details['harga_produk'],
                            'berat_produk' => $details['berat_produk'],
                            'kuantitas' => $details['kuantitas'],
                            'total_harga' => $details['harga_produk'] * $details['kuantitas'],
                            'total_berat' => $details['berat_produk'] * $details['kuantitas'],
                            'akumulasi_pembelian' => $ed + 1,
                            'status_pengiriman' => 'belum',
                            'no_resi' => str_pad($increment,7,0, STR_PAD_LEFT),
                            'rating' => 'belum',
                            'kurir' => $request->kurir,
                        ]);
                        $pr = Produk::where('id', $details['id_produk'])->first();
                        $pr->stok_produk = $pr->stok_produk - $details['kuantitas'];
                        $pr->trending += 1;
                        $pr->save();
                    }
                    foreach ($keranjang as $kr) {
                        Pesanan::create([
                            'pemilik' => $kr->pemilik,
                            'id_produk' => $kr->id_produk,
                            'pemilik_produk' => $kr->pemilik_produk,
                            'nama_produk' => $kr->nama_produk,
                            'gambar_produk' => $kr->gambar_produk,
                            'deskripsi_produk' => $kr->deskripsi_produk,
                            'harga_produk' => $kr->harga_produk,
                            'berat_produk' => $kr->berat_produk,
                            'kuantitas' => $kr->kuantitas,
                            'total_harga' => $kr->total_harga,
                            'total_berat' => $kr->total_berat,
                            'akumulasi_pembelian' => $ed + 1,
                            'status_pengiriman' => 'belum',
                            'no_resi' => str_pad($increment,7,0, STR_PAD_LEFT),
                            'rating' => 'belum',
                            'kurir' => $request->kurir,
                        ]);
                        $pr = Produk::where('id', $kr->id_produk)->first();
                        $pr->stok_produk = $pr->stok_produk - $kr->kuantitas;
                        $pr->trending += 1;
                        $pr->save();
                    }
                    session()->forget(['cart', 'ongkir']);
                } else {
                    foreach ($keranjang as $kr) {
                        Pesanan::create([
                            'pemilik' => $kr->pemilik,
                            'id_produk' => $kr->id_produk,
                            'pemilik_produk' => $kr->pemilik_produk,
                            'nama_produk' => $kr->nama_produk,
                            'gambar_produk' => $kr->gambar_produk,
                            'deskripsi_produk' => $kr->deskripsi_produk,
                            'harga_produk' => $kr->harga_produk,
                            'berat_produk' => $kr->berat_produk,
                            'kuantitas' => $kr->kuantitas,
                            'total_harga' => $kr->total_harga,
                            'total_berat' => $kr->total_berat,
                            'akumulasi_pembelian' => $ed + 1,
                            'status_pengiriman' => 'belum',
                            'no_resi' => str_pad($increment,7,0, STR_PAD_LEFT),
                            'rating' => 'belum',
                            'kurir' => $request->kurir,
                        ]);
                        $pr = Produk::where('id', $kr->id_produk)->first();
                        $pr->stok_produk = $pr->stok_produk - $kr->kuantitas;
                        $pr->trending += 1;
                        $pr->save();
                    }
                }
                
                Keranjang::where('pemilik', auth()->user()->name)->delete();
                $user = User::find(Auth::id());
                $user->qm_wallet = $user->qm_wallet - $request->total;
                $user->save();
                
                return redirect('/user/transaction');
            }
        }
    }

    public function indexAdmin(){
        $data['unread'] = Inbox::where('read_inbox', 'unread')->where('kepada', '=', auth()->user()->name)->count();
        $data['unread'] = Inbox::where('read_inbox', 'unread')->where('kepada', '=', auth()->user()->name)->where('delete_status_in', 'no')->count();
        $data['unvalidate'] = Berita::where('validasi', 'belum')->count();
        $data['notifikasi'] = Notifikasi::where('pemilik', auth()->user()->name)->count();
        $data['total_pesanan'] = Pesanan::where('pemilik_produk', auth()->user()->name)->count();
        return view('admin/transaksi/transaksi', $data);
    }
}
