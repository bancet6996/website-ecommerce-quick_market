<?php

namespace App\Http\Controllers;

use App\Banner;
use App\Berita;
use App\Keranjang;
use App\Produk;
use App\Province;
use Illuminate\Http\Request;
Use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class UserzController extends Controller
{

    public function index(){
        $data['banner'] = Banner::get();
        $data['list_berita'] = Berita::where('validasi', '=', 'sudah')->orderBy('created_at', 'desc')->get();
        $data['list_produk'] = Produk::where('pemilik', 'Quick Market')->where('stok_produk', '>', 0)->get();
        $data['trending'] = Produk::where('stok_produk', '>', 0)->orderBy('trending', 'desc')->get();
        $data['rating'] = Produk::where('stok_produk', '>', 0)->orderBy('rating', 'desc')->get();
        if (Auth::check()) {
            $data['count_produk'] = Keranjang::where('pemilik', auth()->user()->name)->count();
        }
        return view('user/user', $data);
    }

    public function readmore(Request $request, $id){
        $data['berita'] = Berita::where('validasi', '=', 'sudah')->find($id);
        $data['kategori'] = \DB::table('kategori_berita')->get();
        $data['users'] = \DB::table('users')->get();
        return view('user/readmore/readmore', $data);
    }

    public function setting(){
        $data['count_produk'] = Keranjang::where('pemilik', auth()->user()->name)->count();
        return view('user/pages/setting', $data);
    }

    public function update(Request $request)
    {
        if (auth()->user()->kota === null) {
            return redirect('/user/address/'. auth()->user()->name)->with('message', 'Mohon isi alamat terlebih dahulu');
        } else {
            $this->validate($request, [
                'name' => 'required',
                'image' => 'nullable|image|mimes:jpg,png,jpeg,svg|max:2048',
            ]);
    
            $user = User::find(Auth::id());
    
            $user->name = $request->name;
            if ($user->image != null) {
                $user->image = $user->image;
            } else {
                $user->image = $request->image;
            }
            
            if ($request->password != null) {
                $user->password = Hash::make($request->password);
                $user->password_helper = $request->password;
            } else {
                $user->password = $user->password;
                $user->password_helper = $user->password_helper;
            }
    
            if(auth()->user()->provider === 'default'){
                $user->provider = $request->provider;
            }
            else{
                $user->provider = auth()->user()->provider;
            }
            
            $user->save();
    
            if($request->file() != null){
                $user = $request->file('image');
                $destinationPath = 'uploads/tmp';
                $user->move($destinationPath, $user);
            }
            else{
                $user->image = '';
            }
            
            return redirect('/user')->with('message', 'Akun telah diperbarui');
        }
    }

    public function address(){
        $data['count_produk'] = Keranjang::where('pemilik', auth()->user()->name)->count();
        $data['provinces'] = Province::pluck('title', 'province_id');
        return view('user/pages/address', $data);
    }

    public function addressUpdate(Request $request){
        $this->validate($request, [
            'kota' => 'required',
            'kode_pos' => 'required',
            'alamat_lengkap' => 'required',
            'telepon' => 'required',
        ]);

        $user = User::find(Auth::id());
        $user->kota = $request->kota;
        $user->kode_pos = $request->kode_pos;
        $user->alamat_lengkap = $request->alamat_lengkap;
        $user->telepon = $request->telepon;
        $user->save();

        Produk::where('pemilik', auth()->user()->name)->update([
            'kota' => $request->kota,
        ]);

        return redirect('/user')->with('message', 'Alamat berhasil diubah');
    }
}
