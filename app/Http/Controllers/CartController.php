<?php

namespace App\Http\Controllers;

use App\Keranjang;
use App\Produk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class CartController extends Controller
{
    public function index(){
        if (Auth::check()) {
            $data['keranjang'] = Keranjang::where('pemilik', auth()->user()->name)->get();
            $data['cek_keranjang'] = Keranjang::where('pemilik', auth()->user()->name)->count();
            $data['count_produk'] = Keranjang::where('pemilik', auth()->user()->name)->count();
            $data['produk'] = Produk::get();
            return view('user/shop/cart/cart', $data);
        } else {
            $data['produk'] = Produk::get();
            return view('user/shop/cart/cart', $data);
        }
    }

    public function add(Request $request, $id){
        $produk = Produk::find($id);
        if ($produk->stok_produk <= 0) {
            return redirect()->back()->with('message', 'Stok produk ini tidak tersedia');
        }
        else{
            if ($produk->pemilik != auth()->user()->name) {
                $keranjang = Keranjang::where('pemilik', auth()->user()->name)->where('id_produk', $produk->id)->first();
                if ($keranjang) {
                    Keranjang::where('pemilik', auth()->user()->name)->where('id_produk', $produk->id)->update([
                        'kuantitas' => $keranjang->kuantitas + $request->qty,
                    ]);
                    return redirect()->back()->with('message', 'Produk ini sudah ada di keranjang dan sudah ditambahkan kuantitasnya');
                } else {
                    if ($request->qty != null) {
                        Keranjang::create([
                            'pemilik' => auth()->user()->name,
                            'id_produk' => $produk->id,
                            'pemilik_produk' => $produk->pemilik,
                            'nama_produk' => $produk->nama_produk,
                            'gambar_produk' => $produk->gambar_produk,
                            'deskripsi_produk' => $produk->deskripsi_produk,
                            'harga_produk' => $produk->harga_produk,
                            'berat_produk' => $produk->berat_produk,
                            'stok_produk' => $produk->stok_produk,
                            'kuantitas' => abs($request->qty),
                            'total_harga' => $produk->harga_produk * $request->qty,
                            'total_berat' => $produk->berat_produk * $request->qty,
                            'kota' => $produk->kota,
                        ]);
                        return redirect()->back()->with('message', 'Produk berhasil dimasukkan ke kerajang');
                    } else {
                        Keranjang::create([
                            'pemilik' => auth()->user()->name,
                            'id_produk' => $produk->id,
                            'pemilik_produk' => $produk->pemilik,
                            'nama_produk' => $produk->nama_produk,
                            'gambar_produk' => $produk->gambar_produk,
                            'deskripsi_produk' => $produk->deskripsi_produk,
                            'harga_produk' => $produk->harga_produk,
                            'berat_produk' => $produk->berat_produk,
                            'stok_produk' => $produk->stok_produk,
                            'kuantitas' => 1,
                            'total_harga' => $produk->harga_produk,
                            'total_berat' => $produk->berat_produk * $request->qty,
                            'kota' => $produk->kota,
                        ]);
                        return redirect()->back()->with('message', 'Produk berhasil dimasukkan ke kerajang');
                    }
                }
            } else {
                return redirect()->back()->with('message', 'Masa iya mau belanja barang sendiri ?');
            }
        }
    }

    public function update(Request $request, $id){
            // Keranjang::where('id', $id)->where('pemilik', auth()->user()->name)->update([
            // 'kuantitas' => abs($request->quantity),
            // ]);
        $keranjang = Keranjang::where('pemilik', auth()->user()->name)->find($id);
        $keranjang->kuantitas = abs($request->quantity);
        $keranjang->total_harga = $keranjang->harga_produk * $request->quantity;
        $keranjang->total_berat = $keranjang->berat_produk * $request->quantity;
        $keranjang->save();

        return redirect()->back()->with('message', 'Produk telah diperbarui');
    }

    public function remove($id){
        Keranjang::where('id', $id)->delete();
        return redirect()->back()->with('message', 'Produk telah dihapus dari keranjang');
    }

    public static function cartExist($string){
        if(Auth::check()){
            return Keranjang::where('id_produk', $string)->where('pemilik', auth()->user()->name)->first();
        }
    }

    public function addSession($id)
    {
        $product = Produk::find($id);

        if ($product->stok_produk <= 0) {
            return redirect()->back()->with('message', 'Stok produk ini tidak tersedia');
        } else {
            if(!$product) {
                abort(404);
            }
    
            $cart = session()->get('cart');
    
            // if cart is empty then this the first product
            if(!$cart) {
    
                $cart = [
                    $id => [
                        "id_produk" => $product->id,
                        "pemilik_produk" => $product->pemilik,
                        "nama_produk" => $product->nama_produk,
                        "deskripsi_produk" => $product->deskripsi_produk,
                        "kuantitas" => 1,
                        "harga_produk" => $product->harga_produk,
                        "gambar_produk" => $product->gambar_produk,
                        "berat_produk" => $product->berat_produk,
                        "stok_produk" => $product->stok_produk,
                        "kota" => $product->kota,
                    ]
                ];
    
                session()->put('cart', $cart);
    
                return redirect()->back()->with('message', 'Produk berhasil ditambahkan ke keranjang');
            }
    
            // if cart not empty then check if this product exist then increment quantity
            if(isset($cart[$id])) {
    
                $cart[$id]['kuantitas']++;
    
                session()->put('cart', $cart);
    
                return redirect()->back()->with('message', 'Produk berhasil ditambahkan ke keranjang');
    
            }
    
            // if item not exist in cart then add to cart with quantity = 1
            $cart[$id] = [
                "id_produk" => $product->id,
                "pemilik_produk" => $product->pemilik,
                "deskripsi_produk" => $product->deskripsi_produk,
                "nama_produk" => $product->nama_produk,
                "kuantitas" => 1,
                "harga_produk" => $product->harga_produk,
                "gambar_produk" => $product->gambar_produk,
                "berat_produk" => $product->berat_produk,
                "stok_produk" => $product->stok_produk,
                "kota" => $product->kota,
            ];
    
            session()->put('cart', $cart);
    
            return redirect()->back()->with('message', 'Produk berhasil ditambahkan ke keranjang');
        }
    }

    public function updateSession(Request $request)
    {
        if($request->id and $request->quantity)
        {
            $cart = session()->get('cart');

            $cart[$request->id]["kuantitas"] = $request->quantity;

            session()->put('cart', $cart);

            session()->flash('message', 'Keranjang telah diperbarui');
        }
    }

    public function removeSession(Request $request)
    {
        if($request->id) {

            $cart = session()->get('cart');

            if(isset($cart[$request->id])) {

                unset($cart[$request->id]);

                session()->put('cart', $cart);
            }

            session()->flash('message', 'Produk berhasil dihapus dari keranjang');
        }
    }
}
