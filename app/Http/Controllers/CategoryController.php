<?php

namespace App\Http\Controllers;

use App\Keranjang;
use App\Produk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    public function index(){
        $data['produk_kategori'] = Produk::paginate(9);
        $data['kategori'] = \DB::table('kategori_produk')->orderBy('nama_kategori', 'asc')->get();
        $data['top'] = Produk::orderBy('favorit', 'desc')->get();
        if (Auth::check()) {
            $data['count_produk'] = Keranjang::where('pemilik', auth()->user()->name)->count();
        }
        return view('user/shop/category/category', $data);
    }

    public function fltKategori(Request $request){
        $kategori = strtolower($request->nama_kategori);
        $data['produk_kategori'] = Produk::where('kategori_produk', '=', $kategori)->get();
        $data['top'] = Produk::orderBy('favorit', 'desc')->get();
        $data['kategori'] = \DB::table('kategori_produk')->orderBy('nama_kategori', 'asc')->get();
        if (Auth::check()) {
            $data['count_produk'] = Keranjang::where('pemilik', auth()->user()->name)->count();
        }
        return view('user/shop/category/category', $data);
    }

    public function srcProduk(Request $request){
        $keyword = $request->get('cari');
        $data['produk_kategori'] = Produk::where('nama_produk', 'LIKE', '%'.$keyword.'%')->get();
        $data['top'] = Produk::orderBy('favorit', 'desc')->get();
        $data['kategori'] = \DB::table('kategori_produk')->orderBy('nama_kategori', 'asc')->get();
        if (Auth::check()) {
            $data['count_produk'] = Keranjang::where('pemilik', auth()->user()->name)->count();
        }
        return view('user/shop/category/category', $data);

        // return 'Rp. ' .strrev(implode('.', str_split(strrev(strval($int))),3));
    }
}
