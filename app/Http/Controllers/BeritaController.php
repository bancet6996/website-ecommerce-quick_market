<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Berita;
use App\Inbox;
use App\Notifikasi;
use App\Pesanan;

class BeritaController extends Controller
{
    public function index()
    {
        $data['berita'] = Berita::where('penulis', '=', auth()->user()->name)->where('delete_status', '=', 'no')->orderBy('id', 'desc')->get();
        $data['unread'] = Inbox::where('read_inbox', 'unread')->where('kepada', '=', auth()->user()->name)->count();
        $data['unvalidate'] = Berita::where('validasi', 'belum')->count();
        $data['notifikasi'] = Notifikasi::where('pemilik', auth()->user()->name)->count();
        $data['total_pesanan'] = Pesanan::where('pemilik_produk', auth()->user()->name)->count();
        return view('admin/berita/berita', $data);
    }

    public function create(){
        $data['kategori'] = \DB::table('kategori_berita')->orderBy('nama_kategori', 'asc')->get();
        $data['unread'] = Inbox::where('read_inbox', 'unread')->where('kepada', '=', auth()->user()->name)->where('delete_status_in', 'no')->count();
        $data['unvalidate'] = Berita::where('validasi', 'belum')->count();
        $data['notifikasi'] = Notifikasi::where('pemilik', auth()->user()->name)->count();
        $data['total_pesanan'] = Pesanan::where('pemilik_produk', auth()->user()->name)->count();
        return view('admin/berita/form', $data);
    }
    
    public function store(Request $request){
        $rule = [
            'judul_berita' => 'required|string',
            'kategori_berita' => 'required|string',
            'deskripsi_berita' => 'required',
            'thumbnail_berita' => 'required|image|mimes:jpg,png,jpeg,svg'
        ];
        $this->validate($request, $rule);
        
        Berita::create([
            'penulis' => auth()->user()->name,
            'judul_berita' => $request->judul_berita,
            'caption_berita' => $request->caption_berita,
            'kategori_berita' => $request->kategori_berita,
            'thumbnail_berita' => $request->thumbnail_berita,
            'deskripsi_berita' => $request->deskripsi_berita,
            'validasi' => 'belum',
            'delete_status' => 'no',
            'kunjungan' => '0',
        ]);

        $file = $request->file('thumbnail_berita');
        $destinationPath = 'uploads/tmp';
        $file->move($destinationPath, $file);

        return redirect('/admin/berita')->with('message', 'Berhasil menambahkan berita');
    }

    public function edit(Request $request, $id){
        $data['berita'] = Berita::where('delete_status', '=', 'no')->find($id);
        $data['kategori'] = \DB::table('kategori_berita')->orderBy('nama_kategori', 'asc')->get();
        $data['unread'] = Inbox::where('read_inbox', 'unread')->where('kepada', '=', auth()->user()->name)->where('delete_status_in', 'no')->count();
        $data['unvalidate'] = Berita::where('validasi', 'belum')->count();
        $data['notifikasi'] = Notifikasi::where('pemilik', auth()->user()->name)->count();
        $data['total_pesanan'] = Pesanan::where('pemilik_produk', auth()->user()->name)->count();

        if (Berita::where('id', $id)->where('penulis', auth()->user()->name)->first()) {
            return view('admin/berita/form', $data);
        } else {
            return redirect()->back();
        }
    }

    public function update(Request $request, $id){
        $rule = [
            'judul_berita' => 'required|string',
            'caption_berita' => 'required|string',
            'kategori_berita' => 'required|string',
            'deskripsi_berita' => 'required',
            'thumbnail_berita' => 'required|image|mimes:jpg,png,jpeg,svg'
        ];
        $this->validate($request, $rule);

        // $input = $request->all();
        // unset($input['_token']);
        // unset($input['_method']);

        $berita = Berita::find($id);
        $berita->penulis = auth()->user()->name;
        $berita->judul_berita = $request->judul_berita;
        $berita->caption_berita = $request->caption_berita;
        $berita->kategori_berita = $request->kategori_berita;
        $berita->thumbnail_berita = $request->thumbnail_berita;
        $berita->deskripsi_berita = $request->deskripsi_berita;
        $berita->validasi = 'belum';
        $berita->delete_status = 'no';
        $berita->save();

        $file = $request->file('thumbnail_berita');
        $destinationPath = 'uploads/tmp';
        $file->move($destinationPath, $file);

        return redirect('/admin/berita')->with('message', 'Berhasil mengubah berita');

        // $status = Berita::where('id', $id)->update($input);

        // if($status){
        //     return redirect('/admin/berita');
        // }
        // else{
        //     return redirect('/admin/berita/create');
        // }
    }

    public function destroy(Request $request, $id){
        $status = Berita::where('id', $id)->where('penulis', auth()->user()->name)->where('delete_status', '=', 'no')->update([
            'delete_status' => 'yes',
        ]);
        if($status){
            return redirect('/admin/berita')->with('message', 'Berita telah dipindahkan ke tempat sampah');
        }
        else{
            return redirect()->back();
        }
    }

    public function baca(Request $request, $id){
        $data['berita'] = Berita::find($id);
        $data['unread'] = Inbox::where('read_inbox', 'unread')->where('kepada', '=', auth()->user()->name)->where('delete_status_in', 'no')->count();
        $data['unvalidate'] = Berita::where('validasi', 'belum')->count();
        $data['notifikasi'] = Notifikasi::where('pemilik', auth()->user()->name)->count();
        $data['total_pesanan'] = Pesanan::where('pemilik_produk', auth()->user()->name)->count();
        return view('admin/berita/baca', $data);
    }

    public function purge($id){
        $status = Berita::where('id', $id)->where('penulis', auth()->user()->name)->where('delete_status', 'yes')->delete();
        if($status){
            return redirect()->back()->with('message', 'Berhasil menghapus berita');
        }
        else{
            return redirect()->back();
        }
    }

}
