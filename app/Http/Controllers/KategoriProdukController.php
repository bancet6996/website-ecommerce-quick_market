<?php

namespace App\Http\Controllers;

use App\Berita;
use App\Inbox;
use App\Notifikasi;
use App\Pesanan;
use Illuminate\Http\Request;

class KategoriProdukController extends Controller
{
    public function index()
    {
        $data['kategori_produk'] = \DB::table('kategori_produk')->orderBy('nama_kategori', 'asc')->get();
        $data['unread'] = Inbox::where('read_inbox', 'unread')->where('kepada', '=', auth()->user()->name)->where('delete_status_in', 'no')->count();
        $data['unvalidate'] = Berita::where('validasi', 'belum')->count();
        $data['notifikasi'] = Notifikasi::where('pemilik', auth()->user()->name)->count();
        $data['total_pesanan'] = Pesanan::where('pemilik_produk', auth()->user()->name)->count();
        return view('admin/kategori-produk/kategori', $data);
    }

    public function create(){
        $data['unread'] = Inbox::where('read_inbox', 'unread')->where('kepada', '=', auth()->user()->name)->where('delete_status_in', 'no')->count();
        $data['unvalidate'] = Berita::where('validasi', 'belum')->count();
        $data['notifikasi'] = Notifikasi::where('pemilik', auth()->user()->name)->count();
        $data['total_pesanan'] = Pesanan::where('pemilik_produk', auth()->user()->name)->count();
        return view('admin/kategori-produk/form', $data);
    }
    
    public function store(Request $request){
        $rule = [
            'nama_kategori' => 'required|string|unique:kategori_produk',
        ];
        $this->validate($request, $rule);

        $input = $request->all();
        unset($input['_token']);
        $status = \DB::table('kategori_produk')->insert($input);

        if($status){
            return redirect('/admin/kategori-produk')->with('message', 'Berhasil menambahkan kategori');
        }
        else{
            return redirect('/admin/kategori-produk/create');
        }
    }

    public function edit(Request $request, $id){
        $data['kategori_produk'] = \DB::table('kategori_produk')->find($id);
        $data['unread'] = Inbox::where('read_inbox', 'unread')->where('kepada', '=', auth()->user()->name)->where('delete_status_in', 'no')->count();
        $data['unvalidate'] = Berita::where('validasi', 'belum')->count();
        $data['notifikasi'] = Notifikasi::where('pemilik', auth()->user()->name)->count();
        $data['total_pesanan'] = Pesanan::where('pemilik_produk', auth()->user()->name)->count();
        return view('admin/kategori-produk/form', $data);
    }

    public function update(Request $request, $id){
        $rule = [
            'nama_kategori' => 'required|string|unique:kategori_produk',
        ];
        $this->validate($request, $rule);

        $input = $request->all();
        unset($input['_token']);
        unset($input['_method']);

        $status = \DB::table('kategori_produk')->where('id', $id)->update($input);

        if($status){
            return redirect('/admin/kategori-produk')->with('message', 'Berhasil mengubah kategori');
        }
        else{
            return redirect('/admin/kategori-produk/create');
        }
    }

    public function destroy(Request $request, $id){
        $status = \DB::table('kategori_produk')->where('id', $id)->delete();
        if($status){
            return redirect('/admin/kategori-produk')->with('message', 'Berhasil menghapus kategori');
        }
        else{
            return redirect('/admin/kategori-produk/create');
        }
    }
}
