<?php

namespace App\Http\Controllers;

use App\Berita;
use App\Inbox;
use App\Kunjungan;
use App\Notifikasi;
use App\Pesanan;
use App\Produk;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index(){
        $data['users'] = \DB::table('users')->where('provider', '!=', 'superadmin')->count();
        $data['produk'] = Produk::where('pemilik', auth()->user()->name)->count();
        $data['unread'] = Inbox::where('read_inbox', 'unread')->where('kepada', '=', auth()->user()->name)->count();
        $data['unvalidate'] = Berita::where('validasi', 'belum')->count();
        $data['notifikasi'] = Notifikasi::where('pemilik', auth()->user()->name)->count();
        $data['total_pesanan'] = Pesanan::where('pemilik_produk', auth()->user()->name)->count();
        $data['sb_produk'] = Produk::get();
        return view('admin/admin', $data);
    }

    public function about(){
        $data['unread'] = Inbox::where('read_inbox', 'unread')->where('kepada', '=', auth()->user()->name)->where('delete_status_in', 'no')->count();
        $data['unvalidate'] = Berita::where('validasi', 'belum')->count();
        $data['notifikasi'] = Notifikasi::where('pemilik', auth()->user()->name)->count();
        $data['total_pesanan'] = Pesanan::where('pemilik_produk', auth()->user()->name)->count();
        return view('admin/about/about', $data);
    }
}


//
