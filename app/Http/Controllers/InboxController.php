<?php

namespace App\Http\Controllers;

use App\Berita;
use App\Inbox;
use App\Notifikasi;
use App\Pesanan;
use Illuminate\Http\Request;

class InboxController extends Controller
{
    public function index(){       
        $data['collection'] = Inbox::where('kepada', '=', auth()->user()->name)->where('delete_status_in', 'no')->orderBy('id', 'desc')->get();
        $data['unread'] = Inbox::where('read_inbox', 'unread')->where('kepada', '=', auth()->user()->name)->where('delete_status_in', 'no')->count();
        $data['unvalidate'] = Berita::where('validasi', 'belum')->count();
        $data['notifikasi'] = Notifikasi::where('pemilik', auth()->user()->name)->count();
        $data['total_pesanan'] = Pesanan::where('pemilik_produk', auth()->user()->name)->count();
        return view('admin/inbox/inbox', $data);
    }

    public function read(Request $request, $id){
        $data['inbox'] = Inbox::find($id);
        $data['unread'] = Inbox::where('read_inbox', 'unread')->where('kepada', '=', auth()->user()->name)->where('delete_status_in', 'no')->count();
        $data['unvalidate'] = Berita::where('validasi', 'belum')->count();
        $data['notifikasi'] = Notifikasi::where('pemilik', auth()->user()->name)->count();
        $data['total_pesanan'] = Pesanan::where('pemilik_produk', auth()->user()->name)->count();

        if (Inbox::where('id', $id)->where('kepada', auth()->user()->name)->where('delete_status_in', 'no')->first()) {
            $inbox = Inbox::find($id);
            $inbox->read_inbox = 'readed';
            $inbox->save();
            return view('admin/inbox/read', $data);
        } else {
            return redirect()->back();
        }
        
    }

    public function destroy(Request $request, $id){
        $status = Inbox::where('id', $id)->where('kepada', auth()->user()->name)->where('delete_status_in', 'no')->update([
            'delete_status_in' => 'yes',
        ]);
        if($status){
            return redirect('/admin/inbox')->with('message', 'Berhasil menghapus pesan');
        }
        else{
            return redirect()->back();
        }
    }
}
