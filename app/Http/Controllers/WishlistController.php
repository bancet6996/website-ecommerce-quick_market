<?php

namespace App\Http\Controllers;

use App\Keranjang;
use App\Produk;
use App\Wishlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WishlistController extends Controller
{

    public function index(){
        $data['wishlist'] = Wishlist::where('pemilik', auth()->user()->name)->get();
        if (Auth::check()) {
            $data['count_produk'] = Keranjang::where('pemilik', auth()->user()->name)->count();
        }
        return view('user/wishlist/wishlist', $data);
    }

    public function add($id){
        $produk = Produk::find($id);
        $wishlist = Wishlist::where('pemilik', auth()->user()->name)->where('id_produk', $produk->id)->first();
        if (Produk::where('pemilik', auth()->user()->name)->get() === auth()->user()->name) {
            return redirect()->back()->with('message', 'Jangan curang euy');
        } else {
            if ($wishlist) {
                return redirect()->back()->with('message', 'Produk ini sudah ada di daftar harapan');
            } else{
                Wishlist::create([
                    'pemilik' => auth()->user()->name,
                    'id_produk' => $produk->id,
                    'nama_produk' => $produk->nama_produk,
                    'gambar_produk' => $produk->gambar_produk,
                    'deskripsi_produk' => $produk->deskripsi_produk,
                    'harga_produk' => $produk->harga_produk,
                ]);

                $produk->favorit += 1;
                $produk->save();
            }
        }
        
        return redirect()->back()->with('message', 'Produk telah ditambahkan ke daftar harapan');
    }

    public function remove($id){
        $produk = Produk::find($id);
        $produk->favorit -= 1;
        $produk->save();

        Wishlist::where('id', $id)->delete();
        return redirect()->back()->with('message', 'Berhasil menghapus produk dari daftar harapan');
    }

    public static function wishExist($string){
        if(Auth::check()){
            return Wishlist::where('id_produk', $string)->where('pemilik', auth()->user()->name)->first();
        }
    }
}
