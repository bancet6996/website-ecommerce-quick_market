<?php

namespace App\Http\Controllers;

use App\Berita;
use App\Inbox;
use App\Notifikasi;
use App\Pesanan;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PesananController extends Controller
{
    public function index(){
        $data['pesanan'] = Pesanan::where('pemilik_produk', auth()->user()->name)->get();
        $data['total_pesanan'] = Pesanan::where('pemilik_produk', auth()->user()->name)->count();
        $data['unread'] = Inbox::where('read_inbox', 'unread')->where('kepada', '=', auth()->user()->name)->count();
        $data['unvalidate'] = Berita::where('validasi', 'belum')->count();
        $data['notifikasi'] = Notifikasi::where('pemilik', auth()->user()->name)->count();
        return view('admin/pesanan/pesanan', $data);
    }

    public static function rating($string, $string2){
        return Pesanan::where('pemilik', $string)->where('id_produk', $string2)->where('rating', 'belum')->first();
    }
}
