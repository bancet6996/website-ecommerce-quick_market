<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Inbox;
use App\Keranjang;
use Illuminate\Support\Facades\Auth;

class ContactController extends Controller
{
    public function index(){
        $data['users'] = \DB::table('users')->get();
        // if (Auth::check()) {
            // $data['admin'] = \DB::table('users')->where('provider', '=', 'admin')->where('provider', '=', 'superadmin')->where('name', '!=', auth()->user()->name)->orderBy('name', 'asc')->get();
        // }
        // else{
            $data['admin'] = \DB::table('users')->where('provider', '=', 'admin')->orderBy('name', 'asc')->get();
        // }
        if (Auth::check()) {
            $data['count_produk'] = Keranjang::where('pemilik', auth()->user()->name)->count();
        }
        return view('user/contact/contact', $data);
    }

    public function post(Request $request){
        if (Auth::check()) {
            $rule = [
                'nama' => 'required|string',
                'email' => 'required|email',
                'subjek' => 'required|string',
                'pesan' => 'required|string',
                'kepada' => 'required|string',
            ];
            $this->validate($request, $rule);

            Inbox::create([
                'name' => $request->nama,
                'email' => auth()->user()->email,
                'kepada' => $request->kepada,
                'subject' => $request->subjek,
                'message' => $request->pesan,
                'read_inbox' => 'unread',
                'delete_status_in' => 'no',
                'delete_status_out' => 'no',
            ]);
        } else {
            $rule = [
                'nama' => 'required|string',
                'email' => 'required|email',
                'subjek' => 'required|string',
                'pesan' => 'required|string',
            ];
            $this->validate($request, $rule);

            Inbox::create([
                'name' => $request->nama,
                'email' => $request->email,
                'kepada' => 'Quick Market',
                'subject' => $request->subjek,
                'message' => $request->pesan,
                'read_inbox' => 'unread',
                'delete_status_in' => 'no',
                'delete_status_out' => 'no',
            ]);
        }

        return redirect()->back()->with('message', 'Pesan telah dikirm');
    }
}
