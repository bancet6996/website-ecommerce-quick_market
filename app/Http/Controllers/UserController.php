<?php

namespace App\Http\Controllers;

use App\Berita;
use App\Inbox;
use App\Notifikasi;
use App\Pesanan;
use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function index(){
        $data['users'] = User::where('provider', '!=', 'superadmin')->where('status', '!=', 'banned')->get();
        $data['unread'] = Inbox::where('read_inbox', 'unread')->where('kepada', '=', auth()->user()->name)->where('delete_status_in', 'no')->count();
        $data['unvalidate'] = Berita::where('validasi', 'belum')->count();
        $data['notifikasi'] = Notifikasi::where('pemilik', auth()->user()->name)->count();
        $data['total_pesanan'] = Pesanan::where('pemilik_produk', auth()->user()->name)->count();
        return view('admin/adminuser/adminuser', $data);
    }

    public function banned(Request $request, $id){
        $user = User::find($id);
        $user->status = 'banned';
        $user->save();
        return redirect('/admin/adminuser')->with('message', 'Berhasil membanned user');
    }

    public function unbanned(Request $request, $id){
        $user = User::find($id);
        $user->status = 'aman';
        $user->save();
        return redirect('/admin/banned-list')->with('message', 'Berhasil membatalkan banned user');
    }

    public function list(){
        $data['users'] = User::where('provider', '!=', 'superadmin')->where('status', '!=', 'aman')->get();
        $data['unread'] = Inbox::where('read_inbox', 'unread')->where('kepada', '=', auth()->user()->name)->where('delete_status_in', 'no')->count();
        $data['unvalidate'] = Berita::where('validasi', 'belum')->count();
        $data['notifikasi'] = Notifikasi::where('pemilik', auth()->user()->name)->count();
        $data['total_pesanan'] = Pesanan::where('pemilik_produk', auth()->user()->name)->count();
        return view('admin/useradmin/useradmin', $data);
    }

    public function banneder(){
        $data['unread'] = \DB::table('inbox')->where('read_inbox', 'unread')->where('kepada', '=', auth()->user()->name)->count();
        return view('banned', $data);
    }
}
