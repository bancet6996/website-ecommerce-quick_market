<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Berita;
use App\Inbox;
use App\Notifikasi;
use App\Pesanan;

class ValidasiController extends Controller
{
    public function listBerita(){
        $data['list_berita'] = \DB::table('berita')->where('validasi', '=', 'belum')->orderBy('created_at', 'desc')->get();
        $data['unread'] = Inbox::where('read_inbox', 'unread')->where('kepada', '=', auth()->user()->name)->where('delete_status_in', 'no')->count();
        $data['unvalidate'] = Berita::where('validasi', 'belum')->count();
        $data['notifikasi'] = Notifikasi::where('pemilik', auth()->user()->name)->count();
        $data['total_pesanan'] = Pesanan::where('pemilik_produk', auth()->user()->name)->count();
        return view('admin/validasi/berita', $data);
    }

    public function validasiBerita($id){
        $berita = Berita::find($id);
        $berita->validasi = 'sudah';
        $berita->save();

        return redirect('/admin/validasi-berita')->with('message', 'Berhasil memvalidasi item');
    }

    public function validListBerita(){
        $data['list_berita'] = \DB::table('berita')->where('validasi', '=', 'sudah')->orderBy('updated_at', 'desc')->get();
        $data['unread'] = Inbox::where('read_inbox', 'unread')->where('kepada', '=', auth()->user()->name)->where('delete_status_in', 'no')->count();
        $data['unvalidate'] = Berita::where('validasi', 'belum')->count();
        $data['notifikasi'] = Notifikasi::where('pemilik', auth()->user()->name)->count();
        $data['total_pesanan'] = Pesanan::where('pemilik_produk', auth()->user()->name)->count();
        return view('admin/validasi/unberita', $data);
    }

    public function unvalidasiBerita($id){
        $berita = Berita::find($id);
        $berita->validasi = 'belum';
        $berita->save();

        return redirect('/admin/unvalidasi-berita')->with('message', 'Berhasil membatalkan validasi item');
    }

    public function hapusBerita($id){
        $status = Berita::where('id', $id)->delete();
        if($status){
            return redirect()->back()->with('message', 'Berhasil menghapus berita');
        }
        else{
            return redirect()->back();
        }
    }
}
