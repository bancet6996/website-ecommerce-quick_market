<?php

namespace App\Http\Controllers;

use App\Berita;
use App\Inbox;
use App\Keranjang;
use App\Komentar;
use App\Notifikasi;
use App\Pesanan;
use Illuminate\Http\Request;
use App\Produk;
use App\Rating;
use Illuminate\Support\Facades\Auth;

class ProdukController extends Controller
{
    public function index()
    {
        $data['produk'] = Produk::where('pemilik', '=', auth()->user()->name)->orderBy('id', 'desc')->where('delete_status', 'no')->get();
        $data['unread'] = Inbox::where('read_inbox', 'unread')->where('kepada', '=', auth()->user()->name)->where('delete_status_in', 'no')->count();
        $data['unvalidate'] = Berita::where('validasi', 'belum')->count();
        $data['notifikasi'] = Notifikasi::where('pemilik', auth()->user()->name)->count();
        $data['total_pesanan'] = Pesanan::where('pemilik_produk', auth()->user()->name)->count();
        return view('admin/produk/produk', $data);
    }

    public function create(){
        $data['kategori'] = \DB::table('kategori_produk')->orderBy('nama_kategori', 'asc')->get();
        $data['unread'] = Inbox::where('read_inbox', 'unread')->where('kepada', '=', auth()->user()->name)->where('delete_status_in', 'no')->count();
        $data['unvalidate'] = Berita::where('validasi', 'belum')->count();
        $data['notifikasi'] = Notifikasi::where('pemilik', auth()->user()->name)->count();
        $data['total_pesanan'] = Pesanan::where('pemilik_produk', auth()->user()->name)->count();
        return view('admin/produk/form', $data);
    }
    
    public function store(Request $request){
        $rule = [
            'nama_produk' => 'required|string',
            'kategori_produk' => 'required|string',
            'deskripsi_produk' => 'required|string',
            'harga_produk' => 'required|numeric|min:0',
            'stok_produk' => 'required|numeric|min:0',
            'gambar_produk' => 'required|image|mimes:jpg,png,jpeg,svg|max:2048',
            'berat_produk' => 'required|numeric|min:0',
            'lebar_produk' => 'nullable|string|min:0',
            'tinggi_produk' => 'nullable|string|min:0',
            'ketebalan_produk' => 'nullable|string|min:0',
            'deskripsi_lengkap_produk' => 'nullable|string',
        ];
        $this->validate($request, $rule);

        $tautan = Produk::max('id') + 1;

        Produk::create([
            'pemilik' => auth()->user()->name,
            'nama_produk' => $request->nama_produk,
            'kategori_produk' => $request->kategori_produk,
            'gambar_produk' => $request->gambar_produk,
            'deskripsi_produk' => $request->deskripsi_produk,
            'harga_produk' => abs($request->harga_produk),
            'stok_produk' => abs($request->stok_produk),
            'berat_produk' => abs($request->berat_produk),
            'lebar_produk' => $request->lebar_produk,
            'tinggi_produk' => abs($request->tinggi_produk),
            'ketebalan_produk' => abs($request->ketebalan_produk),
            'deskripsi_lengkap_produk' => $request->deskripsi_lengkap_produk,
            'delete_status' => 'no',
            'tautan' => '/user/product/'. $tautan .'/info',
            'favorit' => 0,
            'rating' => 0,
            'trending' => 0,
            'kota' => auth()->user()->kota,
        ]);

        $file = $request->file('gambar_produk');
   
        // Mendapatkan Nama File
        // $nama_file = $file->getClientOriginalName();
    
        // Mendapatkan Extension File
        // $extension = $file->getClientOriginalExtension();
    
        // Mendapatkan Ukuran File
        // $ukuran_file = $file->getSize();
    
        // Proses Upload File
        $destinationPath = 'uploads/tmp';
        $file->move($destinationPath, $file);

        return redirect('/admin/produk')->with('message', 'Berhasil menambahkan produk');
    }

    public function edit(Request $request, $id){
        $data['produk'] = Produk::find($id);
        $data['kategori'] = \DB::table('kategori_produk')->orderBy('nama_kategori', 'asc')->get();
        $data['unread'] = Inbox::where('read_inbox', 'unread')->where('kepada', '=', auth()->user()->name)->where('delete_status_in', 'no')->count();
        $data['unvalidate'] = Berita::where('validasi', 'belum')->count();
        $data['notifikasi'] = Notifikasi::where('pemilik', auth()->user()->name)->count();
        $data['total_pesanan'] = Pesanan::where('pemilik_produk', auth()->user()->name)->count();   

        if (Produk::where('id', $id)->where('pemilik', auth()->user()->name)->first()) {
            return view('admin/produk/form', $data);
        } else {
            return redirect()->back();
        }
        
    }

    public function update(Request $request, $id){
        $rule = [
            'nama_produk' => 'required|string',
            'kategori_produk' => 'required|string',
            'deskripsi_produk' => 'required|string',
            'kategori_produk' => 'required|string',
            'harga_produk' => 'required|numeric|min:0',
            'stok_produk' => 'required|integer',
            'gambar_produk' => 'required|image|mimes:jpg,png,jpeg,svg|max:2048',
            'berat_produk' => 'required|numeric',
            'lebar_produk' => 'nullable|string',
            'tinggi_produk' => 'nullable|string',
            'ketebalan_produk' => 'nullable|string',
            'deskripsi_lengkap_produk' => 'nullable|string',
        ];
        $this->validate($request, $rule);

        $produk = Produk::find($id);
        $produk->pemilik = auth()->user()->name;
        $produk->nama_produk = $request->nama_produk;
        $produk->kategori_produk = $request->kategori_produk;
        $produk->gambar_produk = $request->gambar_produk;
        $produk->deskripsi_produk = $request->deskripsi_produk;
        $produk->harga_produk = $request->harga_produk;
        $produk->stok_produk = $request->stok_produk;
        $produk->berat_produk = $request->berat_produk;
        $produk->lebar_produk = $request->lebar_produk;
        $produk->tinggi_produk = $request->tinggi_produk;
        $produk->ketebalan_produk = $request->ketebalan_produk;
        $produk->deskripsi_lengkap_produk = $request->deskripsi_lengkap_produk;
        $produk->delete_status = 'no';
        $produk->kota = auth()->user()->kota;
        $produk->save();

        $file = $request->file('gambar_produk');
        $destinationPath = 'uploads/tmp';
        $file->move($destinationPath, $file);

        return redirect('/admin/produk')->with('message', 'Berhasil mengubah produk');
    }

    public function destroy(Request $request, $id){
        $status = Produk::where('id', $id)->where('pemilik', auth()->user()->name)->where('delete_status', 'no')->update([
            'delete_status' => 'yes',
        ]);
        if($status){
            return redirect('/admin/produk')->with('message', 'Produk telah dipindahkan ke tempat sampah');
        }
        else{
            return redirect()->back();
        }
    }

    public function infoProduk(Request $request, $id){
        $produk = Produk::find($id);
        $data['produk'] = Produk::find($id);
        if (Auth::check()) {
            $data['count_produk'] = Keranjang::where('pemilik', auth()->user()->name)->count();
            $data['top'] = Produk::where('pemilik', '!=', auth()->user()->name)->orderBy('favorit', 'desc')->get();
        }
        else{
            $data['top'] = Produk::orderBy('favorit', 'desc')->get();
        }
        $produk = Produk::find($id);
        $data['komentar'] = Komentar::where('category', '=', 'produk')->where('to_post', '=', $produk->id)->where('delete_status', 'no')->get();
        $data['rating'] = Rating::get();
        $data['avg_rating'] = Rating::avg('rating');
        $data['total_rating'] = Rating::count();
        $data['total_rating_1'] = Rating::where('rating', 1)->count();
        $data['total_rating_2'] = Rating::where('rating', 2)->count();
        $data['total_rating_3'] = Rating::where('rating', 3)->count();
        $data['total_rating_4'] = Rating::where('rating', 4)->count();
        $data['total_rating_5'] = Rating::where('rating', 5)->count();

        if (Auth::check()) {
            Notifikasi::where('pemilik', auth()->user()->name)->where('status', 'unread')->update([
                'status' => 'readed',
            ]);
        }

        return view('user/infoproduk/infoproduk', $data);
    }

    public function purge($id){
        $status = Produk::where('id', $id)->where('pemilik', auth()->user()->name)->where('delete_status', 'yes')->delete();
        if($status){
            return redirect()->back()->with('message', 'Berhasil menghapus produk');
        }
        else{
            return redirect()->back();
        }
    }
}
