<?php

namespace App\Http\Controllers;

use App\Berita;
use App\Inbox;
use App\Notifikasi;
use App\Pesanan;
use Illuminate\Http\Request;
use App\Produk;

class TrashController extends Controller
{
    public function index(){
        $data['trash_produk'] = Produk::where('pemilik', '=', auth()->user()->name)->where('delete_status', 'yes')->orderBy('id', 'desc')->get();
        $data['trash_berita'] = Berita::where('penulis', '=', auth()->user()->name)->where('delete_status', 'yes')->orderBy('id', 'desc')->get();        
        $data['unread'] = Inbox::where('read_inbox', 'unread')->where('kepada', '=', auth()->user()->name)->where('delete_status_in', 'no')->count();
        $data['unvalidate'] = Berita::where('validasi', 'belum')->count();
        $data['notifikasi'] = Notifikasi::where('pemilik', auth()->user()->name)->count();
        $data['total_pesanan'] = Pesanan::where('pemilik_produk', auth()->user()->name)->count();
        return view('admin/trash/trash', $data);
    }

    public function restoreProduk(Request $request, $id){
        $status = Produk::where('id', $id)->where('pemilik', auth()->user()->name)->where('delete_status', 'yes')->update([
            'delete_status' => 'no',
        ]);
        if($status){
            return redirect('/admin/trash')->with('message', 'Berhasil mengembalikan produk');
        }
        else{
            return redirect()->back();
        }
    }

    public function restoreBerita(Request $request, $id){
        $status = Berita::where('id', $id)->where('penulis', auth()->user()->name)->where('delete_status', 'yes')->update([
            'delete_status' => 'no',
        ]);
        if($status){
            return redirect('/admin/trash')->with('message', 'Berhasil mengembalikan berita');
        }
        else{
            return redirect()->back();
        }
    }
}
