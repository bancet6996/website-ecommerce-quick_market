<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Banner;
use App\Berita;
use App\Inbox;
use App\Notifikasi;
use App\Pesanan;

class BannerController extends Controller
{
    public function index()
    {
        $data['banner'] = Banner::where('pemilik', '=', auth()->user()->name)->orderBy('id', 'desc')->get();
        $data['unread'] = Inbox::where('read_inbox', 'unread')->where('kepada', '=', auth()->user()->name)->count();
        $data['unvalidate'] = Berita::where('validasi', 'belum')->count();
        $data['notifikasi'] = Notifikasi::where('pemilik', auth()->user()->name)->count();
        $data['total_pesanan'] = Pesanan::where('pemilik_produk', auth()->user()->name)->count();
        return view('admin/banner/banner', $data);
    }

    public function create(){
        $data['unread'] = Inbox::where('read_inbox', 'unread')->where('kepada', '=', auth()->user()->name)->where('delete_status_in', 'no')->count();
        $data['unvalidate'] = Berita::where('validasi', 'belum')->count();
        $data['notifikasi'] = Notifikasi::where('pemilik', auth()->user()->name)->count();
        $data['total_pesanan'] = Pesanan::where('pemilik_produk', auth()->user()->name)->count();
        return view('admin/banner/form', $data);
    }
    
    public function store(Request $request){
        $rule = [
            'gambar_banner' => 'required|image|mimes:jpg,png,jpeg,svg',
            'tautan' => 'required|string',
            'judul' => 'required|string',
            'keterangan' => 'required|string',
        ];
        $this->validate($request, $rule);

        Banner::create([
            'pemilik' => auth()->user()->name,
            'gambar_banner' => $request->gambar_banner,
            'tautan' => $request->tautan,
            'judul' => $request->judul,
            'keterangan' => $request->keterangan,
        ]);

        $file = $request->file('gambar_banner');
        $destinationPath = 'uploads/tmp';
        $file->move($destinationPath, $file);

        return redirect('/admin/banner')->with('message', 'Berhasil menambahkan banner');
    }

    public function edit(Request $request, $id){
        $data['banner'] = Banner::find($id);
        $data['unread'] = Inbox::where('read_inbox', 'unread')->where('kepada', '=', auth()->user()->name)->where('delete_status_in', 'no')->count();
        $data['unvalidate'] = Berita::where('validasi', 'belum')->count();
        $data['notifikasi'] = Notifikasi::where('pemilik', auth()->user()->name)->count();
        $data['total_pesanan'] = Pesanan::where('pemilik_produk', auth()->user()->name)->count();
        return view('admin/banner/form', $data);
    }

    public function update(Request $request, $id){
        $rule = [
            'gambar_banner' => 'required|image|mimes:jpg,png,jpeg,svg'
        ];
        $this->validate($request, $rule);

        $banner = Banner::find($id);
        $banner->pemilik = auth()->user()->name;
        $banner->gambar_banner = $request->gambar_banner;
        $banner->save();

        $file = $request->file('gambar_banner');
        $destinationPath = 'uploads/tmp';
        $file->move($destinationPath, $file);

        return redirect('/admin/banner')->with('message', 'Berhasil mengubah banner');
    }

    public function destroy(Request $request, $id){
        $status = Banner::where('id', $id)->delete();
        if($status){
            return redirect('/admin/banner')->with('message', 'Berhasil menghapus banner');
        }
        else{
            return redirect('/admin/banner');
        }
    }
}
