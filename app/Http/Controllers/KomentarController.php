<?php

namespace App\Http\Controllers;

use App\Komentar;
use App\Produk;
use App\Berita;
use App\Notifikasi;
use Illuminate\Http\Request;

class KomentarController extends Controller
{
    public function commentProduct(Request $request, $id){
        $rule = [
            'komentar' => 'required|string',
        ];
        $this->validate($request, $rule);

        $produk = Produk::find($id);
        
        Komentar::create([
            'category' => 'produk',
            'to_post' => $produk->id,
            'provider' => auth()->user()->provider,
            'name' => auth()->user()->name,
            'image' => auth()->user()->image,
            'email' => auth()->user()->email,
            'comment' => $request->komentar,
            'delete_status' => 'no',
        ]);

        if ($produk->pemilik != auth()->user()->name) {
            Notifikasi::create([
                'pemilik' => $produk->pemilik,
                'subjek' => auth()->user()->name.' telah mengomentari '. $produk->nama_produk,
                'category' => 'produk',
                'to_post' => 'user/product/'. $produk->id .'/info',
                'status' => 'unread',
            ]);
        } else {
            # code...
        }  
        
        return redirect()->back()->with('message', 'Komentar telah dikirim');
    }

    public function commentNews(Request $request, $id){
        $rule = [
            'komentar' => 'required|string',
        ];
        $this->validate($request, $rule);

        $berita = Berita::find($id);
        Komentar::create([
            'category' => 'berita',
            'to_post' => $berita->id,
            'provider' => auth()->user()->provider,
            'name' => auth()->user()->name,
            'image' => auth()->user()->image,
            'email' => auth()->user()->email,
            'comment' => $request->komentar,
            'delete_status' => 'no',
        ]);

        if ($berita->penulis != auth()->user()->name) {
            Notifikasi::create([
                'pemilik' => $berita->penulis,
                'subjek' => auth()->user()->name.' telah mengomentari berita yang berjudul ' .$berita->judul_berita,
                'category' => 'berita',
                'to_post' => 'user/news/'. $berita->id .'/read',
                'status' => 'unread',
            ]);
        } else {
            # code...
        }

        return redirect()->back()->with('message', 'Komentar telah dikirim');
    }

    public function deleteProductComment($id){
        $status = Komentar::where('id', $id)->where('category', 'produk')->where('name', auth()->user()->name)->update([
            'delete_status' => 'yes',
        ]);
        if($status){
            return redirect()->back()->with('message', 'Berhasil menghapus komentar');
        }
        else{
            return redirect()->back();
        }
    }

    public function deleteNewsComment($id){
        $status = Komentar::where('id', $id)->where('category', 'berita')->where('name', auth()->user()->name)->update([
            'delete_status' => 'yes',
        ]);
        if($status){
            return redirect()->back()->with('message', 'Berhasil menghapus komentar');
        }
        else{
            return redirect()->back();
        }
    }
}
