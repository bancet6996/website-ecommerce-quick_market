<?php

namespace App\Http\Controllers;

use App\Keranjang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(){
        $data['users'] = \DB::table('users')->get();
        if (Auth::check()) {
            $data['count_produk'] = Keranjang::where('pemilik', auth()->user()->name)->count();
        }
        return view('user/pages/login', $data);
    }

    public function register(){
        $data['users'] = \DB::table('users')->get();
        if (Auth::check()) {
            $data['count_produk'] = Keranjang::where('pemilik', auth()->user()->name)->count();
        }
        return view('user/pages/register', $data);
    }
}
