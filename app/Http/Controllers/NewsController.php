<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Berita;
use App\Keranjang;
use App\Komentar;
use App\Notifikasi;
use Illuminate\Support\Facades\Auth;

class NewsController extends Controller
{
    public function index(Request $request){
        $data['news'] = Berita::where('validasi', '=', 'sudah')->paginate(4);
        $data['kategori'] = \DB::table('kategori_berita')->orderBy('nama_kategori', 'asc')->get();
        $data['ob_visit'] = Berita::where('validasi', '=', 'sudah')->orderBy('kunjungan', 'desc')->get();
        if (Auth::check()) {
            $data['count_produk'] = Keranjang::where('pemilik', auth()->user()->name)->count();
        }
        return view('user/news/news', $data);
    }

    public function readmore(Request $request, $id){
        $berita = Berita::find($id);
        $data['news'] = Berita::where('validasi', '=', 'sudah')->find($id);
        $data['kategori'] = \DB::table('kategori_berita')->orderBy('nama_kategori', 'asc')->get();
        $data['ob_visit'] = Berita::where('validasi', '=', 'sudah')->orderBy('kunjungan', 'desc')->get();
        $data['users'] = \DB::table('users')->get();
        $data['total_komentar'] = Komentar::where('category', 'berita')->where('to_post', $berita->id)->where('delete_status', 'no')->count();
        $data['komentar'] = Komentar::where('category', 'berita')->where('to_post', $berita->id)->where('delete_status', 'no')->get();
        Berita::where('id', $id)->increment('kunjungan');

        if($berita->kunjungan == null){
            $berita->kunjungan += 1;
        }
        else{
            $berita->kunjungan += 0;
        }
        $berita->save();

        if (Auth::check()) {
            $data['count_produk'] = Keranjang::where('pemilik', auth()->user()->name)->count();
            Notifikasi::where('pemilik', auth()->user()->name)->where('status', 'unread')->update([
                'status' => 'readed',
            ]);
        }

        return view('user/readmore/readmore', $data);
    }

    public function fltKategori(Request $request){
        $kategori = $request->nama_kategori;
        $data['news'] = Berita::where('kategori_berita', '=', $kategori)->get();
        $data['kategori'] = \DB::table('kategori_berita')->orderBy('nama_kategori', 'asc')->get();
        $data['count_category'] = Berita::where('kategori_berita', '=', '')->count();
        $data['ob_visit'] = Berita::orderBy('kunjungan', 'desc')->get();
        if (Auth::check()) {
            $data['count_produk'] = Keranjang::where('pemilik', auth()->user()->name)->count();
        }
        return view('user/news/news', $data);
    }

    public function fltPenulis(Request $request){
        $kategori = $request->penulis;
        $data['news'] = Berita::where('penulis', '=', $kategori)->get();
        $data['kategori'] = \DB::table('kategori_berita')->orderBy('nama_kategori', 'asc')->get();
        $data['count_category'] = Berita::where('kategori_berita', '=', '')->count();
        $data['ob_visit'] = Berita::orderBy('kunjungan', 'desc')->get();
        if (Auth::check()) {
            $data['count_produk'] = Keranjang::where('pemilik', auth()->user()->name)->count();
        }
        return view('user/news/news', $data);
    }

    public static function totalKategori($string){
        return Berita::where('kategori_berita', $string)->count();
    }

    public static function totalKomentar($string){
        return Komentar::where('category', 'berita')->where('to_post', $string)->where('delete_status', 'no')->count();
    }
}
