<?php

namespace App\Http\Controllers;

use App\Keranjang;
use App\City;
use App\Courier;
use App\Kurir;
use App\Province;
use App\Voucher;
use Illuminate\Http\Request;
use Kavist\RajaOngkir\Facades\RajaOngkir;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;

class CheckoutController extends Controller
{
    public function index(Request $request){
        $jalur = $request->kota_asal. '-' .$request->kota_tujuan;
        $total_berat = Keranjang::where('pemilik', auth()->user()->name)->sum('total_berat');
        $data['keranjang'] = Keranjang::where('pemilik', auth()->user()->name)->get();
        $data['couriers'] = Courier::pluck('title', 'code');
        $data['provinces'] = Province::pluck('title', 'province_id');
        $data['kurir'] = Kurir::where('jalur', $jalur)->where('skt_kurir', $request->kurir)->where('total_berat', $total_berat)->get();
        $data['count_produk'] = Keranjang::where('pemilik', auth()->user()->name)->count();
        $data['subtotal'] = Keranjang::where('pemilik', auth()->user()->name)->sum('total_harga');
        return view('user/shop/checkout/checkout', $data);
    }

    public function checkout(Request $request, $id){
        if (Keranjang::where('pemilik', auth()->user()->name) === null) {
            return redirect()->back();
        } else {
            $harga = Keranjang::find($id);
            Keranjang::where('pemilik', auth()->user()->name)->update([
                'kuantitas' => $request->kuantitas,
                'total_harga' => $harga->harga_produk * $request->kuantitas,
            ]);

            return redirect('/user/checkout');
        }
    }

    public function getCities($id){
        $city = City::where('province_id', $id)->pluck('title', 'city_id');
        return json_encode($city);
    }

    public function submit(Request $request){
        if (auth()->user()->kota === null) {
            return redirect('/user/address/'. auth()->user()->name)->with('message', 'Mohon isi alamat terlebih dahulu');
        } else {
            $rule = [
                'kurir' => 'required|string',
            ];
            $this->validate($request, $rule);

            $total_berat = Keranjang::where('pemilik', auth()->user()->name)->sum('total_berat');
            $keranjang = Keranjang::where('pemilik', auth()->user()->name)->first();

            if (session('cart')) {
                $total_berat = Keranjang::where('pemilik', auth()->user()->name)->sum('total_berat');
                $cart = session()->get('cart');
                $total = 0;

                foreach (session('cart') as $id => $details) {
                    $total += $details['berat_produk'];
                }
                $cost = RajaOngkir::ongkosKirim([
                    'origin' => $cart[$id]["kota"],
                    'destination' => auth()->user()->kota,
                    'weight' => $total + $total_berat,
                    'courier' => $request->kurir,
                ])->get();

                $jalur = $cart[$id]["kota"]. '-' .auth()->user()->kota;
        
                if (Kurir::where('jalur', $jalur)->where('skt_kurir', $request->kurir)->where('total_berat', $total + $total_berat)->first()) {
                    $data['keranjang'] = Keranjang::where('pemilik', auth()->user()->name)->get();
                    $data['subtotal'] = Keranjang::where('pemilik', auth()->user()->name)->sum('total_harga');
                    $data['couriers'] = Courier::pluck('title', 'code');
                    $data['provinces'] = Province::pluck('title', 'province_id');
                    $data['kurir'] = Kurir::where('jalur', $jalur)->where('skt_kurir', $request->kurir)->where('total_berat', $total + $total_berat)->get();
                    $data['count_produk'] = Keranjang::where('pemilik', auth()->user()->name)->count();
                    return view('user/shop/checkout/checkout', $data);
                } else {
                    for ($x = 0; $x <= count($cost[0]['costs'])-1; $x++) {
                        $kurir = new Kurir();
                        $kurir->jalur = $jalur;
                        $kurir->skt_kurir = $cost[0]['code'];
                        $kurir->pjg_kurir = $cost[0]['name'];
                        $kurir->skt_jenis_layanan = $cost[0]['costs'][$x]['service'];
                        $kurir->pjg_jenis_layanan = $cost[0]['costs'][$x]['description'];
                        $kurir->harga = $cost[0]['costs'][$x]['cost'][0]['value'];
                        $kurir->estimasi = $cost[0]['costs'][$x]['cost'][0]['etd'];
                        $kurir->total_berat = $total + $total_berat;
                        $array[$x] = $kurir->save();
                    }
                    if ($array) {
                        $data['keranjang'] = Keranjang::where('pemilik', auth()->user()->name)->get();
                        $data['subtotal'] = Keranjang::where('pemilik', auth()->user()->name)->sum('total_harga');
                        $data['couriers'] = Courier::pluck('title', 'code');
                        $data['provinces'] = Province::pluck('title', 'province_id');
                        $data['kurir'] = Kurir::where('jalur', $jalur)->where('skt_kurir', $request->kurir)->where('total_berat', $total + $total_berat)->get();
                        $data['count_produk'] = Keranjang::where('pemilik', auth()->user()->name)->count();
                        return view('user/shop/checkout/checkout', $data);
                    } else {
                        return redirect()->back()->with('ongkir', 'Gagal mencek ongkir');
                    }
                }
            }
            else{
                $total_berat = Keranjang::where('pemilik', auth()->user()->name)->sum('total_berat');
                $cost = RajaOngkir::ongkosKirim([
                    'origin' => $keranjang->kota,
                    'destination' => auth()->user()->kota,
                    'weight' => $total_berat,
                    'courier' => $request->kurir,
                ])->get();
        
                $jalur = $keranjang->kota. '-' .auth()->user()->kota;
        
                if (Kurir::where('jalur', $jalur)->where('skt_kurir', $request->kurir)->where('total_berat', $total_berat)->first()) {
                    $data['keranjang'] = Keranjang::where('pemilik', auth()->user()->name)->get();
                    $data['subtotal'] = Keranjang::where('pemilik', auth()->user()->name)->sum('total_harga');
                    $data['couriers'] = Courier::pluck('title', 'code');
                    $data['provinces'] = Province::pluck('title', 'province_id');
                    $data['kurir'] = Kurir::where('jalur', $jalur)->where('skt_kurir', $request->kurir)->where('total_berat', $total_berat)->get();
                    $data['count_produk'] = Keranjang::where('pemilik', auth()->user()->name)->count();
                    return view('user/shop/checkout/checkout', $data);
                } else {
                    for ($x = 0; $x <= count($cost[0]['costs'])-1; $x++) {
                        $kurir = new Kurir();
                        $kurir->jalur = $jalur;
                        $kurir->skt_kurir = $cost[0]['code'];
                        $kurir->pjg_kurir = $cost[0]['name'];
                        $kurir->skt_jenis_layanan = $cost[0]['costs'][$x]['service'];
                        $kurir->pjg_jenis_layanan = $cost[0]['costs'][$x]['description'];
                        $kurir->harga = $cost[0]['costs'][$x]['cost'][0]['value'];
                        $kurir->estimasi = $cost[0]['costs'][$x]['cost'][0]['etd'];
                        $kurir->total_berat = $total_berat;
                        $array[$x] = $kurir->save();                
                    }
                    if ($array) {
                        $data['keranjang'] = Keranjang::where('pemilik', auth()->user()->name)->get();
                        $data['subtotal'] = Keranjang::where('pemilik', auth()->user()->name)->sum('total_harga');
                        $data['couriers'] = Courier::pluck('title', 'code');
                        $data['provinces'] = Province::pluck('title', 'province_id');
                        $data['kurir'] = Kurir::where('jalur', $jalur)->where('skt_kurir', $request->kurir)->where('total_berat', $total_berat)->get();
                        $data['count_produk'] = Keranjang::where('pemilik', auth()->user()->name)->count();
                        return view('user/shop/checkout/checkout', $data);
                    } else {
                        return redirect()->back()->with('ongkir', 'Gagal mencek ongkir');
                    }
                }
            }
        }
    }
}
