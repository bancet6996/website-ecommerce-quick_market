<?php

namespace App\Http\Controllers;

use App\Notifikasi;
use App\Pesanan;
use App\Produk;
use App\Rating;
use Illuminate\Http\Request;

class RatingController extends Controller
{
    public function rating(Request $request, $id){
        $rule = [
            'rating' => 'required',
            'ulasan' => 'required|string',
        ];
        $this->validate($request, $rule);

        $produk = Produk::find($id);
        
        Rating::create([
            'to_post' => $produk->id,
            'provider' => auth()->user()->provider,
            'name' => auth()->user()->name,
            'image' => auth()->user()->image,
            'email' => auth()->user()->email,
            'rating' => $request->rating,
            'comment' => $request->ulasan,
            'delete_status' => 'no',
        ]);

        $produk->rating = $produk->rating + $request->rating;
        $produk->save();

        if ($produk->pemilik != auth()->user()->name) {
            Notifikasi::create([
                'pemilik' => $produk->pemilik,
                'subjek' => auth()->user()->name.' telah memberi nilai '. $produk->nama_produk,
                'category' => 'produk',
                'to_post' => 'user/product/'. $produk->id .'/info',
                'status' => 'unread',
            ]);
        } else {
            # code...
        } 

        Pesanan::where('pemilik', auth()->user()->name)->where('id_produk', $produk->id)->update([
            'rating' => 'sudah',
        ]);
        
        return redirect()->back()->with('message', 'Rating telah dikirim');
    }
}
