<?php

namespace App\Http\Controllers;

use App\Berita;
use App\Inbox;
use App\Notifikasi;
use App\Pesanan;
use Illuminate\Http\Request;

class OutboxController extends Controller
{
    public function index(){       
        $data['collection'] = \DB::table('inbox')->where('name', '=', auth()->user()->name)->where('delete_status_out', 'no')->orderBy('id', 'desc')->get();
        $data['unread'] = Inbox::where('read_inbox', 'unread')->where('kepada', '=', auth()->user()->name)->where('delete_status_in', 'no')->count();
        $data['unvalidate'] = Berita::where('validasi', 'belum')->count();
        $data['notifikasi'] = Notifikasi::where('pemilik', auth()->user()->name)->count();
        $data['total_pesanan'] = Pesanan::where('pemilik_produk', auth()->user()->name)->count();
        return view('admin/outbox/outbox', $data);
    }

    public function read(Request $request, $id){
        $data['inbox'] = \DB::table('inbox')->find($id);
        $data['unread'] = Inbox::where('read_inbox', 'unread')->where('kepada', '=', auth()->user()->name)->where('delete_status_in', 'no')->count();
        $data['unvalidate'] = Berita::where('validasi', 'belum')->count();
        $data['notifikasi'] = Notifikasi::where('pemilik', auth()->user()->name)->count();
        $data['total_pesanan'] = Pesanan::where('pemilik_produk', auth()->user()->name)->count();

        if (\DB::table('inbox')->where('id', $id)->where('name', auth()->user()->name)->where('delete_status_out', 'no')->first()) {
            return view('admin/outbox/read', $data);
        } else {
            return redirect()->back();
        }
        
    }

    public function destroy(Request $request, $id){
        $status = Inbox::where('id', $id)->where('name', auth()->user()->name)->where('delete_status_in', 'no')->update([
            'delete_status_out' => 'yes',
        ]);
        if($status){
            return redirect('/admin/outbox')->with('message', 'Berhasil menghapus pesan');
        }
        else{
            return redirect()->back();
        }
    }
}
