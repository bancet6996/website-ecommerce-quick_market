<?php

namespace App\Http\Middleware;

use App\Keranjang;
use Closure;

class CheckCart
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (empty(Keranjang::where('pemilik', auth()->user()->name)->get())) {
            return redirect()->back();
        }
        else{
            return $next($request);
        }
        
        return $next($request);
    }
}
