<?php

namespace App\Http\Middleware;

use Closure;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user()->provider != 'superadmin' && auth()->user()->provider != 'admin') {
            return redirect()->back();
        }
        else{
            return $next($request);
        }
        
        return $next($request);
    }
}
