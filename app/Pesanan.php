<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pesanan extends Model
{
    protected $table = 'pesanan';
    protected $fillable = ['pemilik', 'id_produk', 'pemilik_produk', 'nama_produk', 'gambar_produk', 'deskripsi_produk', 'harga_produk', 'berat_produk', 'kuantitas', 'total_harga', 'total_berat', 'akumulasi_pembelian', 'status_pengiriman', 'no_resi', 'rating', 'kurir',];
    public $timestamps = true;
}
