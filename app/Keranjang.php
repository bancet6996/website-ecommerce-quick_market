<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keranjang extends Model
{
    protected $table = 'keranjang';
    protected $fillable = ['pemilik', 'id_produk', 'pemilik_produk', 'nama_produk', 'gambar_produk', 'deskripsi_produk', 'harga_produk', 'berat_produk', 'stok_produk', 'kuantitas', 'total_harga', 'total_berat', 'status_pesanan', 'kota',];
    public $timestamps = true;
}
