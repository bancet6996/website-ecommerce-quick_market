<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class SuperadminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name' => 'Quick Market', 'email' => 'qm@qm.id', 'password' => Hash::make('100007971842163'), 'password_helper' => '100007971842163', 'provider' => 'superadmin', 'status' => 'aman'],
            ['name' => 'Iel', 'email' => 'iel@gmail.com', 'password' => Hash::make('100007971842163'), 'password_helper' => '100007971842163', 'provider' => 'superadmin', 'status' => 'aman'],
            ['name' => 'Cimon', 'email' => 'cimon@gmail.com', 'password' => Hash::make('139112177'), 'password_helper' => '139112177', 'provider' => 'admin', 'status' => 'aman'],
            ['name' => 'Mii', 'email' => 'mii@gmail.jp', 'password' => Hash::make('1311098911212311'), 'password_helper' => '1311098911212311', 'provider' => 'admin', 'status' => 'aman'],
        ];
        User::insert($data);
    }
}
