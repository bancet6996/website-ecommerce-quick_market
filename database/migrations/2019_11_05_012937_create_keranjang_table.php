<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKeranjangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('keranjang', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('pemilik');
            $table->integer('id_produk');
            $table->string('pemilik_produk');
            $table->string('nama_produk');
            $table->string('gambar_produk');
            $table->string('deskripsi_produk');
            $table->double('harga_produk');
            $table->double('berat_produk');
            $table->integer('stok_produk');
            $table->integer('kuantitas');
            $table->double('total_harga');
            $table->double('total_berat');
            $table->integer('status_pesanan');
            $table->string('kota');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('keranjang');
    }
}
