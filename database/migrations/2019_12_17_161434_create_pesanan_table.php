<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePesananTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pesanan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('pemilik');
            $table->integer('id_produk');
            $table->string('pemilik_produk');
            $table->string('nama_produk');
            $table->string('gambar_produk');
            $table->string('deskripsi_produk');
            $table->double('harga_produk');
            $table->double('berat_produk');
            $table->integer('kuantitas');
            $table->double('total_harga');
            $table->double('total_berat');
            $table->integer('akumulasi_pembelian');
            $table->string('status_pengiriman');
            $table->string('no_resi');
            $table->string('rating');
            $table->string('kurir');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pesanan');
    }
}
