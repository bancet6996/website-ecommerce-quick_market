<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProdukTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produk', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('pemilik');
            $table->string('nama_produk');
            $table->string('kategori_produk');
            $table->string('gambar_produk');
            $table->string('deskripsi_produk');
            $table->double('harga_produk');
            $table->double('stok_produk');
            $table->double('berat_produk');
            $table->double('lebar_produk')->nullable();
            $table->double('tinggi_produk')->nullable();
            $table->double('ketebalan_produk')->nullable();
            $table->longText('deskripsi_lengkap_produk')->nullable();
            $table->string('delete_status')->nullable();
            $table->string('tautan')->nullable();
            $table->integer('favorit')->nullable();
            $table->integer('rating')->nullable();
            $table->integer('trending')->nullable();
            $table->string('kota')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produk');
    }
}
