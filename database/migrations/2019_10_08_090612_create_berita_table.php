<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBeritaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('berita', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('penulis');
            $table->string('judul_berita');
            $table->string('caption_berita')->nullable();;
            $table->string('kategori_berita');
            $table->string('thumbnail_berita');
            $table->longText('deskripsi_berita');
            $table->string('kunjungan');
            $table->string('delete_status');
            $table->string('validasi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('berita');
    }
}
