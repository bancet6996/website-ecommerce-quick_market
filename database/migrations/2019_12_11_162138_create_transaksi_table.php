<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransaksiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('pemilik');
            $table->string('kurir');
            $table->string('jenis_layanan');
            // $table->string('alamat_penerima')->nullable();
            // $table->string('kota_penerima')->nullable();
            // $table->string('kode_pos_penerima')->nullable();
            // $table->string('alamat_pengirim')->nullable();
            // $table->string('kota_pengirim')->nullable();
            // $table->string('kode_pos_pengirim')->nullable();
            $table->string('metode_pembayaran');
            $table->double('ongkir');
            $table->double('total_harga');
            $table->string('status_pembayaran');
            $table->string('no_resi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi');
    }
}
