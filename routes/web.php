<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'UserzController@index')->name('beranda');

Auth::routes();
Route::get('/home', 'UserzController@index')->name('home');

Route::middleware(['auth', 'admin', 'banned'])->group(function(){
    //Admin
    Route::get('/admin', 'AdminController@index');
    Route::get('/admin/about', 'AdminController@about');
   
    //Produk
    Route::get('/admin/produk', 'ProdukController@index');
    Route::get('/admin/produk/create', 'ProdukController@create');
    Route::post('/admin/produk', 'ProdukController@store');
    Route::get('/admin/produk/{id}/edit', 'ProdukController@edit');
    Route::patch('/admin/produk/{id}', 'ProdukController@update');
    Route::patch('/admin/produk/{id}/delete', 'ProdukController@destroy');
    Route::delete('/admin/produk/{id}/purge', 'ProdukController@purge');

    //Berita
    Route::get('/admin/berita', 'BeritaController@index');
    Route::get('/admin/berita/create', 'BeritaController@create');
    Route::post('/admin/berita', 'BeritaController@store');
    Route::get('/admin/berita/{id}/edit', 'BeritaController@edit');
    Route::patch('/admin/berita/{id}', 'BeritaController@update');
    Route::patch('/admin/berita/{id}', 'BeritaController@destroy');
    Route::get('/admin/berita/{id}/baca', 'BeritaController@baca');
    Route::delete('/admin/berita/{id}/purge', 'BeritaController@purge');

    //Inbox
    Route::get('/admin/inbox', 'InboxController@index');
    Route::get('/admin/inbox/{id}/read','InboxController@read');
    Route::patch('/admin/inbox/{id}', 'InboxController@destroy');

    //Outbox
    Route::get('/admin/outbox', 'OutboxController@index');
    Route::get('/admin/outbox/{id}/read','OutboxController@read');
    Route::patch('/admin/outbox/{id}', 'OutboxController@destroy');

    //Trash
    Route::get('/admin/trash', 'TrashController@index');
    Route::patch('/admin/produk/{id}/restore', 'TrashController@restoreProduk');
    Route::patch('/admin/berita/{id}/restore', 'TrashController@restoreBerita');

    //Notification
    Route::get('/admin/notification', 'NotifikasiController@index');

    //Transaction
    Route::get('/admin/transaction', 'TransaksiController@indexAdmin');

    //Order
    Route::get('/admin/order', 'PesananController@index');
});

Route::middleware(['auth', 'superadmin'])->group(function(){

    //Banner
    Route::get('/admin/banner', 'BannerController@index');
    Route::get('/admin/banner/create', 'BannerController@create');
    Route::post('/admin/banner','BannerController@store');
    Route::get('/admin/banner/{id}/edit', 'BannerController@edit');
    Route::patch('/admin/banner/{id}', 'BannerController@update');
    Route::delete('/admin/banner/{id}', 'BannerController@destroy');

    Route::delete('/admin/berita/hapus/{id}', 'ValidasiController@hapusBerita');

     //User (Admin)
    Route::get('/admin/adminuser', 'UserController@index');
    Route::patch('/admin/adminuser/{id}', 'UserController@banned');
    Route::get('/admin/banned-list', 'UserController@list');
    Route::patch('/admin/banned-list/{id}', 'UserController@unbanned');
    Route::get('/banned', 'UserController@banneder');

    //Kategori Produk Admin
    Route::get('/admin/kategori-produk', 'KategoriProdukController@index');
    Route::get('/admin/kategori-produk/create', 'KategoriProdukController@create');
    Route::post('/admin/kategori-produk', 'KategoriProdukController@store');
    Route::get('/admin/kategori-produk/{id}/edit', 'KategoriProdukController@edit');
    Route::patch('/admin/kategori-produk/{id}', 'KategoriProdukController@update');
    Route::delete('/admin/kategori-produk/{id}', 'KategoriProdukController@destroy');

    //Kategori Berita Admin
    Route::get('/admin/kategori-berita', 'KategoriBeritaController@index');
    Route::get('/admin/kategori-berita/create', 'KategoriBeritaController@create');
    Route::post('/admin/kategori-berita', 'KategoriBeritaController@store');
    Route::get('/admin/kategori-berita/{id}/edit', 'KategoriBeritaController@edit');
    Route::patch('/admin/kategori-berita/{id}', 'KategoriBeritaController@update');
    Route::delete('/admin/kategori-berita/{id}', 'KategoriBeritaController@destroy');

    //Validasi
    Route::get('/admin/validasi-berita', 'ValidasiController@listBerita');
    Route::patch('/admin/validasi-berita/{id}', 'ValidasiController@validasiBerita');
    Route::get('/admin/unvalidasi-berita', 'ValidasiController@validListBerita');
    Route::patch('/admin/unvalidasi-berita/{id}', 'ValidasiController@unvalidasiBerita');

    //Voucher
    Route::get('/admin/voucher', 'VoucherController@indexAdmin');
    Route::get('/admin/voucher/create', 'VoucherController@acakKode');
    Route::post('/admin/voucher/generate', 'VoucherController@buatKode');
});

Route::middleware('auth', 'banned')->group(function(){
    
    //User Setting
    Route::get('/user/setting/{name}', 'UserzController@setting');
    Route::put('/user/setting/{name}/update', 'UserzController@update');
    Route::put('/user-to-admin/{$id}', 'UserzController@beAdmin');

    //User Address
    Route::get('/user/address/{name}', 'UserzController@address');
    Route::put('/user/address/{name}/update', 'UserzController@addressUpdate');

    //User Shop
    Route::get('/user/checkout', 'CheckoutController@index');
    Route::post('/user/checkout', 'CheckoutController@submit');
    Route::get('/user/checkout/{id}/cities', 'CheckoutController@getCities');
    
    Route::post('/user/product/{id}/add', 'CartController@add');
    Route::patch('/user/product/{id}/update', 'CartController@update');
    Route::delete('/user/product/{id}/remove', 'CartController@remove');

    Route::patch('/user/product/checkout/{id}', 'CheckoutController@checkout');

    //User Comment on Product
    Route::post('/user/product/{id}/info/comment', 'KomentarController@commentProduct');
    Route::patch('/user/product/comment/{id}/delete', 'KomentarController@deleteProductComment');

    //User Rating
    Route::post('/user/product/{id}/info/rating', 'RatingController@rating');

    //User Wishlist
    Route::get('/user/wishlist', 'WishlistController@index');
    Route::post('/user/product/{id}/wish', 'WishlistController@add');
    Route::delete('/user/product/{id}/wish/remove', 'WishlistController@remove');

    //User News
    Route::post('/user/news/{id}/read/comment', 'KomentarController@commentNews');
    Route::patch('/user/news/comment/{id}/delete', 'KomentarController@deleteNewsComment');

    //User Transaction
    Route::get('/user/transaction', 'TransaksiController@index');
    Route::post('/user/transaction', 'TransaksiController@pesan');
    Route::get('/user/shipping/{id}/choose', 'TransaksiController@pilihOngkir');

    //User Voucher
    Route::get('/user/voucher', 'VoucherController@index');
    Route::put('/user/voucher/fill/{id}', 'VoucherController@isi');
});

//User
Route::get('/user', 'UserzController@index')->name('user');
Route::get('/user/reset', 'UserzController@reset')->middleware('auth');
Route::post('/user/reset', 'UserzController@born');

//User Category
Route::get('/user/category', 'CategoryController@index');
Route::get('/user/category/product/{nama_kategori}', 'CategoryController@fltKategori');
Route::get('/user/category/search', 'CategoryController@srcProduk');

//User Shop
Route::get('/user/product/{id}/info', 'ProdukController@infoProduk');
Route::get('/user/cart', 'CartController@index');
Route::get('/user/product/{id}/cookie', 'CartController@index');

//User News
Route::get('/user/news', 'NewsController@index');
Route::get('/user/category/news/{nama_kategori}', 'NewsController@fltKategori');
Route::get('/user/news/{penulis}', 'NewsController@fltPenulis');
Route::get('/user/news/{created_at}', 'NewsController@fltTanggal');
Route::get('/user/news/{id}/read', 'NewsController@readmore');

//User Pages
Route::get('/user/login', 'AuthController@login');
Route::get('/user/register', 'AuthController@register');

//User Contact
Route::get('/user/contact', 'ContactController@index');
Route::post('/user/contact/post', 'ContactController@post');

//Login Socialite
Route::get('/user/login/{provider}', 'SocialController@redirect');
Route::get('/user/login/{provider}/callback','SocialController@callback');

//User Products Session
Route::get('user/product/{id}/add/session', 'CartController@addSession');
Route::patch('user/product/update/session', 'CartController@updateSession');
Route::delete('user/product/remove/session', 'CartController@removeSession');